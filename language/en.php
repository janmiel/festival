<?php
$title = "Uncle SAM";
$logo =  "unclesam_logo";
$tak = "yes";
$nie = "no";

$teksty_1 = "Enter exact error message or key words separeted by +";
$teksty_2 = "Find solution";
$teksty_3 = "Error not found, do you want us to find a solution for you?";
$teksty_4 = "Error";
$teksty_5 = "Email";
$teksty_6 = "Additional information";
$teksty_7 = "Screenshot";
$teksty_8 = "This service is free. You will receive email from us when we find a solution.";
$teksty_9 = "Enter code";
$teksty_10 = "Send";
$teksty_11 = "Solution";
$teksty_12 = "If this solution does not work for you and you need another one, please fill this form and describe situation when you are getting this error. We will constact you as soon as we find another solution.";
$teksty_13 = "Detailed problem description";
$teksty_14 = "Message has been sent.";
$teksty_15 = "Not what you are looking for? Click here to fill out a form.";
$teksty_16 = "Form has been sent.";

?>
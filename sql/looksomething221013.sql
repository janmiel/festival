-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 22 Paź 2013, 22:03
-- Wersja serwera: 5.5.24-log
-- Wersja PHP: 5.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `looksomething`
--
CREATE DATABASE IF NOT EXISTS `looksomething` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `looksomething`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_cat`
--

CREATE TABLE IF NOT EXISTS `gallery_cat` (
  `idgallery_cat` varchar(40) NOT NULL,
  `gallery_cat_name` text NOT NULL,
  `gallery_cat_description` text NOT NULL,
  `gallery_cat_folder_name` varchar(100) NOT NULL,
  `gallery_cat_folder_location` varchar(100) NOT NULL,
  `gallery_cat_create_date` date NOT NULL,
  PRIMARY KEY (`idgallery_cat`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `idnews` varchar(40) NOT NULL,
  `idnews_idnews_cat` varchar(40) NOT NULL,
  `news_name` varchar(50) DEFAULT NULL,
  `news_title` text,
  `news_description` text,
  `news_key_words` text,
  `news_url` text,
  `news_tags` text,
  `news_content` text,
  `news_temp1` text,
  `news_temp2` text,
  `news_data_dodania` date DEFAULT NULL,
  `news_godzina_dodania` time DEFAULT NULL,
  `news_title_s` text,
  `news_content_s` text,
  `news_zdjecie` text,
  `news_hash` text,
  `news_wejsc` int(11) DEFAULT '0',
  `news_publish` int(10) unsigned DEFAULT NULL,
  `news_access` varchar(40) DEFAULT NULL,
  `news_use_editor` int(1) DEFAULT NULL,
  `news_location` varchar(100) DEFAULT NULL,
  `news_style` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idnews`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news_cat`
--

CREATE TABLE IF NOT EXISTS `news_cat` (
  `idnews_cat` varchar(40) NOT NULL,
  `news_cat_name` varchar(100) DEFAULT NULL,
  `news_cat_description` text,
  `news_cat_timestamp` varchar(50) DEFAULT NULL,
  `news_cat_main` int(1) DEFAULT NULL,
  PRIMARY KEY (`idnews_cat`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `idpage` varchar(40) NOT NULL,
  `view_page_idview_page` varchar(40) NOT NULL,
  `idmenu` varchar(40) DEFAULT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `page_title` text,
  `page_description` text,
  `page_key_words` text,
  `page_url` text,
  `page_tags` text,
  `page_content` text,
  `page_temp1` text,
  `page_temp2` text,
  `page_data_dodania` date DEFAULT NULL,
  `page_godzina_dodania` time DEFAULT NULL,
  `page_title_s` text,
  `page_content_s` text,
  `page_zdjecie` text,
  `page_hash` text,
  `page_wejsc` int(11) DEFAULT '0',
  `page_publish` int(10) unsigned DEFAULT NULL,
  `page_access` varchar(40) DEFAULT NULL,
  `page_use_editor` int(1) DEFAULT NULL,
  `page_location` varchar(100) DEFAULT NULL,
  `page_style` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idpage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`idpage`, `view_page_idview_page`, `idmenu`, `page_name`, `page_title`, `page_description`, `page_key_words`, `page_url`, `page_tags`, `page_content`, `page_temp1`, `page_temp2`, `page_data_dodania`, `page_godzina_dodania`, `page_title_s`, `page_content_s`, `page_zdjecie`, `page_hash`, `page_wejsc`, `page_publish`, `page_access`, `page_use_editor`, `page_location`, `page_style`) VALUES
('1382018423rth1017qqeh1017dyn', '1382018247qej1017xmas1017ysx', NULL, 'banner', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 0, './page/banner.php', ''),
('1382021674ynd1017edw1017txs', '1382018247qej1017xmas1017ysx', NULL, 'homePage_ourWork', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 1, './page/homePage_ourWork.php', ''),
('1382024403mad1017dysq1017yso', '1382018247qej1017xmas1017ysx', NULL, 'homePage_ourPortfolio_left', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 0, NULL, 0, './page/homePage_ourPortfolio_left.php', ''),
('1382024414xq1017dwb1017kh', '1382018247qej1017xmas1017ysx', NULL, 'homePage_ourPortfolio_right', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 0, NULL, 0, './page/homePage_ourPortfolio_right.php', ''),
('1382386779txrt1021ysqef1021go', '1382018247qej1017xmas1017ysx', NULL, 'homePage_meetTeam', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 0, './page/homePage_meetTeam.php', ''),
('1382387013hrt1021ortd1021gk', '1382018247qej1017xmas1017ysx', NULL, 'homePage_contactUs_newsletter', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 0, './page/homePage_contactUs_newsletter.php', ''),
('1382387179hh1021axma1021dq', '1382018247qej1017xmas1017ysx', NULL, 'homePage_contactUs_contactUs', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 0, './page/homePage_contactUs_contactUs.php', ''),
('1382387302kk1021yqf1021dx', '1382018247qej1017xmas1017ysx', NULL, 'homePage_contactUs_tweet', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 0, './page/homePage_contactUs_tweet.php', ''),
('1382387433stx1021xcq1021qh', '1382018247qej1017xmas1017ysx', NULL, 'homePage_googleMaps', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 0, './page/homePage_googleMaps.php', ''),
('1382388448rth1021wqb1021oh', '1382018247qej1017xmas1017ysx', NULL, 'footer', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 1, './page/footer.php', ''),
('1382389147dys1021hqe1021qeh', '1382018247qej1017xmas1017ysx', NULL, 'questions_appTimeline', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 0, './page/questions_appTimeline.php', ''),
('1382389330ae1021qftx1021qq', '1382389041drt1021yscq1021kh', NULL, 'create-radio-app', 'Create Radio App', '', '', 'create-radio-app.html', '', NULL, '', '', NULL, NULL, 'Create Radio App', '', '', '0496b80bcc5b7bb65c79d8e92ce977a0', 0, 1, NULL, 0, './page/create-radio-app.php', ''),
('1382392868hx1021edj1021qed', '1382389041drt1021yscq1021kh', NULL, 'bundle-radio-app', '', '', '', 'bundle-radio-app.html', '', NULL, '', '', NULL, NULL, '', '', '', '71325952877327ace3531dc8ba6bc122', 0, 1, NULL, 0, './page/bundle-radio-app.php', ''),
('1382392613yx1021rtqw1021stx', '1382389041drt1021yscq1021kh', NULL, 'hotel-app-application', 'Hotel Application', '', '', 'hotel-app-application.html', '', NULL, '', '', NULL, NULL, 'Hotel Application', '', '', '46feb84d6505799936af0ed86e79eca6', 0, 1, NULL, 0, './page/hotel-app-application.php', ''),
('1382392961hh1021axma1021dq', '1382389041drt1021yscq1021kh', NULL, 'hotel-app-bundle-buy', '', '', '', 'hotel-app-bundle-buy.html', '', NULL, '', '', NULL, NULL, '', '', '', 'a3994c11fb9ae8cae7344c11d433c476', 0, 1, NULL, 0, './page/hotel-app-bundle-buy.php', ''),
('1382393018kk1021yqf1021dx', '1382389041drt1021yscq1021kh', NULL, 'contact', '', '', '', 'contact.html', '', NULL, '', '', NULL, NULL, '', '', '', '6d2cd37929c7c76ab3de0a8538b13998', 0, 1, NULL, 0, './page/contact.php', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `idsettings` varchar(40) NOT NULL,
  `settings_name` varchar(100) NOT NULL,
  `settings_value` varchar(500) NOT NULL,
  KEY `idsettins` (`idsettings`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `settings`
--

INSERT INTO `settings` (`idsettings`, `settings_name`, `settings_value`) VALUES
('1382479011hq1022xkys1022aw', 'contact_email', 'jakis@email.pl'),
('1382479126ysc1022qeye1022gq', 'contact_email_subject', 'Question form web form');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `idthemes` varchar(40) NOT NULL,
  `themes_name` varchar(100) DEFAULT NULL,
  `themes_description` text,
  `themes_location` varchar(100) DEFAULT NULL,
  `themes_main` int(1) NOT NULL,
  PRIMARY KEY (`idthemes`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `themes`
--

INSERT INTO `themes` (`idthemes`, `themes_name`, `themes_description`, `themes_location`, `themes_main`) VALUES
('1382018232hb1017yqg1017rtj', 'looksomething', '', 'themes/looksomething', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `idusers` varchar(40) NOT NULL,
  `users_imie` text,
  `users_nazwisko` text,
  `users_login` text,
  `users_haslo` text,
  `users_email` text,
  `users_prawa` varchar(40) DEFAULT NULL,
  `users_sygnatura` text,
  PRIMARY KEY (`idusers`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`idusers`, `users_imie`, `users_nazwisko`, `users_login`, `users_haslo`, `users_email`, `users_prawa`, `users_sygnatura`) VALUES
('1382018133hk1017matxyn1017cw', 'jan', 'miel', 'jan.miel', 'af8bc28d25298d4144660e90453e7a04', '', 'adminsuperuser', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `view`
--

CREATE TABLE IF NOT EXISTS `view` (
  `idview` varchar(40) NOT NULL,
  `view_name` varchar(100) DEFAULT NULL,
  `view_description` text,
  `view_location` varchar(100) DEFAULT NULL,
  `view_style` varchar(100) DEFAULT NULL,
  `view_editor` int(1) DEFAULT NULL,
  `view_themes` text NOT NULL,
  PRIMARY KEY (`idview`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `view`
--

INSERT INTO `view` (`idview`, `view_name`, `view_description`, `view_location`, `view_style`, `view_editor`, `view_themes`) VALUES
('1382018367aw1017maynx1017kd', 'banner', '', './themes/looksomething/views/banner.php', './themes/looksomething/views/style/banner.css', 0, 'looksomething'),
('1382020328rtk1017mayntx1017mama', 'homePage_slideshow', '', './themes/looksomething/views/homePage_slideshow.php', './themes/looksomething/views/style/homePage_slideshow.css', 0, 'looksomething'),
('1382021717dd1017fcx1017yg', 'homePage_ourWork', '', './themes/looksomething/views/homePage_ourWork.php', './themes/looksomething/views/style/homePage_ourWork.css', 0, 'looksomething'),
('1382024279xj1017xcc1017xw', 'homePage_ourPortfolio', '', './themes/looksomething/views/homePage_ourPortfolio.php', './themes/looksomething/views/style/homePage_ourPortfolio.css', 0, 'looksomething'),
('1382386426otx1021xynh1021ch', 'homePage_meetTeam', '', './themes/looksomething/views/homePage_meetTeam.php', './themes/looksomething/views/style/homePage_meetTeam.css', NULL, 'looksomething'),
('1382386884rth1021qekx1021xx', 'homePage_contactUs', '', './themes/looksomething/views/homePage_contactUs.php', './themes/looksomething/views/style/homePage_contactUs.css', 0, 'looksomething'),
('1382387401mak1021maxh1021bd', 'homePage_googleMaps', '', './themes/looksomething/views/homePage_googleMaps.php', './themes/looksomething/views/style/homePage_googleMaps.css', 0, 'looksomething'),
('1382388400wg1021kkh1021fx', 'footer', '', './themes/looksomething/views/footer.php', './themes/looksomething/views/style/footer.css', NULL, 'looksomething'),
('1382389097wq1021ynmaf1021bqe', 'questions_applicationTimeline', '', './themes/looksomething/views/questions_applicationTimeline.php', './themes/looksomething/views/style/questions_applicationTimeline.css', 0, 'looksomething'),
('1382390006xma1021xxk1021bh', 'showPageContent', '', './themes/looksomething/views/showPageContent.php', './themes/looksomething/views/style/showPageContent.css', NULL, 'looksomething');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `view_page`
--

CREATE TABLE IF NOT EXISTS `view_page` (
  `idview_page` varchar(40) NOT NULL,
  `view_page_name` text,
  `view_page_description` text,
  `view_page_sequence_inside` text,
  `view_page_sequence_outside` text,
  `view_page_home` int(1) DEFAULT NULL,
  `view_page_title` text,
  `view_page_description_2` text,
  `view_page_key_words` text,
  `view_page_themes` text NOT NULL,
  PRIMARY KEY (`idview_page`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `view_page`
--

INSERT INTO `view_page` (`idview_page`, `view_page_name`, `view_page_description`, `view_page_sequence_inside`, `view_page_sequence_outside`, `view_page_home`, `view_page_title`, `view_page_description_2`, `view_page_key_words`, `view_page_themes`) VALUES
('1382018247qej1017xmas1017ysx', 'homePage', '', 'banner homePage_slideshow homePage_ourWork homePage_ourPortfolio homePage_meetTeam homePage_contactUs homePage_googleMaps footer', '', 1, 'Home page', 'view_page_description_2', 'view_page_key_words', 'looksomething'),
('1382389041drt1021yscq1021kh', 'tempA', '', 'banner questions_applicationTimeline showPageContent homePage_contactUs footer', '', 0, '', '', '', 'looksomething');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2013 at 03:14 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `gallery_cat`
--

CREATE TABLE IF NOT EXISTS `gallery_cat` (
  `idgallery_cat` varchar(40) NOT NULL,
  `gallery_cat_name` text NOT NULL,
  `gallery_cat_description` text NOT NULL,
  `gallery_cat_folder_name` varchar(100) NOT NULL,
  `gallery_cat_folder_location` varchar(100) NOT NULL,
  `gallery_cat_create_date` date NOT NULL,
  PRIMARY KEY (`idgallery_cat`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_cat`
--

INSERT INTO `gallery_cat` (`idgallery_cat`, `gallery_cat_name`, `gallery_cat_description`, `gallery_cat_folder_name`, `gallery_cat_folder_location`, `gallery_cat_create_date`) VALUES
('13mart0906qeeyn0906fma', 'pierwszy dzien w szkole', '', 'pierwszydzienwszkole2013', './gallery/pierwszydzienwszkole2013', '2013-09-06'),
('13eh0909hyntx0909fq', 'rozpoczęcie roku szkolnego 2013/2014 dfg sd fgdf gdsf gsdfg sdfg', '', '2013rozpoczecieroku', './gallery/2013rozpoczecieroku', '2013-09-09'),
('13wy0909odd0909qyn', 'Brzechwaleczki 2013', '', 'brzechwaleczki2013', './gallery/brzechwaleczki2013', '2013-09-09'),
('1380038423ynys0924qkx0924txk', 'konkursortograficzny2013', '', 'konkursortograficzny2013', './gallery/konkursortograficzny2013', '2013-09-24');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `idnews` varchar(40) NOT NULL,
  `idnews_idnews_cat` varchar(40) NOT NULL,
  `news_name` varchar(50) DEFAULT NULL,
  `news_title` text,
  `news_description` text,
  `news_key_words` text,
  `news_url` text,
  `news_tags` text,
  `news_content` text,
  `news_temp1` text,
  `news_temp2` text,
  `news_data_dodania` date DEFAULT NULL,
  `news_godzina_dodania` time DEFAULT NULL,
  `news_title_s` text,
  `news_content_s` text,
  `news_zdjecie` text,
  `news_hash` text,
  `news_wejsc` int(11) DEFAULT '0',
  `news_publish` int(10) unsigned DEFAULT NULL,
  `news_access` varchar(40) DEFAULT NULL,
  `news_use_editor` int(1) DEFAULT NULL,
  `news_location` varchar(100) DEFAULT NULL,
  `news_style` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idnews`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`idnews`, `idnews_idnews_cat`, `news_name`, `news_title`, `news_description`, `news_key_words`, `news_url`, `news_tags`, `news_content`, `news_temp1`, `news_temp2`, `news_data_dodania`, `news_godzina_dodania`, `news_title_s`, `news_content_s`, `news_zdjecie`, `news_hash`, `news_wejsc`, `news_publish`, `news_access`, `news_use_editor`, `news_location`, `news_style`) VALUES
('1380295140maw0927txyrt0927qc', '1382697983wy1025yskk1025ay', 'Zakończenie roku szkolnego', 'Zakończenie roku szkolnego', 'sdfaa', 'ss', 'zakonczenie-roku-szkolnego', '22', NULL, '', '', NULL, NULL, 'Zakonczenie roku szkolnego', 'ss', '', 'a6ab5336fac51507270ae240f3769d20', 0, 1, NULL, 1, './news/1380295140maw0927txyrt0927qc.html', NULL),
('1382712993yb1025ddqe1025be', '1380285143jx0927dynd0927j', 'Pasowanie na ucznia 2013', 'Pasowanie na ucznia 2013', '', '', 'pasowanie-na-ucznia-2013', '', NULL, '', '', NULL, NULL, 'Pasowanie na ucznia 2013', '', '', 'e3416a1ece45c0a99ef167a22c214ae9', 0, 1, NULL, 1, './news/1382712993yb1025ddqe1025be.html', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news_cat`
--

CREATE TABLE IF NOT EXISTS `news_cat` (
  `idnews_cat` varchar(40) NOT NULL,
  `news_cat_name` varchar(100) DEFAULT NULL,
  `news_cat_description` text,
  `news_cat_timestamp` varchar(50) DEFAULT NULL,
  `news_cat_main` int(1) DEFAULT NULL,
  PRIMARY KEY (`idnews_cat`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_cat`
--

INSERT INTO `news_cat` (`idnews_cat`, `news_cat_name`, `news_cat_description`, `news_cat_timestamp`, `news_cat_main`) VALUES
('1380285143jx0927dynd0927j', 'nie_przydzielone', '', '1380285143', NULL),
('1382697983wy1025yskk1025ay', 'Pakusiaki', '', '1382697983', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `idpage` varchar(40) NOT NULL,
  `view_page_idview_page` varchar(40) NOT NULL,
  `idmenu` varchar(40) DEFAULT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `page_title` text,
  `page_description` text,
  `page_key_words` text,
  `page_url` text,
  `page_tags` text,
  `page_content` text,
  `page_temp1` text,
  `page_temp2` text,
  `page_data_dodania` date DEFAULT NULL,
  `page_godzina_dodania` time DEFAULT NULL,
  `page_title_s` text,
  `page_content_s` text,
  `page_zdjecie` text,
  `page_hash` text,
  `page_wejsc` int(11) DEFAULT '0',
  `page_publish` int(10) unsigned DEFAULT NULL,
  `page_access` varchar(40) DEFAULT NULL,
  `page_use_editor` int(1) DEFAULT NULL,
  `page_location` varchar(100) DEFAULT NULL,
  `page_style` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idpage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`idpage`, `view_page_idview_page`, `idmenu`, `page_name`, `page_title`, `page_description`, `page_key_words`, `page_url`, `page_tags`, `page_content`, `page_temp1`, `page_temp2`, `page_data_dodania`, `page_godzina_dodania`, `page_title_s`, `page_content_s`, `page_zdjecie`, `page_hash`, `page_wejsc`, `page_publish`, `page_access`, `page_use_editor`, `page_location`, `page_style`) VALUES
('13xys0906ysmaa0906wrt', '13go0905yqetx0905qq', NULL, 'information', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1, NULL, 1, './page/information.php', ''),
('13may0909ysdq0909gx', '13qe0909qtxma0909hk', NULL, 'galeria', 'Szkoła PAK - galeria zdjęć', 'galeria zdjęć szkoły pak', 'galeria zdjęć szkoły pak', 'galeria.html', '', NULL, '', '', NULL, NULL, 'Szkola PAK - galeria zdjec', 'galeria zdjec szkoly pak', '', '99156df92f328786cc9220faf971c098', 0, 1, NULL, 0, './page/galeria.php', ''),
('13xq0923eysj0923ao', '13qe0909qtxma0909hk', NULL, 'nauczyciele', '', '', '', 'nauczyciele.html', '', NULL, '', '', NULL, NULL, '', '', '', 'f446931fa4148a4c2b81579c1d11e1dc', 0, 1, NULL, 1, './page/nauczyciele.php', ''),
('13xyn0923dxk0923cqe', '13qe0909qtxma0909hk', NULL, 'contact', '', '', '', 'contact.html', '', NULL, '', '', NULL, NULL, '', '', '', '6d2cd37929c7c76ab3de0a8538b13998', 0, 0, NULL, 1, './page/contact.php', ''),
('1382713123hf1025dqd1025qj', '13qe0909qtxma0909hk', NULL, 'wiadomosci', '', '', '', 'wiadomosci.html', '', NULL, '', '', NULL, NULL, '', '', '', 'c3ebc187fb283e5ca0c21063c1a36c70', 0, 1, NULL, 0, './page/wiadomosci.php', '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `idsettins` varchar(40) NOT NULL,
  `settings_name` varchar(100) NOT NULL,
  `settings_value` varchar(500) NOT NULL,
  KEY `idsettins` (`idsettins`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `idthemes` varchar(40) NOT NULL,
  `themes_name` varchar(100) DEFAULT NULL,
  `themes_description` text,
  `themes_location` varchar(100) DEFAULT NULL,
  `themes_main` int(1) NOT NULL,
  PRIMARY KEY (`idthemes`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`idthemes`, `themes_name`, `themes_description`, `themes_location`, `themes_main`) VALUES
('13sh0905qefk0905dd', 'dlaKarola', '', 'themes/dlaKarola', 0),
('1381939785wb1016ywx1016dq', 'looksomething', '', 'themes/looksomething', 0),
('1382017623dk1017txde1017yny', 'test', '', 'themes/test', 0),
('1382542401yb1023wqw1023fc', 'szkolapak', '', 'themes/szkolapak', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `idusers` varchar(40) NOT NULL,
  `users_imie` text,
  `users_nazwisko` text,
  `users_login` text,
  `users_haslo` text,
  `users_email` text,
  `users_prawa` varchar(40) DEFAULT NULL,
  `users_sygnatura` text,
  PRIMARY KEY (`idusers`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `users_imie`, `users_nazwisko`, `users_login`, `users_haslo`, `users_email`, `users_prawa`, `users_sygnatura`) VALUES
('13xma0905wfd0905xx', 'jan', 'miel', 'jan.miel', 'af8bc28d25298d4144660e90453e7a04', '', 'adminsuperuser', '');

-- --------------------------------------------------------

--
-- Table structure for table `view`
--

CREATE TABLE IF NOT EXISTS `view` (
  `idview` varchar(40) NOT NULL,
  `view_name` varchar(100) DEFAULT NULL,
  `view_description` text,
  `view_location` varchar(100) DEFAULT NULL,
  `view_style` varchar(100) DEFAULT NULL,
  `view_editor` int(1) DEFAULT NULL,
  `view_themes` text NOT NULL,
  PRIMARY KEY (`idview`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `view`
--

INSERT INTO `view` (`idview`, `view_name`, `view_description`, `view_location`, `view_style`, `view_editor`, `view_themes`) VALUES
('13ws0905txbq0905kk', 'banner', '', './themes/dlaKarola/views/banner.php', './themes/dlaKarola/views/style/banner.css', 0, 'dlaKarola'),
('13cx0905hxf0905ow', 'black_top', '', './themes/dlaKarola/views/black_top.php', './themes/dlaKarola/views/style/black_top.css', 0, 'dlaKarola'),
('13ah0905dqk0905jyn', 'slider', '', './themes/dlaKarola/views/slider.php', './themes/dlaKarola/views/style/slider.css', 0, 'dlaKarola'),
('13xrt0905hgx0905cc', 'slider_background', '', './themes/dlaKarola/views/slider_background.php', './themes/dlaKarola/views/style/slider_background.css', 0, 'dlaKarola'),
('13ynj0906qyy0906bg', 'our_work', '', './themes/dlaKarola/views/our_work.php', './themes/dlaKarola/views/style/our_work.css', 0, 'dlaKarola'),
('13qrt0906xxk0906gw', 'box_containter', '', './themes/dlaKarola/views/box_containter.php', './themes/dlaKarola/views/style/box_containter.css', 0, 'dlaKarola'),
('13kx0906txgd0906yntx', 'about_us_bar', '', './themes/dlaKarola/views/about_us_bar.php', './themes/dlaKarola/views/style/about_us_bar.css', 0, 'dlaKarola'),
('13ysk0906hoo0906cc', 'footer', '', './themes/dlaKarola/views/footer.php', './themes/dlaKarola/views/style/footer.css', 0, 'dlaKarola'),
('13aq0906wtxc0906bqe', 'banner', '', './themes/szkolapak/views/banner.php', './themes/szkolapak/views/style/banner.css', 0, 'szkolapak'),
('13htx0906ceh0906jd', 'information', '', './themes/szkolapak/views/information.php', './themes/szkolapak/views/style/information.css', 0, 'szkolapak'),
('13drt0906xqj0906hb', 'left_side', '', './themes/szkolapak/views/left_side.php', './themes/szkolapak/views/style/left_side.css', 0, 'szkolapak'),
('13xo0923ddg0923rtk', 'home_slideshow', '', './themes/szkolapak/views/home_slideshow.php', './themes/szkolapak/views/style/home_slideshow.css', 0, 'szkolapak'),
('13rtf0909aynys0909mad', 'link_left_side', '', './themes/szkolapak/views/link_left_side.php', './themes/szkolapak/views/style/link_left_side.css', 0, 'szkolapak'),
('13qma0909ogx0909ys', 'page_content', 'show page content', './themes/szkolapak/views/page_content.php', './themes/szkolapak/views/style/page_content.css', 0, 'szkolapak'),
('13yg0923txysg0923kq', 'partners', 'partners logo on bottom', './themes/szkolapak/views/partners.php', './themes/szkolapak/views/style/partners.css', 0, 'szkolapak'),
('13qx0909ahq0909ynd', 'gallery', '', './themes/szkolapak/views/gallery.php', './themes/szkolapak/views/style/gallery.css', 0, 'szkolapak'),
('13sh0923heq0923yo', 'footer', '', './themes/szkolapak/views/footer.php', './themes/szkolapak/views/style/footer.css', 0, 'szkolapak'),
('1380039704cg0924bjtx0924w', 'right_side', '', './themes/szkolapak/views/right_side.php', './themes/szkolapak/views/style/right_side.css', 0, 'szkolapak'),
('1382017797bw1017qyyn1017bx', 'banner', '', './themes/looksomething/views/banner.php', './themes/looksomething/views/style/banner.css', 0, 'looksomething'),
('1382630862fj1024fmag1024bb', 'background', '', './themes/szkolapak/views/background.php', './themes/szkolapak/views/style/background.css', 0, 'szkolapak'),
('1382697839txys1025qehc1025de', 'homepage_wiadomosci', '', './themes/szkolapak/views/homepage_wiadomosci.php', './themes/szkolapak/views/style/homepage_wiadomosci.css', 0, 'szkolapak');

-- --------------------------------------------------------

--
-- Table structure for table `view_page`
--

CREATE TABLE IF NOT EXISTS `view_page` (
  `idview_page` varchar(40) NOT NULL,
  `view_page_name` text,
  `view_page_description` text,
  `view_page_sequence_inside` text,
  `view_page_sequence_outside` text,
  `view_page_home` int(1) DEFAULT NULL,
  `view_page_title` text,
  `view_page_description_2` text,
  `view_page_key_words` text,
  `view_page_themes` text NOT NULL,
  PRIMARY KEY (`idview_page`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `view_page`
--

INSERT INTO `view_page` (`idview_page`, `view_page_name`, `view_page_description`, `view_page_sequence_inside`, `view_page_sequence_outside`, `view_page_home`, `view_page_title`, `view_page_description_2`, `view_page_key_words`, `view_page_themes`) VALUES
('13go0905yqetx0905qq', 'homePage', 'description', 'banner slider our_work box_containter about_us_bar footer', 'black_top slider_background', 1, 'view_page_title', 'view_page_description_2', 'view_page_key_words', 'dlaKarola'),
('13qex0906cxy0906fy', 'homePage', '', 'banner information homepage_wiadomosci home_slideshow partners footer', 'background left_side info_left facebook', 1, 'view_page_title', 'view_page_description_2', 'view_page_key_words', 'szkolapak'),
('13qe0909qtxma0909hk', 'showPageContent', '', 'banner information page_content right_side partners footer', 'background left_side info_left facebook', 0, '', '', '', 'szkolapak'),
('1381939818qeb1016gey1016btx', 'homePage', '', 'banner', '', 1, 'view_page_title', 'view_page_description_2', 'view_page_key_words', 'looksomething');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

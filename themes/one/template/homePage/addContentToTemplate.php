<?php

// SET DEFAULT VALUES

	$buildPage->addBlock('eventsByCategory-1');
	$buildPage->addBlock('eventsByCategory-2');
	$buildPage->addBlock('eventsByCategory-0');
// NAVBAR
	$navbarClasses = 'data-spy="affix" data-offset-top="80"';
	$buildPage->addToBlock('navbar_classes', $navbarClasses);

// SEARCH
$search = '
 <!-- Begin: Search -->    
        <div class="hero-content clearfix">
          <form id="nl-form" class="nl-form text-center" method="get">
            I&#39;m looking for an event in
            <!--input type="text" value="" placeholder="an event" data-subline="For example: <em>U2</em> or <em>The Cranberries</em>"/-->
            
            <select>
              <option value="" selected>any Category</option>
              <option value="./?categry/3/Celebrity Appearance">Celebrity Appearance</option>
              <option value="./?category/2/comedy">Comedy</option>
              <option value="./?category/9/Darts">Darts</option>
              <option value="./?category/8/Family Entertainment">Family Entertainment</option>
              <option value="./?category/1/music">Music</option>
            </select>
            
            <!--select style="height:200px;">
              <option value="1" selected>any County</option>
              <option value="2">Antrim</option>
              <option value="3">Armagh</option>
              <option value="4">Carlow</option>
              <option value="5">Cavan</option>
              <option value="6">Clare</option>
              <option value="7">Cork</option>
              <option value="8">Derry</option>
              <option value="9">Donegal</option>
              <option value="10">Down</option>
            </select-->
            <!--center><button class="nl-submit" type="submit">Search</button></center-->

            <div class="nl-overlay"></div>
          </form>
        </div>
        
      <!-- End: Search -->
';

	//required by search form
	$searchVerifier = '
	<script src="'. $themes_path . '/js/nlform.js"></script>
        <script>
        var nlform = new NLForm(document.getElementById( "nl-form" ) );
      </script>';
      $buildPage->addToBlock("template_footer_after", $searchVerifier);

$buildPage->addToBlock("search", $search);

// HERO DIV

	$heroDiv = '<div class="hero-home height-70vh" style="background-image: url(\'images/page/hero_1920px.jpg\');">';
	$heroDivEnd = '</div>';

	$buildPage->addToBlock("hero-div", $heroDiv);
	$buildPage->addToBlock("hero-div-end", $heroDivEnd);

// ADVERTS BANNER


    $buildPage->addToBlock("adverts_banner", $advertsBanner);


// EVENT UPCOMMING 

		

if($pageContent['eventDetails'] != ""){

	$event_details = "";

	foreach($pageContent['eventDetails'] as $eventDetails){

		$event_details .= '
		<div class="event">
		        <div class="block">
		          <div class="row">
		            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 column text-center">
		            ';
		            if(isset($eventDetails->pictures['defaultEvent'])){

						$event_picture = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['url']; 
						$event_picture_thumb = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['urlThumb']; 
						$event_picture_medium = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['urlMedium']; 
						$event_picture_crop = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['urlCrop']; 
					}elseif(isset($eventDetails->pictures['pictureEvent'][0]['url'])){

						if (isset($eventDetails->pictures['pictureEvent'][0]['url']))$event_picture = $eventDetails->pictures['pictureEvent'][0]['url']; 
						if (isset($eventDetails->pictures['pictureEvent'][0]['urlThumb']))$event_picture_thumb = $eventDetails->pictures['pictureEvent'][0]['urlThumb']; 
						if (isset($eventDetails->pictures['pictureEvent'][0]['urlMedium']))$event_picture_medium = $eventDetails->pictures['pictureEvent'][0]['urlMedium']; 
						if (isset($eventDetails->pictures['pictureEvent'][0]['urlCrop']))$event_picture_crop = $eventDetails->pictures['pictureEvent'][0]['urlCrop']; 

					}else{
						$event_picture_crop = $eventDetails->pictures['no_image'][0]['default_2_1'];
					}

				

					if(file_exists($event_picture_crop)){
					
						$event_picture = $event_picture_crop;
						
					}
					$event_details .= '
		              <a href="' . $eventDetails->eventDateDetails['url'] . '">
		              	<img src="' . $event_picture . '" alt="..." class="img-thumbnail">
		              </a>
		            </div>
		            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 column text-center">
		             
		              <!--span class="day">' . $eventDetails->eventDateDetails['date_day_day'] . '</span> 
		              <span class="date">' . $eventDetails->eventDateDetails['date_month_name'] . '</span-->

		            </div>
		            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 column">
		             <h3>
		             	<a href="' . $eventDetails->eventDateDetails['url'] . '"> 
		             	' . $eventDetails->eventDetails['title'] . ': ' . $eventDetails->eventDateDetails['date_day_day'] . ' ' . $eventDetails->eventDateDetails['date_month_name'] . ' ' . $eventDetails->eventDateDetails['date_year'] . '</a>
		             </h3>
		             <p>';

		             if(!empty($eventDetails->venue)){
		             	$event_details .='
		             	<a target="_blank" href="https://www.google.com/maps/search/' . $eventDetails->venue['name'] . ', ' . $eventDetails->venue['address'] . '">' . $eventDetails->venue['name'] . ', ' . $eventDetails->venue['city'] . ', ' . $eventDetails->venue['county'] .' 
		             	</a>
		            	
		             ';
		         	}

		         	$event_details .= '
		         	 </p>
		             <p>' . $eventDetails->eventDateDetails['date_day_name'] . ', ' . $eventDetails->eventDateDetails['date_month_name'] . ' ' . $eventDetails->eventDateDetails['date_day_day'] . ', ' . $eventDetails->eventDateDetails['date_year']  . ', ' . $eventDetails->eventDateDetails['time'] . '</p>';

		             $event_details .= '

		              <p>Tickets from &euro;' . ($eventDetails->tickets['ticket'][0]['price'] + $eventDetails->tickets['ticket'][0]['commission']);

		              $numberOfTickets = sizeof($eventDetails->tickets['ticket']);

		              if($numberOfTickets > 1){
						$event_details .= ' - &euro;' . $eventDetails->tickets['ticket'][$numberOfTickets - 1 ]['price'];	
		              }

		              $event_details .= '</p>
		           </div>
		         </div>   

		         <div class="row"> 
		          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column">
		           <a href="' . $eventDetails->eventDateDetails['url'] . '" class="btn-buy pull-right">Get Tickets</a>
		         </div>
		       </div>
		     </div>
		   </div>
		   ';
	}  
	}

	$buildPage->addToBlock('event_details', $event_details);
	  




////////////////////////////////////////




// SOCIAL MEDIA

 $buildPage->addBlock("description");
 if(isset($pageContent->details['description'])){
 	$buildPage->addToBlock("description", clearPost(cut_string($pageContent->details['description'], 100, "...")));
 }

 $buildPage->addToBlock("social_image_url", $buildPage->settings['page_url'] . '/images/page/ticketstoplogo.png');
 $buildPage->addToBlock("social_url", $buildPage->settings['page_url'] . $full_page_url);



<?php

	//find event
	$shortEventId = clearPost($_GET[1]);
	$tempEventName = clearPost($_GET[2]);

	$eventHash = createPageHash($shortEventId, $tempEventName);
	$event = findEventByHash($eventHash);

	$preview = false;

if($event != false){
	
	if(isset($_GET[3]) and $_GET[3] == "preview" and ($_SESSION['user_access_tier'] == "promoter" or $_SESSION['user_access_tier'] == "admin" or $_SESSION['user_access_tier'] == "adminsuperuser")){

	$preview = true;

	$currentIdEvent = $event['idevent'];
	$file_location = "./sessions/event_build_temp/";


	$eventBuilder = new eventBuilder($currentIdEvent);
	$new_dates_temp =  unserialize(open_file($file_location . $currentIdEvent . '.session'));



		if(!empty($new_dates_temp['dates'])){

			$new_dates_with_tickets = array();
			
			foreach($new_dates_temp['dates'] as $new_dates){

				$new_dates['tickets'] = $new_dates_temp['tickets'];

				$new_dates_with_tickets[] = $new_dates;

			}

			$eventBuilder->dates['event_date_dates'][0]['dates'] = $new_dates_with_tickets ;
			
			//don't save class stage
			$dontSaveClass = true;
			

			// images 

			$idevent = $eventBuilder->details['idevent'];

			$result = getEventImages($idevent);

			if($result != false){

					//reset pictures

					unset($eventBuilder->pictures['picture']);

	            	$i=0;

	            	foreach($result as $res){

	            		$picture = array(
						"url" => '' . $buildPage->settings['eventImageLocation']  . $idevent . '/' . $res['event_image_filename'],
						"urlThumb" => '' . $buildPage->settings['eventImageLocation'] . $idevent . '/thumb/' . $res['event_image_filename'],
						"urlCrop" => '' . $buildPage->settings['eventImageLocation'] . $idevent . '/thumb/crop-' . $res['event_image_filename'],
						"urlMedium" => '' . $buildPage->settings['eventImageLocation'] . $idevent . '/thumb/medium-' . $res['event_image_filename'],
						"alt" => $eventBuilder->details['title']
						
						);

	            		if($res['event_image_default'] == 1){
	            			$eventBuilder->addToPictures('default', $i);
	            		}

						$eventBuilder->addToPictures('picture', $picture, "push");

						$i++;

	            	}

	         }

			$pageContent = $eventBuilder;
		}

	}elseif(!isset($_GET['preview'])){ // if there is event id but not review


			$idevent = $event['idevent'];
		
			$pageContent = new eventBuilder($idevent, $ticketsActive = 1);



		


	
	}
}else{
	$redirectUrl = "index.html";
	redirectTo($redirectUrl);
}


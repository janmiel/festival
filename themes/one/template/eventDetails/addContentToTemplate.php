<?php

$buildPage->addBlock("event_details_age_info");

// pageContent contains all data collected by "prepareContent.php"

// ADVERT

	//$buildPage->addToBlock("adverts_banner", $advertsBanner);

// HERO 

	$hero = '<div class="hero height-70vh" style="background-image: url(<<<{event_picture}>>>);">';
	$buildPage->addToBlock("hero-div", $hero);

	$endHero = '
	  <div class="hero-content hidden-xs ">
         <h1> <<<{event_details_title}>>> </h1>
         <h2></h2>
       </div>

     </div>';
	$buildPage->addToBlock("hero-div-end", $endHero);
	

// NAVBAR CLASSES

	$buildPage->addToBlock("navbar_classes", "navbar-default");
     


// IMAGES


	
	if(!empty($pageContent->pictures['picture'])){
		if(isset($pageContent->pictures['defaultEvent'])){
			
			//$event_picture = $pageContent->pictures['pictureEvent'][$pageContent->pictures['defaultEvent']]['url']; 

		}elseif(isset($pageContent->pictures['default'])){

			$event_picture = $pageContent->pictures['picture'][$pageContent->pictures['default']]['url']; 
			$event_picture_thumb = $pageContent->pictures['picture'][$pageContent->pictures['default']]['urlThumb']; 
			$event_picture_medium = $pageContent->pictures['picture'][$pageContent->pictures['default']]['urlMedium']; 
			$event_picture_crop = $pageContent->pictures['picture'][$pageContent->pictures['default']]['urlCrop']; 
		}else{

			$event_picture = $pageContent->pictures['picture'][0]['url']; 
			$event_picture_thumb = $pageContent->pictures['picture'][0]['urlThumb']; 
			$event_picture_medium = $pageContent->pictures['picture'][0]['urlMedium']; 
			$event_picture_crop = $pageContent->pictures['picture'][0]['urlCrop']; 

		}

		$event_picture_social = $event_picture_medium;

		if(file_exists("." . $event_picture_crop)){
		
			$event_picture = $event_picture_crop;
			$event_picture_social = $event_picture_crop;
		}
	}else{
		$event_picture = $pageContent->pictures['no_image'][0]['default_1200'];
		$event_picture_social = $pageContent->pictures['no_image'][0]['default_800'];

	}

	$buildPage->addToBlock('event_picture', "." . $event_picture);

// EVENT DETAILS

	$event_details_title = $pageContent->details['title'];


	$buildPage->addToBlock("event_details_title",$event_details_title);



// DESCRIPTION

	$buildPage->addToBlock("event_details_description", $pageContent->details['description']);

// AGE INFO
	$age_info = $pageContent->age_info;
	if($age_info != ""){
		
		$content = "<h3>" . $age_info['name'] . "</h3>";
		$content .= "<p>" . $age_info['value'] . "</p><br>";

		$buildPage->addToBlock("event_details_age_info", $content);
	}

//DATES

	$event_details_dates = "";
	$event_details_dates_array = array();

	$buildPage->addBlock('event_details_day_name');
	$buildPage->addBlock('event_details_day');
	$buildPage->addBlock('event_details_month_name');
	$buildPage->addBlock('event_details_year');
	$buildPage->addBlock('event_details_time');
	$buildPage->addBlock('event_details_times_dates');
	$buildPage->addBlock('event_details_dates');
	
	$event_details_dates_array = array();

	if(!empty($pageContent->dates['event_date_dates'])){

		foreach($pageContent->dates['event_date_dates'] as $tmp){
			$event_details_dates_array = array_merge($event_details_dates_array , $tmp['dates']);
		}


		$previous_month = ""; // to trigger month change

		foreach($event_details_dates_array as $event_date){
//for preview only
		$time = $event_date['event_date_time'];




			$event_day_name = getDateForMe("getNameShort", null, $event_date['event_date_date']);
			$event_day_day = getDateForMe("getDay", null, $event_date['event_date_date']);
			$event_year = getDateForMe("year", null, $event_date['event_date_date']);
			$event_month = getDateForMe("month", null, $event_date['event_date_date']);
			$event_month_name = getDateForMe("getMonthName", null, $event_date['event_date_date']);

			$event_date_venue = getVenue($event_date['idvenue']);
			
			
				// show dates
				if($previous_month == "" or $event_month != $previous_month){
					$event_details_dates .= ' <h3><span class="glyphicon glyphicon-calendar"></span> ' . strtoupper($event_month_name) . ' ' . strtoupper($event_year) . '</h3> ';
				}
				$event_details_dates .='
				     
				    <form method="post" action="?checkout"> 

				      <div class="event">
				        <div class="header"><a target="_blank" href="https://www.google.com/maps/search/' . $event_date_venue['name'] . ', ' . $event_date_venue['address'] . '">' 

				        	. strtoupper($event_day_name) . ' ' . strtoupper($event_day_day) . ' ' . $time . ' - <span class="glyphicon glyphicon-map-marker"></span> ' . $event_date_venue['name'] . ', ' . $event_date_venue['venue_city'] . ', ' . $event_date_venue['venue_county'] . '</a>
				         </div>';
						

						
						;

						if(!empty($event_date['tickets'])){

					        foreach($event_date['tickets'] as $ticket){

					        	//for preview
					        		if($preview == true){
					        			$ticket['idevent_ticket'] = null;
					        		}

							 	$numberOfSoldTickets = getNumberSoldTickets($event_date['idevent_date'], $ticket['idevent_ticket']);

							 	if($numberOfSoldTickets >= $ticket['event_ticket_quantity']){
							 		$tickets_sold = true;
							 	}else{
							 		$tickets_sold = false;
							 	}

								$event_details_dates .='

								<input class="hide" name="event_date[]" value="' . $event_date['idevent_date'] . '">
					    		<input class="hide" name="event_ticket[]" value="' . $ticket['idevent_ticket'] . '">


						        <div class="block">
						          <div class="row">
						            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 column"><p><i class="fa fa-ticket" aria-hidden="true"></i> ' . strtoupper($ticket['event_ticket_name']) . '</p><span></span></div>
						            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 column">';
						            if($tickets_sold){

										$event_details_dates .= '
											<img class="responsive right" src="'. $buildPage->getSettings("images_page") . '/sold_out.jpg"></div>
										';
										}else{

											//$payment_fee = calculateStripeFees($ticket['event_ticket_price'] + $ticket['event_ticket_commission'], $buildPage->settings['stripe_fee_fixed'] , $buildPage->settings['stripe_fee_percentage'] );

											$tooltip_content = '<div>
																Ticket price: &euro;' . priceFormat($ticket['event_ticket_price']) . ' <br>
																Fee: &euro;' . $ticket['event_ticket_commission'] . '<br>
																
																Buyer Total: &euro;' . ($ticket['event_ticket_price'] + $ticket['event_ticket_commission'] ) . '
																</div>';

										$event_details_dates .= '
										&euro;' . ($ticket['event_ticket_price'] + $ticket['event_ticket_commission'])  . '
											<span class="glyphicon glyphicon-info-sign tooltipped" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="' . $tooltip_content . '"></span>
										</div>

							            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 column">
							              <select name="tickets_quantity[]"" class="form-control">
							                <option>0</option>
							                <option>1</option>
							                <option>2</option> 
							                <option>3</option>
							                <option>4</option>
							                <option>5</option> 
							                <option>6</option>
							                <option>7</option>
							                <option>8</option>
							              </select>
							            </div>';
							        }
						          $event_details_dates .= ' 
						          </div>   
						        </div>
						        ';
					   		}
					   	}

				       if($preview == false){
					       $event_details_dates .='
					        <div class="block">
					          <div class="row"> 
					            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column">
					              <button type="submit" class="btn-buy pull-right">Check out</button>
					           </div>
					         </div>
					       </div>';
					    }
				     $event_details_dates .= '
				     </div>
				    </form>';


								
					$previous_month = $event_month;
					}

				
			

				$event_details_dates .= '
				
				</div>
				';
		}

		$buildPage->addToBlock("event_details_dates", $event_details_dates);
	

	




//showVar($pageContent);

// SOCIAL MEDIA
$buildPage->addBlock("title"); // resets previous value
$buildPage->addToBlock("title", $pageContent->details['title']);

$buildPage->addBlock("description");
$buildPage->addToBlock("description", clearPost(cut_string($pageContent->details['description'], 100, "...")));

$buildPage->addToBlock("social_image_url", $buildPage->settings['page_url'] . $event_picture_social);

list($width, $height) = getimagesize("." . $event_picture_social);
$buildPage->addToBlock("social_image_width", $width);
$buildPage->addToBlock("social_image_height", $height);





$buildPage->addToBlock("social_url", $buildPage->settings['page_url'] . $full_page_url);
// <meta property="og:title" content="<<<{title}>>>"/>
// <meta property="og:description" content="<<<{description}>>>" />
// <meta property="og:url" content="<<<{social_url}>>>" />
// <meta property="og:image" content="<<<{social_image_url}>>>" />



// <meta name="twitter:site" content="@ticketstopie">
// <meta name="twitter:title" content="<<<{title}>>>">
// <meta name="twitter:description" content="<<<{description}>>>">
// <meta name="twitter:image:src" content="<<<{social_image_url}>>>">

		

<?php


// BODY CLASS
	$bodyClasses = 'fixed-navbar';
	$buildPage->addToBlock('body_classes' , $bodyClasses);

// NAVBAR
	$navbarClasses = 'navbar-fixed-top';
	$buildPage->addToBlock('navbar_classes', $navbarClasses);


// pageContent contains all data collected by "prepareContent.php"

$buildPage->addBlock("buyer_first_name_validation_state");
$buildPage->addBlock("buyer_last_name_validation_state");
$buildPage->addBlock("buyer_email_validation_state");
$buildPage->addBlock("buyer_first_name");
$buildPage->addBlock("buyer_last_name");
$buildPage->addBlock("buyer_email");

// IMAGES
	
	if(isset($order->eventDateObject->pictures['defaultEvent'])){
		
		
		$event_picture = $order->eventDateObject->pictures['pictureEvent'][$order->eventDateObject->pictures['defaultEvent']]['url']; 

	}elseif(isset($order->pictures['default'])){	

		$event_picture = $order->eventDateObject->pictures['picture'][$order->eventDateObject->pictures['default']]['url']; 
	}else{

		$event_picture = $order->eventDateObject->pictures['picture'][0]['url']; 

	}

	$buildPage->addToBlock('event_picture', $event_picture);

// EVENT DETAILS

	$event_details_title = $order->eventDateObject->eventDetails['title'];


	$buildPage->addToBlock("event_details_title",$event_details_title);



// DESCRIPTION

	$buildPage->addToBlock("event_details_description", $order->eventDateObject->details['description']);

//DATES

	$event_details_dates = "";
	$event_details_dates_array = array();

	
	

	if(!empty($order->eventDateObject->eventDateDetails['date'])){

			$buildPage->addToBlock('event_details_day_name', getDateForMe("dayName", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_day', getDateForMe("getDay", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_month_name', getDateForMe("getMonthName", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_year', getDateForMe("year", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_time', $order->eventDateObject->eventDateDetails['time']);

			}

	// TICKET

	if(!empty($order->ticketObject)){

		$ticket_details_table = "";

		foreach($order->ticketObject as $ticket){
		
			$ticket_details_table .= '

			<tr>
				<td>' . $ticket['details']['event_ticket_name'] . '</td>
				<td>' . $ticket['quantity'] . '</td>
				<td>' . ($ticket['details']['event_ticket_price'] + $ticket['details']['event_ticket_commission']) . '</td>
			</tr>

			';

		}


		$buildPage->addToBlock('ticket_details_table', $ticket_details_table);
		$buildPage->addToBlock('orderTotal', $order->order_total);

	}

	// VENUE

	if(!empty($order->eventDateObject->venue)){

	

		$buildPage->addToBlock("event_details_venue_name", $order->eventDateObject->venue['name']);
		$buildPage->addToBlock("event_details_venue_address", $order->eventDateObject->venue['address']);

	}



	


	//ADDRESS

	if($order->getBuyerObject()){

		$buyerDetails = $order->getBuyerObject();

		if($buyerDetails['buyer_first_name'] != ""){
			
			$buildPage->addToBlock("buyer_first_name", $buyerDetails['buyer_first_name']);

		}else{
			$buildPage->addToBlock("buyer_first_name", "");

		}		

		if($buyerDetails['buyer_last_name'] != ""){
			
			$buildPage->addToBlock("buyer_last_name", $buyerDetails['buyer_last_name']);

		}else{
		$buildPage->addToBlock("buyer_last_name", "");

		}		

		if($buyerDetails['buyer_email'] != ""){
			
			$buildPage->addToBlock("buyer_email", $buyerDetails['buyer_email']);

		}else{
			$buildPage->addToBlock("buyer_email", "");

		}

	}

	//ORDER

	if(isset($order->order_payment_status) and $order->order_payment_status == "succeeded"){	

		$buildPage->addToBlock("order_number", $order->order_number['ticketNumber']);
		$buildPage->addToBlock("order_payment_status", $order->order_payment_status);
		$buildPage->addToBlock("ticket_print_url", "./?ticket/" . $order->order_number['ticketNumber'] . "/" . $order->buyerObject['buyer_email'] . "/getTicketPdf");
		

	}
	
<?php
ob_start();
// require_once("./vendors/dompdf/autoload.inc.php");
// use Dompdf\Dompdf;
// use Dompdf\Options;



$ticketNumber = clearPost($_GET[1]);
$buyerEmail = clearPost($_GET[2]);

require_once("./vendors/mpdf60/mpdf.php");
$mpdf=new mPDF('utf-8', 'A4', 0, '', 0, 0, 0, 0, 0, 0);


if(!empty($ticketNumber) and !empty($buyerEmail)){

	$tickets = getTicketDetailsForBuyer($ticketNumber, $buyerEmail);

	if($tickets['email'] == $buyerEmail){

		$order = decodeObject($tickets['order_object']);



		//print ticket
		if(isset($_GET[3]) and $_GET[3] == "getTicketPdf"){ 


			//prepare ticket content

			$ticketTemplate = file_get_contents("./ticketTemplates/print_ticket.phtml");

			$i = 1;
			
			$totalQuantity = 0;

			$ticketCount = 1;


			foreach($order->ticketObject as $ticket){

			$ticketId = $order->order_number['ticketNumberShort'];
		
				
			
			$totalQuantity += $ticket['quantity'];



				

			
				for($t = 1; $t <= $ticket['quantity']; $t++ ){

					$ticket_details_table = '
						<table>
						<tr>
							<td>Ticket type</td><td>' . $ticket['details']['event_ticket_name'] . '</td>
						</tr>
						<tr>
							<td>Price</td><td>' . ($ticket['details']['event_ticket_price'] + $ticket['details']['event_ticket_commission']) . ' &euro;</td>
						</tr>
						<tr>
							<td>Ticket number</td><td><b>' . $ticketId . '-' . $ticketCount . '</b></td>
						</tr>
						</table>

					';


					$replace = array(
						"<<<{ticketstop_logo}>>>" => "./images/page/ticketslogo.png",
						"<<<{ticket_background}>>>" => "./images/page/ticketsbg.png",
						"<<<{buyer_name}>>>" => $order->buyerObject['buyer_first_name']. " " . $order->buyerObject['buyer_last_name'],
						"<<<{buyer_email}>>>" => $order->buyerObject['buyer_email'],
						"<<<{event_tickets}>>>" => $ticket_details_table,
						"<<<{page_url}>>>" => $buildPage->settings['page_url'],
						"<<<{ticket_ids}>>>" => $ticketId . '-' . $ticketCount,
						"<<<{accessmsg}>>>" => "This ticket permits access for 1 person",

						"<<<{event_title}>>>" => $order->eventDateObject->eventDetails['title'],
						"<<<{order_number}>>>" => $order->order_number['ticketNumber'],
						"<<<{order_total}>>>" => $order->order_total . "&euro; (" . $ticket['quantity'] . " x " . ($ticket['details']['event_ticket_price'] + $ticket['details']['event_ticket_commission'])  . ")",
						"<<<{date_time}>>>" => $order->eventDateObject->eventDateDetails['date'] . " " . $order->eventDateObject->eventDateDetails['time'],
						"<<<{venue_name}>>>" => $order->eventDateObject->venue['name'],
						"<<<{venue_address}>>>" => $order->eventDateObject->venue['address'],
						"<<<{event_date_date}>>>" => $order->eventDateObject->eventDateDetails['date'],
						"<<<{event_date_time}>>>" => $order->eventDateObject->eventDateDetails['time'],
						"<<<{print_ticket_url}>>>" => "./?ticket/" . $order->order_number["ticketNumber"] . "/" . $toEmail

					);

					$ticketContent = replace_texts($replace, $ticketTemplate);


					$mpdf->addPage();
					$mpdf->WriteHTML($ticketContent);

					$ticketCount++;
					
				}


				
			}
		$mpdf->Output();
			
		}


	}else{
		$_SESSION['info'] = "Ticket not found";

		$template_name = "template_error";
	}





}else{
		$_SESSION['info'] = "Ticket not found";

		$template_name = "template_error";
}



 
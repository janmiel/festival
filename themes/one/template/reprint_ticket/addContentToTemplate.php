<?php
// BODY CLASS
$bodyClasses = 'fixed-navbar';
$buildPage->addToBlock('body_classes' , $bodyClasses);

// NAVBAR
$navbarClasses = 'navbar-fixed-top';
$buildPage->addToBlock('navbar_classes', $navbarClasses);

// SCRIPT

$script = ' <script src="https://www.google.com/recaptcha/api.js"></script>';
$buildPage->addToBlock("template_footer_after", $script);


// INIT VALUE

$buildPage->addToBlock('email');
$buildPage->addToBlock('order_number');

// ERROR HANDLIGN

$buildPage->addBlock('error_order_number');
$buildPage->addBlock('error_email');




if(isset($error)){

	if(isset($error['error_order_number'])){
		$buildPage->addToBlock('error_order_number', "Field cannot be empty");
	}

	if(isset($error['error_email'])){
		$buildPage->addToBlock('error_email', "Email not correct or empty");
	}



}

	

if(isset($_SESSION['form'])){



	$buildPage->addToBlock("order_number", $_SESSION['form']['order_number']);
	$buildPage->addToBlock("email", $_SESSION['form']['email']);


}







<?php

// BODY CLASS
	$bodyClasses = 'fixed-navbar';
	$buildPage->addToBlock('body_classes' , $bodyClasses);

// NAVBAR
	$navbarClasses = 'navbar-fixed-top';
	$buildPage->addToBlock('navbar_classes', $navbarClasses);

// pageContent contains all data collected by "prepareContent.php"

$buildPage->addBlock("buyer_first_name_validation_state");
$buildPage->addBlock("buyer_last_name_validation_state");
$buildPage->addBlock("buyer_email_validation_state");
$buildPage->addBlock("buyer_first_name");
$buildPage->addBlock("buyer_last_name");
$buildPage->addBlock("buyer_email");

// ERROR
	if(isset($error)){
		$buildPage->addToBlock("error", '<span class="text-danger">' . $error . '</span>');
	}


// IMAGES
	if(isset($order->eventDateObject->pictures['defaultEvent'])){
		
		$event_picture = $order->eventDateObject->pictures['pictureEvent'][$order->eventDateObject->pictures['defaultEvent']]['url']; 
		$event_picture_thumb = $order->eventDateObject->pictures['pictureEvent'][$order->eventDateObject->pictures['defaultEvent']]['urlThumb']; 
		$event_picture_medium = $order->eventDateObject->pictures['pictureEvent'][$order->eventDateObject->pictures['defaultEvent']]['urlMedium']; 
		$event_picture_crop = $order->eventDateObject->pictures['pictureEvent'][$order->eventDateObject->pictures['defaultEvent']]['urlCrop']; 

	}elseif(isset($order->eventDateObject->pictures['default'])){

		$event_picture = $order->eventDateObject->pictures['picture'][$order->eventDateObject->pictures['default']]['url']; 
		$event_picture_thumb = $order->eventDateObject->pictures['picture'][$order->eventDateObject->pictures['default']]['urlThumb']; 
		$event_picture_medium = $order->eventDateObject->pictures['picture'][$order->eventDateObject->pictures['default']]['urlMedium']; 
		$event_picture_crop = $order->eventDateObject->pictures['picture'][$order->eventDateObject->pictures['default']]['urlCrop']; 

	}elseif(isset($order->eventDateObject->pictures['pictureEvent'][0]['url'])){

		$event_picture = $order->eventDateObject->pictures['pictureEvent'][0]['url']; 
		$event_picture_thumb = $order->eventDateObject->pictures['pictureEvent'][0]['urlThumb']; 
		$event_picture_medium = $order->eventDateObject->pictures['pictureEvent'][0]['urlMedium']; 
		$event_picture_crop = $order->eventDateObject->pictures['pictureEvent'][0]['urlCrop']; 
		

	}else{

		$event_picture = $order->eventDateObject->pictures['no_image'][0]['default_1200']; 
		$event_picture_thumb = $order->eventDateObject->pictures['no_image'][0]['default_600']; 
		$event_picture_medium = $order->eventDateObject->pictures['no_image'][0]['default_800']; 
		$event_picture_crop = $order->eventDateObject->pictures['no_image'][0]['default_2_1']; 

	}

	$event_picture_social = $event_picture_medium;

	if(file_exists("." . $event_picture_crop)){
	
		$event_picture = $event_picture_crop;
		$event_picture_social = $event_picture_crop;
	}

	$buildPage->addToBlock('event_picture', $event_picture);

// EVENT DETAILS

	$event_details_title = $order->eventDateObject->eventDetails['title'];



	$buildPage->addToBlock("event_details_title",$event_details_title);



// DESCRIPTION

	$buildPage->addToBlock("event_details_description", $order->eventDateObject->eventDetails['description']);



//DATES

	$event_details_dates = "";
	$event_details_dates_array = array();

	
	

	if(!empty($order->eventDateObject->eventDateDetails['date'])){

			$buildPage->addToBlock('event_details_day_name', getDateForMe("dayName", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_day', getDateForMe("getDay", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_month_name', getDateForMe("getMonthName", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_year', getDateForMe("year", null, $order->eventDateObject->eventDateDetails['date']));
			$buildPage->addToBlock('event_details_time', $order->eventDateObject->eventDateDetails['time']);

			}

	// TICKET

	if(!empty($order->ticketObject)){

		$ticket_details_table = "";

		foreach($order->ticketObject as $ticket){
		
			$ticket_details_table .= '

			<tr>
				<td>' . $ticket['details']['event_ticket_name'] . '</td>
				<td>' . $ticket['quantity'] . '</td>
				<td>&euro;' . ($ticket['details']['event_ticket_price'] + $ticket['details']['event_ticket_commission'] ) . '</td>
			</tr>

			';

		}


		$buildPage->addToBlock('ticket_details_table', $ticket_details_table);
		$buildPage->addToBlock('orderPaymentFee', $order->order_payment_fees);
		$buildPage->addToBlock('orderTotal', $order->order_total + $order->order_payment_fees);

	}

	// VENUE

	if(!empty($order->eventDateObject->venue)){

	

		$buildPage->addToBlock("event_details_venue_name", $order->eventDateObject->venue['name']);
		$buildPage->addToBlock("event_details_venue_address", $order->eventDateObject->venue['address']);

	}else{

		$buildPage->addToBlock("event_details_venue_name", "");
		$buildPage->addToBlock("event_details_venue_address", "");


	}



	


	//ADDRESS

	if($order->getBuyerObject()){

		$buyerDetails = $order->getBuyerObject();

		if($buyerDetails['buyer_first_name'] != ""){
			
			$buildPage->addToBlock("buyer_first_name", $buyerDetails['buyer_first_name']);
			$buildPage->addToBlock("buyer_first_name_validation_state", "valid");

		}else{
			$buildPage->addToBlock("buyer_first_name", "");
			$buildPage->addToBlock("buyer_first_name_validation_state", "invalid");

		}		

		if($buyerDetails['buyer_last_name'] != ""){
			
			$buildPage->addToBlock("buyer_last_name", $buyerDetails['buyer_last_name']);
			$buildPage->addToBlock("buyer_last_name_validation_state", "valid");

		}else{
		$buildPage->addToBlock("buyer_last_name", "");
			$buildPage->addToBlock("buyer_last_name_validation_state", "invalid");

		}		

		if($buyerDetails['buyer_email'] != ""){
			
			$buildPage->addToBlock("buyer_email", $buyerDetails['buyer_email']);
			$buildPage->addToBlock("buyer_email_validation_state", "valid");

		}else{
			$buildPage->addToBlock("buyer_email", "");
			$buildPage->addToBlock("buyer_email_validation_state", "invalid");

		}

	}

	//PAYMENT

	if($order->order_payment_status != "succeeded"){
		

		if($order->order_total == 0){

			$payment_form = '

				<p class="green-text red">Payment is not required for free events. Please press "Get Ticket" to complete order.</p>

				<form method="post" action="?checkout/payment" id="payment-form">
					
						<button type="submit" name="free_event" class="btn-buy-white pull-right">Get Ticket</button>
			</form>	

			';
			$buildPage->addToBlock("payment_form", $payment_form);

		}else{

			$payment_form = '

			<h4>PAYMENT</h4>
				<p>We use STRIPE to transfer money between the buyer and the seller securely. You can pay with Credit or Debit Card using STRIPE.</p>

			<form method="post" action="?checkout/payment" id="payment-form">
					<script src="https://js.stripe.com/v3/"></script>

						<div class="form-row">
						    <label for="card-element">
						      Credit or debit card
						    </label>
						    <div id="card-element">
						      <!-- a Stripe Element will be inserted here. -->
						    </div>

						    <!-- Used to display form errors -->
						    <div id="card-errors"></div>
						</div>

						<button type="submit" class="btn-buy-white pull-right">Buy</button>
			</form>	
							
						  	<script>
								// Create a Stripe client
								var stripe = Stripe("' . $buildPage->settings['stripe_pk'] . '");

								// Create an instance of Elements
								var elements = stripe.elements();

								// Create an instance of the card Element
								var card = elements.create("card");

								// Add an instance of the card Element into the `card-element` <div>
								card.mount("#card-element");

								// Handle real-time validation errors from the card Element.
								card.addEventListener("change", function(event) {
								  const displayError = document.getElementById("card-errors");
								  if (event.error) {
								    displayError.textContent = event.error.message;
								  } else {
								    displayError.textContent = "";
								  }
								});

								function stripeTokenHandler(token) {
								  // Insert the token ID into the form so it gets submitted to the server
								  var form = document.getElementById("payment-form");
								  var hiddenInput = document.createElement("input");
								  hiddenInput.setAttribute("type", "hidden");
								  hiddenInput.setAttribute("name", "stripeToken");
								  hiddenInput.setAttribute("value", token.id);
								  form.appendChild(hiddenInput);

								  // Submit the form
								  form.submit();
								}

								function createToken() {
								  stripe.createToken(card).then(function(result) {
								    if (result.error) {
								      // Inform the user if there was an error
								      var errorElement = document.getElementById("card-errors");
								      errorElement.textContent = result.error.message;
								    } else {
								      // Send the token to your server
								      stripeTokenHandler(result.token);
								    }
								  });
								};

								// Create a token when the form is submitted.
								var form = document.getElementById("payment-form");
								form.addEventListener("submit", function(e) {
								  e.preventDefault();
								  createToken();
								});
						  	</script>
			';
			$buildPage->addToBlock('payment_form', $payment_form);
		}

	}else{
		$buildPage->addToBlock('payment_form', "Payment alredy completed");
	}

	//ORDER

	if(isset($order->order_payment_status) and $order->order_payment_status == "succeeded"){


		$buildPage->addToBlock("order_number", $order->order_number['ticketNumber']);
		$buildPage->addToBlock("ticket_print_url", "./?ticket/" . $order->order_number['ticketNumber'] . "/" . $order->buyerObject['buyer_email'] . "/getTicketPdf");

	}

//showVar($order);



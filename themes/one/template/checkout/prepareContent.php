<?php

if($_GET[0] == "checkout" and (!isset($_GET[1]) or $_GET[1] == "")){


	if(isset($_POST['event_date']) and isset($_POST['event_ticket'])){
		$event_date = $_POST['event_date'][0];
		$event_ticket_array = $_POST['event_ticket'];
		$tickets_quantity = $_POST['tickets_quantity'];


		$event_date_details = getEventDate($event_date);

		if($event_date_details){


			$t_q = 0;

			
			$order = new Order();

			$order->setOrderId();

			$idorder = $order->getOrderId();

			$order->setEventDateId($event_date);

			$order->setEventDateObject(new eventDetails($event_date));

			foreach($event_ticket_array as $key => $event_ticket){
				
				
				$event_ticket_details = getTicketDetails($event_ticket);


				//check if event_date exists
				if($event_ticket_details){

					$ticket_q = ($tickets_quantity[$key] == "" ? 0 : $tickets_quantity[$key]);

					if($ticket_q > 0){
						$t_q++;

						$ticket_price = $event_ticket_details['event_ticket_price'] + $event_ticket_details['event_ticket_commission'];
						

						$tmp_ticket = array(
							"details" => $event_ticket_details,
							"quantity" => $ticket_q,
							"total_ticket_type_price" => $ticket_q * $ticket_price
							);
						
						$order->setTicketObject($tmp_ticket);
					}

					

					//$order->setEventObject($event_date_details)
					


				}
			}

			if($t_q > 0){
			
				$redirectUrl = '?checkout/address';
				$redirectLater = true;

			}else{
				$_SESSION['info'] = "Tickets Quantity not selected";
				
				$redirectUrl = $order->eventDateObject->eventDateDetails['url'];
				redirectTo($redirectUrl);
			}
		}else{
			$redirectUrl = 'index.html';
			redirectTo($redirectUrl);
		}

	}else{
		$redirectUrl = 'index.html';
		redirectTo($redirectUrl);
	}
}

	
//if order already exists

if(isset($order) and isset($_GET[1]) and $_GET[1] != "" ){


	//$pageContent['eventDetails'] = new eventDetails($order->getEventDateId());
	//$pageContent['ticketDetails'] = getTicketDetails($order->getEventTicket());
	//$pageContent['ticketQuantity'] = $tickets_quantity;



	$commission = $order->calculateCommission();
	
	$total = $order->calculateTotal();

	

	
	$order->setOrderCommission($commission);
	$order->setOrderTotal($total);

	$stripe_fees = calculateStripeFees($order->order_total); // can be done only if total is knownw

	$order->order_payment_fees = $stripe_fees;
	

	if($_GET[0] == "checkout" and (!isset($_GET[1]) or $_GET[1] == "")){


		$redirectUrl = '?checkout/address';
		$redirectLater = true;


	}elseif($_GET[0] == "checkout" and isset($_GET[1]) and $_GET[1] == "address"){

			//validate billing address
			$validate_address = false;

			$buyer = array();

			if(empty($_POST)){

				if(!empty($order->buyerObject)){

					$buyer = $order->buyerObject;

					
				}

				}else{

					if(isset($_POST['buyer_first_name']) and $_POST['buyer_first_name'] != ""){
						$buyer['buyer_first_name'] = clearPost($_POST['buyer_first_name']);			
						$validate_address = true;
					}else{
						$validate_address = false;
					}
					
					if(isset($_POST['buyer_last_name']) and $_POST['buyer_last_name'] != ""){
						$buyer['buyer_last_name'] = clearPost($_POST['buyer_last_name']);			
						$validate_address = $validate_address;
					}else{
						$validate_address = false;
					}

					if(isset($_POST['buyer_email']) and $_POST['buyer_email'] != ""){
						$buyer['buyer_email'] = clearPost($_POST['buyer_email']);			
						$validate_address = $validate_address;
					}else{
						$validate_address = false;
					}
				}

			

			if($validate_address == true){

				$buyer['validate_address'] = true;
				$redirectUrl = "?checkout/payment";
				$redirectLater = true;

			}

			$order->setBuyerObject($buyer);	

			$template_name = "address";


			$pageContent["order"] = $order;


	}elseif($_GET[0] == "checkout" and isset($_GET[1]) and $_GET[1] == "payment" and $order->buyerObject['validate_address'] == true){

		

			// if event / ticket is free, skip payment

			if(isset($_POST['free_event']) and $order->order_total == 0){

				// finalize payment
					require_once("./functionalSites/process_payment.php");

			}

			// if event is not free process payment

			if(isset($_POST['stripeToken'])){

				$order->setPaymentObject("stripe", "stripeToken", clearPost($_POST['stripeToken']));
				//$order->setPaymentObject("stripe", "stripeTokenType", clearPost($_POST['stripeTokenType']));
				//$order->setPaymentObject("stripe", "stripeEmail", clearPost($_POST['email']));
				

				// finalize payment
					require_once("./functionalSites/process_payment.php");
						

					$template_name = "payment";
			}

			$template_name = "payment";
			
	

	}elseif(
			$_GET[0] == "checkout" and isset($_GET[1]) and $_GET[1] == "success" and 
			(
				(
				isset(
					$order->paymentObject['stripe']['stripeChargeObject']['paid']
					) and $order->paymentObject['stripe']['stripeChargeObject']['paid'] == true
				) or $order->order_total == 0
			)
			
	){

		$template_name = "success";

		$buildPage->addToBlock("order_payment_status", $order->getPaymentStatus());

	}else{
		$redirectUrl = '?checkout';
		$redirectLater = true;
	}


}else{
		$_SESSION['info'] = "Payment already completed";

		
	}


//showVar($order);




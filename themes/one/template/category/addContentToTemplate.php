<?php

// pageContent contains all data collected by "prepareContent.php"

// ADVERT

	//$buildPage->addToBlock("adverts_banner", $advertsBanner);

// HERO 

	$hero = '<div class="hero" style="background-image: url(\'images/page/hero_ballet_1920px.jpg\');">';
	$buildPage->addToBlock("hero-div", $hero);

	$endHero = '
	  <div class="hero-content hidden-xs hidden-sm">
         <h1> <<<{category_title}>>> </h1>
         <h2></h2>
       </div>

     </div>';
	$buildPage->addToBlock("hero-div-end", $endHero);
	

// NAVBAR CLASSES

	$buildPage->addToBlock("navbar_classes", "navbar-default");


// CATEGORY TITLE

	$buildPage->addToBlock('category_title', $pageContent['category']['name']);
     

// EVENT DETAILS 


	if(!empty($pageContent['eventDetails'])){

		$event_details = "";
		foreach($pageContent['eventDetails'] as $eventDetails){

			$event_details .= '
			<div class="event">
			        <div class="block">
			          <div class="row">
			            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 column">
			            ';
			            if(isset($eventDetails->pictures['defaultEvent'])){

							$event_picture = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['url']; 
							$event_picture_thumb = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['urlThumb']; 
							$event_picture_medium = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['urlMedium']; 
							$event_picture_crop = $eventDetails->pictures['pictureEvent'][$eventDetails->pictures['defaultEvent']]['urlCrop']; 
						}else{

							$event_picture = $eventDetails->pictures['pictureEvent'][0]['url']; 
							$event_picture_thumb = $eventDetails->pictures['pictureEvent'][0]['urlThumb']; 
							$event_picture_medium = $eventDetails->pictures['pictureEvent'][0]['urlMedium']; 
							$event_picture_crop = $eventDetails->pictures['pictureEvent'][0]['urlCrop']; 

						}

						

						if(file_exists($event_picture_crop)){
						
							$event_picture = $event_picture_crop;
							
						}
						$event_details .= '
			              <img src="' . $event_picture . '" alt="..." class="img-thumbnail">
			            </div>
			            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 column text-center"> 
			              <!--span class="day">' . $eventDetails->eventDateDetails['date_day_day'] . '</span> 
			              <span class="date">' . $eventDetails->eventDateDetails['date_month_name'] . '</span-->
			            </div>
			            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 column">
			             <h3>' . $eventDetails->eventDetails['title'] . ': ' . $eventDetails->eventDateDetails['date_day_day'] . ' ' . $eventDetails->eventDateDetails['date_month_name'] . ' ' . $eventDetails->eventDateDetails['date_year'] . '</h3>
			             <p>' . $eventDetails->venue['name'] . ' ' . $eventDetails->venue['address'] . '</p>
			             <p>' . $eventDetails->eventDateDetails['date'] . ' ' . $eventDetails->eventDateDetails['time'] . '</p>
			           </div>
			         </div>   

			         <div class="row"> 
			          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 column">
			           <a href="' . $eventDetails->eventDateDetails['url'] . '" class="btn-buy pull-right">Buy</a>
			         </div>
			       </div>
			     </div>
			   </div>
			   ';
		} 

		$buildPage->addToBlock('event_details', $event_details);
  	}else{
	  	$buildPage->addToBlock('event_details', '');
	}

		

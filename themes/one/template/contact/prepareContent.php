<?php


if(isset($_POST['contact_form'])){



  $contactName = clearPost($_POST['contact_name']);
  $contactEmail = clearPost($_POST['contact_email']);
  $contactQueryType = clearPost($_POST['contact_query_type']);
  $contactComments = clearPost($_POST['contact_comments']); 

  $_SESSION['contact']['name'] = $contactName;
  $_SESSION['contact']['email'] = $contactEmail;
  $_SESSION['contact']['query_type'] = $contactQueryType;
  $_SESSION['contact']['comments'] = $contactComments;





  if($contactName == ""){
    $_SESSION['error']['error_name'] = false;
  }
  if($contactEmail == "" or !validateEmail($contactEmail)){
    $_SESSION['error']['error_email'] = false;
  }
  // if($contactQueryType == ""){

  //   $_SESSION['error']['error_query_type'] = false;
  // }
  if($contactComments == ""){
    $_SESSION['error']['error_comments'] = false;
  }


  if(!empty($_SESSION['error'])){

     showVar($_SESSION['error']);
   // $redirectUrl = "./?contact";
   // $redirectLater = true;

  }else{

      // RECAPTCHA VALIDATION


      // your secret key
    $privatekey = $buildPage->settings['reCaptchaPrivate'];

      // empty response
    $response = null;

    $url = "https://www.google.com/recaptcha/api/siteverify";
    $post = array(
      "secret" => $privatekey,
      "response" => $_POST["g-recaptcha-response"],
      "remoteip" => $_SERVER["REMOTE_ADDR"]
      );

    $response = curlIt($url, $post);

    $response = json_decode($response);


   
    if ($response->success == true or $response->success == 1) {

      //send mail
					$fromEmail = $contactEmail;
					$fromName = "Ticketstop - contact form";
        
					$toEmail = $buildPage['email_contact_from_send_to']; //$buildPage->settings['contactEmailAddress'];
					$toName = "Ticketstop - contact";

					$emailTemplate = file_get_contents("./mailTemplates/contact_form.phtml");

					$emailContent = array(

						"<<<{contact_name}>>>" => $contactName,
						"<<<{contact_email}>>>" => $contactEmail,
						"<<<{contact_query_type}>>>" => $contactQueryType,
						"<<<{contact_comments}>>>" => $contactComments

					);

					$body = replace_texts($emailContent, $emailTemplate);
										
					$subject = "Ticketstop Contact Form " . $contactQueryType ;

					

					$sendMail = sendMail($fromEmail, $fromName, $subject, $body, $toEmail, $toName);

          if($sendMail){
    		    $_SESSION['info'] = "Email sent successfully";
            $_SESSION['contact'] = array();
          }else{
            $log = array(
              "recaptcha" => $reponse,
              "sendMail" =>  $sendMail,
              "email" => $_SESSION['contact']
              );
            logIt($log, $type = "email");

            $_SESSION['info'] = "Email NOT sent. Please try again.";
          }

  		$redirectUrl = "?/contact";

    } else {

      $redirectUrl = "./?index.html";
      redirectTo($redirectUrl);
    }



  }




}
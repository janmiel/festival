<?php
// BODY CLASS
$bodyClasses = 'fixed-navbar';
$buildPage->addToBlock('body_classes' , $bodyClasses);

// NAVBAR
$navbarClasses = 'navbar-fixed-top';
$buildPage->addToBlock('navbar_classes', $navbarClasses);

// SCRIPT

$script = ' <script src="https://www.google.com/recaptcha/api.js"></script>';
$buildPage->addToBlock("template_footer_after", $script);



$script = "
	<script>
	var text_max = 200;
	$('#count_message').html(text_max + ' remaining');

	$('#textarea_input').keyup(function() {
	  var text_length = $('#textarea_input').val().length;
	  var text_remaining = text_max - text_length;
	  
	  $('#count_message').html(text_remaining + ' remaining');
	});
	</script>
	";

$buildPage->addToBlock("template_footer_after", $script);

// ERROR HANDLIGN

$buildPage->addBlock('error_name');
$buildPage->addBlock('error_email');
$buildPage->addBlock('error_query_type');
$buildPage->addBlock('error_comments');



if(isset($error)){

	if(isset($error['error_name'])){
		$buildPage->addToBlock('error_name', "Field cannot be empty");
	}

	if(isset($error['error_email'])){
		$buildPage->addToBlock('error_email', "Email not correct or empty");
	}

	if(isset($error['error_query_type'])){
		$buildPage->addToBlock('error_query_type', "Field cannot be empty");
	}

	if(isset($error['error_comments'])){
		$buildPage->addToBlock('error_comments', "Field cannot be empty");
	}


}

// PREFILLING FORM

	$buildPage->addBlock('contact_name');
	$buildPage->addBlock('contact_email');
	$buildPage->addBlock('contact_query_type');
	$buildPage->addBlock('contact_comments');
	

if(isset($_SESSION['contact'])){



	$buildPage->addToBlock("contact_name", $_SESSION['contact']['name']);
	$buildPage->addToBlock("contact_email", $_SESSION['contact']['email']);
	$buildPage->addToBlock("contact_query_type", $_SESSION['contact']['query_type']);
	$buildPage->addToBlock("contact_comments", $_SESSION['contact']["comments"]);

}







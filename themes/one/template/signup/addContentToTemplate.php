<?php
// BODY CLASS
$bodyClasses = 'fixed-navbar';
$buildPage->addToBlock('body_classes' , $bodyClasses);

// NAVBAR
$navbarClasses = 'navbar-fixed-top';
$buildPage->addToBlock('navbar_classes', $navbarClasses);

// SCRIPT

$script = ' <script src="https://www.google.com/recaptcha/api.js"></script>';
$buildPage->addToBlock("template_footer_after", $script);



// ERROR HANDLIGN

$buildPage->addBlock('error_first_name');
$buildPage->addBlock('error_last_name');
$buildPage->addBlock('error_phone_number');
$buildPage->addBlock('error_email');
$buildPage->addBlock('error_password');


if(isset($error)){

	if(isset($error['error_first_name'])){
		$buildPage->addToBlock('error_first_name', "Field cannot be empty");
	}

	if(isset($error['error_last_name'])){
		$buildPage->addToBlock('error_last_name', "Field cannot be empty");
	}

	if(isset($error['error_phone_number'])){
		$buildPage->addToBlock('error_phone_number', "Field cannot be empty");
	}

	if(isset($error['error_email'])){
		$buildPage->addToBlock('error_email', "Field cannot be empty");
	}

	if(isset($error['error_password'])){
		$buildPage->addToBlock('error_password', "Field cannot be empty");
	}

}

// PREFILLING FORM

	$buildPage->addBlock('first_name');
	$buildPage->addBlock('last_name');
	$buildPage->addBlock('organisation');
	$buildPage->addBlock('phone_number');
	$buildPage->addBlock('email');
	

if(isset($currentUser) and !isset($_SESSION['logon'])){



	$buildPage->addToBlock("first_name", $currentUser->first_name);
	$buildPage->addToBlock("last_name", $currentUser->last_name);
	$buildPage->addToBlock("organization", $currentUser->organization);
	$buildPage->addToBlock("phone_number", $currentUser->phone_number);
	$buildPage->addToBlock("email", $currentUser->email);

}







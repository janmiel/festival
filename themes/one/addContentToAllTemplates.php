<?php
// CREATE ALL ELEMENTS
  $buildPage->addBlock('adverts_banner');
  $buildPage->addBlock('body_classes');
  $buildPage->addBlock('block_before_footer');
  $buildPage->addBlock('template_footer');
  $buildPage->addBlock('debug');
  $buildPage->addBlock('search');
  $buildPage->addBlock('error');
  $buildPage->addBlock('base_url');
  //social media
  $buildPage->addBlock('social_url');
  $buildPage->addBlock('social_image_url');
  $buildPage->addBlock('social_image_height');
  $buildPage->addBlock('social_image_width');


//  BASE URL
  $buildPage->addToBlock("base_url", $buildPage->settings['page_url']);

// BODY CLASSES
  $buildPage->addToBlock("body_classes", "");

// LOGO AND MENU
$logo = '<a class="navbar-brand" href="index.html"><div class="logo"><img src="' . $buildPage->settings['logo'] . '" alt=""></div></a>';

$buildPage->addToBlock('logo', $logo);

$menu = '
<nav class="navbar navbar-default <<<{navbar_classes}>>> " data-spy="affix" data-offset-top="80">     
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>' .
            $logo . '
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="index.html">Home</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
                <ul class="dropdown-menu">';

                $categories = getCategries();
                foreach($categories as $category){
                  $menu .= '<li><a href="?category/' . $category['id'] . '/' . $category['name'] . '">' . $category['name'] . '</a></li>';
                }
              $menu .= '
                </ul>
              </li>
              <li><a href="reprint-ticket">Reprint Tickets</a></li>
              <li><a href="faq">FAQ</a></li>
              <li><a href="about-us">About US</a></li>      
              <li><a href="sell-with-us">Sell with US</a></li>                
              <li><a href="signup">Sign Up</a></li>                
              <li><a href="contact">Contact</a></li>                
              <li><a href="login">Log-In</a></li>
              <!--li class="hidden-xs "><a href="search.html"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a></li-->  
              <!--li class="hidden-sm hidden-md hidden-lg"><a href="search.html">Search</a></li-->
            </ul>
          </div><!--/.nav-collapse -->
</nav>


';
$buildPage->addToBlock("menu", $menu);
////////////////////////////////////////

// HERO DIV 
$buildPage->addBlock("hero-div");
$buildPage->addBlock("hero-div-end");


// ADVERT -> DECLARATION, INDIVIDUALLY ADDED IN INDIVIDUAL TEMPLATES
  $advertsBanner = '<div id="news-ticker" class="news-ticker-container">
        <div class="name">Official Ed Sheeran Ticket Resale <a href="event.html" class="btn-buy">Buy now</a></div>
      </div> ';

//INFO

$buildPage->addBlock('info');

if(isset($_SESSION['info'])){

  $info_message = $_SESSION['info'];

  unset($_SESSION['info']);

  $info_div = ' 
    <div class="row container text-center" style="width:100%;">
      <h4 class="text-danger"> ' . $info_message . '</h4>
    </div>
  ';

  $buildPage->addToBlock('info', $info_div);

}

///////////////////////////////////////////




$afterHeader ='
	<!-- CSS  -->
     <link rel="stylesheet" type="text/css" href="' . $buildPage->getSettings("page_url") . $buildPage->getSettings("theme_path") . '/css/materialize.css" />
    <link rel="stylesheet" type="text/css" href="' . $buildPage->getSettings("page_url") . $buildPage->getSettings("theme_path") . '/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="' . $buildPage->getSettings("page_url") . $buildPage->getSettings("theme_path") . '/css/custom.css" />

    


  ';

$buildPage->addToBlock("afterHeader", $afterHeader);


$template_footer_after = '
  <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
  <script src="'. $buildPage->getSettings("page_url") . $buildPage->getSettings("theme_path") . '/js/materialize.min.js"></script>
  <script src="'. $buildPage->getSettings("page_url") . $buildPage->getSettings("theme_path") . '/js/main.js"></script>
  <script async src="./'. $buildPage->getSettings("page_url") . $buildPage->getSettings("theme_path") . '/js/script.js"></script>

';

$buildPage->addToBlock("template_footer_after", $template_footer_after);

$footer ='
  <div class=" row footer no-padding">
   

    <div class="row ">  
      <div class="container col-sm-12 footer-menu ">
      <b>ABOUT</b>
     
          <a class="px-2" href="./?faq" target="_self">FAQ</a> |
          <a href="./?about-us" target="_self">About US</a> |
          <a href="./?terms-and-conditions" target="_self">T&C</a> |
          <a href="./?sell-with-us" target="_self">Sell with US</a> |
          <a href="./?signup" target="_self">Sign Up</a> |
          <a href="./?contact" target="_self">Contact</a> |
          <a href="./?login" target="_self">Log-In</a> |

        
        
      </div>
    </div>

    <div class="row ">  
       <div class="container col-sm-12 footer-menu ">
          <b>STAY CONNECTED</b>
        
         
          
      </div>
    </div>

    <div class="row">  
      <div class="container col-sm-12">
        <div class="" copyright">
       
      </div>
    </div>

  </div>  
';



// add all default block
$buildPage->addToBlock("footer", $footer);

//recaptcha

$buildPage->addToBlock("reCaptchaSiteKey", $buildPage->settings['reCaptchaSiteKey']);

// social media
















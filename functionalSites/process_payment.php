<?php

if($order->order_total > 0){
	require_once('./vendors/stripe/init.php');

		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here: https://dashboard.stripe.com/account/apikeys
		\Stripe\Stripe::setApiKey($buildPage->settings["stripe_sk"]);

		// Token is created using Stripe.js or Checkout!
		// Get the payment token submitted by the form:
		$token = clearPost($_POST['stripeToken']);

		

		$finalCommission = calculateFinalCommission();
		$orderTotal = $order->order_total + $order->order_payment_fees;

		try {	

				



				
			  // Charge the user's card:
				$charge = \Stripe\Charge::create(
						array(
						  	"amount" => ($orderTotal) * 100,
						  	"currency" => "EUR",
						 	"description" => "Ticketstop payment" . $order->idorder,
						 	"source" => $token,
						 	"application_fee" => $finalCommission * 100,
						),
					 	array(
					 		"stripe_account" => $order->eventDateObject->promoter->idstripe
						)

				);

	 // Charge the user's card:
				// $charge = \Stripe\Charge::create(
				// 		array(
				// 		  	"amount" => (53.65) * 100,
				// 		  	"currency" => "EUR",
				// 		 	"description" => "Ticketstop payment" . $order->order_number['ticketNumber'],
				// 		 	"source" => $token,
				// 		 	"application_fee" => 1.42 * 100,
				// 		),
				// 	 	array(
				// 	 		"stripe_account" => $order->eventDateObject->promoter->idstripe
				// 		)

				// );			


				$charge = $charge->jsonSerialize();

				$_SESSION['charge'] = $charge;



			} catch(\Stripe\Error\Card $e) {

			  // Since it's a decline, \Stripe\Error\Card will be caught
			  $body = $e->getJsonBody();
			  $err  = $body['error'];

			  logIt('Order id is:' . $order->idorder . "\n");
			  logIt('Status is:' . $e->getHttpStatus() . "\n");
			  logIt('Type is:' . $err['type'] . "\n");
			  logIt('Code is:' . $err['code'] . "\n");
			  // param is '' in this case
			  logIt('Param is:' . $err['param'] . "\n");
			  logIt('Message is:' . $err['message'] . "\n");

			  $_SESSION['error'] = "Payment not successful. Please try again.";
			} catch (\Stripe\Error\RateLimit $e) {
			  // Too many requests made to the API too quickly
				logIt('Too many requests: ' . $e);
			} catch (\Stripe\Error\InvalidRequest $e) {
			  // Invalid parameters were supplied to Stripe's API
				logIt('Invalid request: ' . $e);
			} catch (\Stripe\Error\Authentication $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
			} catch (\Stripe\Error\ApiConnection $e) {
			  // Network communication with Stripe failed
			} catch (\Stripe\Error\Base $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
			} catch (Exception $e) {
			  // Something else happened, completely unrelated to Stripe
			}
}


if((isset($charge['id']) and $charge['id'] != "") or $order->order_total == 0 ){



	if($order->order_total == 0){
		$payment_status = "succeeded";
	}else{
		$payment_status = $charge['status'];
	}

	$order->setPaymentStatus($payment_status);

	if(isset($charge['id'])){
		$order->setPaymentObject("stripe", "stripeChargeObject", $charge);
	}

	$orderNumbers = createTicketNumber($order->idevent_date);
	$order->setOrderNumber($orderNumbers);

	$result = createOrder($order);



	if($result != false)
	{


		//save temporary buyer
		saveBuyer($order->buyerObject);

		//send mail
		$fromEmail = $buildPage->settings['email_email'];
		$fromName = "Ticketstop";
		$toEmail = $order->buyerObject['buyer_email'];
		$toName = $order->buyerObject['buyer_first_name'] . " " . $order->buyerObject['buyer_last_name'];

		$emailTemplate = file_get_contents("./mailTemplates/order_confirmation.phtml");

		$emailContent = array(

			"<<<{event_name}>>>" => $order->eventDateObject->eventDetails['title'],
			"<<<{order_number}>>>" => $order->order_number['ticketNumber'],
			"<<<{date_time}>>>" => $order->eventDateObject->eventDateDetails['date'] . " " . $order->eventDateObject->eventDateDetails['time'],
			"<<<{venue_address}>>>" => $order->eventDateObject->venue['name'] . "<br>" . $order->eventDateObject->venue['address'],
			"<<<{order_number}>>>" => $order->order_number['ticketNumber'],
			"<<<{print_ticket_url}>>>" => $buildPage->settings['page_url'] . "/?ticket/" . $order->order_number["ticketNumber"] . "/" . $toEmail

		);

		$body = replace_texts($emailContent, $emailTemplate);
							
		$subject = "Ticketstop Order confirmation " . $order->order_number['ticketNumber'] . " - Please Print This!" ;

		

		$sendMail = sendMail($fromEmail, $fromName, $subject, $body, $toEmail, $toName);


		$redirectUrl = '?checkout/success';
		$redirectLater = true;

	}



}

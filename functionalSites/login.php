<?php
//Include Composer's autoloader
include 'vendors/auth/vendor/autoload.php';

//Import Hybridauth's namespace
use Hybridauth;

// create super user account
if (isset($_POST['create_superuser_account']) and getCmsUsers() == false){

 $id = createID();
 $firstName = strip_tags(trim($_POST['first_name']));
 $lastName = strip_tags(trim($_POST['last_name']));
 $email = strip_tags(trim($_POST['email']));
 $loginnn = $email;
 $passworddd = md5(clearPost($_POST['passworddd'].$loginnn));

 $sql = "insert into cms_users values ('$id','$firstName','$lastName','$loginnn','$passworddd','$email','adminsuperuser','')";
 fireSql($sql , "insert");
 header("Location:backend.php");

}	

if(isset($_POST['getMeIn'])){

 $login = clearPost($_POST['loginnn']);
 $haslo = clearPost($_POST['passworddd']);
 $haslo = encodePassword($haslo . $login);


 if(isset($_GET['full_access'])){
   $sql = "select idusers, users_access_tier , users_loginnn , users_passworddd, users_first_name, users_last_name from cms_users where users_loginnn = '$login'";
   $result = fireSql($sql , "select", true);

   if($result != false)
   {	
    if($result['users_passworddd'] == $haslo){	

     setUserAccess($result, $result['users_access_tier']);		


     $redirectUrl = "promoter.html?build=dashboard";
     $redirectLater = true;
   }
 }
}else{

 $sql = "select idpromoter, password from promoters where email = '$login'";
 $result = fireSql($sql , "select", true);


 if($result != false)
 {  

  if($result['password'] == $haslo){  

   $userProfile = findUserByUserId($result['idpromoter']);


   if(setPromoterAccess($userProfile)){
    $redirectUrl = "promoter.html?build=dashboard";
  }else{
    $redirectUrl = "index.html";
  }

  $redirectLater = true;
}
}


}


}




if(isset($_POST['signup'])){



  $firstName = clearPost($_POST['first_name']);
  $lastName = clearPost($_POST['last_name']);
  $organisation = clearPost($_POST['organisation']);
  $email = clearPost($_POST['email']);
  $phoneNumber = clearPost($_POST['phone_number']);
  $password = clearPost($_POST['password']); 

  $currentUser->first_name = $firstName;
  $currentUser->last_name = $lastName;
  $currentUser->organisation = $organisation;
  $currentUser->email = $email;
  $currentUser->phone_number = $phoneNumber;




  if($firstName == ""){
    $_SESSION['error']['error_first_name'] = false;
  }
  if($lastName == ""){
    $_SESSION['error']['error_last_name'] = false;
  }
  if($organisation == ""){

    $_SESSION['error']['error_organisation'] = false;
  }
  if($email == ""){
    $_SESSION['error']['error_email'] = false;
  }
  if($phoneNumber == ""){
    $_SESSION['error']['error_phone_number'] = false;
  }
  if($password == ""){
    $_SESSION['error']['error_password'] = false;
  }



  if(!empty($_SESSION['error'])){
    $redirectUrl = "./?signup";
    $redirectLater = true;

  }else{

      // RECAPTCHA VALIDATION


      // your secret key
    $privatekey = "6LdZRB8UAAAAAD1Z4J2NxmTPvrch0dtTuD9G-3iZ";

      // empty response
    $response = null;

    $url = "https://www.google.com/recaptcha/api/siteverify";
    $post = array(
      "secret" => $privatekey,
      "response" => $_POST["g-recaptcha-response"],
      "remoteip" => $_SERVER["REMOTE_ADDR"]
    );

    $response = curlIt($url, $post);

    $response = json_decode($response);

    
    if ($response->success == true) {

      $iduser = createID();
      $currentUser->iduser = $iduser;

      $password = encodePassword($password, $email);

      $createUser = createPromoterAccount($uid = "", $uemail = $currentUser->email , $firstname = $currentUser->first_name, $surname = $currentUser->last_name, $uProviderName = "", $user_access_tier = "promoter", $organisation = $currentUser->organisation, $password, $phoneNumber);

      if($createUser){

        $userProfile = findUserByUserId($iduser);


        if(setPromoterAccess($userProfile)){
          $redirectUrl = "promoter.html?build=dashboard";
        }

      }

    } else {

      $redirectUrl = "index.html";
      redirectTo($redirectUrl);
    }



  }




}

var_dump($_GET);

if(isset($_GET['loginWith'])){


 $loginWithProviderName = "";
$loginWith = clearPost($_GET['loginWith']);

 if($loginWith == "Facebook"){
  $loginWithProviderName = "Facebook";

}elseif($loginWith == "Google"){
  $loginWithProviderName = "Google";
}

echo $buildPage->settings['page_url'] . "login?loginWith=" . $loginWithProviderName;

$config = array( 
      "callback" => $buildPage->settings['page_url'] . "login?loginWith=" . $loginWithProviderName,
      "providers" => array(

        
        "Google" => array ( 
          "enabled" => true,
          "keys"    => array ( "id" => "155173678033-9b49ssqdi50mleapujue3mvlc1fjr7kl.apps.googleusercontent.com", "secret" => "hmslYw36936QEIIobNe0hRF8" ), 
          "scope"   => "email, user_about_me"
        ),

        "Facebook" => array ( 
          "enabled" => true,
          "keys"    => array ( "id" => "401398530251367", "secret" => "137ff70371570f695b6cfff91062573a" ),
          'scope'   => 'email, public_profile',
          "trustForwarded" => false
        ),

        "Twitter" => array ( 
          "enabled" => true,
          "keys"    => array ( "key" => "oLPYdxJ4NXpuWK332cqLYuLXS", "secret" => "pidSQ62xCPNrHnKkbebqGwzs7qy5KwiW6Z8NWzXfS2u2g6R7aS" ) 
        ),
      ),
      // If you want to enable logging, set 'debug_mode' to true.
      // You can also set it to
      // - "error" To log only error messages. Useful in production
      // - "info" To log info and error messages (ignore debug messages)
      "debug_mode" => false,
      // Path to file writable by the web server. Required if 'debug_mode' is not false
      "debug_file" => "logh.txt",
);

    try {
      //Feed configuration array to Hybridauth
      $hybridauth = new Hybridauth\Hybridauth($config);

      //Then we can proceed and sign in with Twitter as an example. If you want to use a diffirent provider, 
      //simply replace 'Twitter' with 'Google' or 'Facebook'.

      //Attempt to authenticate users with a provider by name
      $adapter = $hybridauth->authenticate($loginWithProviderName); 

      //Returns a boolean of whether the user is connected with Twitter
      $isConnected = $adapter->isConnected();
   
      //Retrieve the user's profile
      $hybridUserProfile = $adapter->getUserProfile();

      //Inspect profile's public attributes
      var_dump($userProfile);

      //Disconnect the adapter 
      $adapter->disconnect();
    }catch(\Exception $e){
        echo 'Oops, we ran into an issue! ' . $e->getMessage();
    }




    if(!empty($hybridUserProfile)){
      if(isset($hybridUserProfile->identifier) and !empty($hybridUserProfile->identifier)){
       $hybridUserId = $hybridUserProfile->identifier;
       $hybridUserEmail = $hybridUserProfile->email;
       $hybridUserFirstName = $hybridUserProfile->firstName;
       $hybridUserSureName = $hybridUserProfile->lastName;



       $userProfile = findUserByProviderId($hybridUserId);

       if($userProfile == false){



         $result = createUserAccount($hybridUserId, $hybridUserEmail, $hybridUserFirstName, $hybridUserSureName, $loginWithProviderName, 'new' );

         if($result){

          $userProfile = findUserByProviderId($hybridUserId);



          $currentUser = new currentUser($userProfile['idusers']);


        }else{
          $_SESSION['info'] = "Account creation failed. Administrator has been informed";

        }



      }else{

        $currentUser = new currentUser($userProfile['idusers']);

      }


      $hybridauth->logoutAllProviders();




    } 
    }


}
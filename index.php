<?php 
ob_start();
session_start() ;



include('./system/dbc.php');
include('./system/functions.php'); // indludes all client functions for example (send email)

include('./system/db_querys.php'); // indludes all client functions for example (send email)
include('./classes/autoload.php');
include('./system/settings.php'); 
include('./system/url_translator.php');  // 

include("functionalSites/login.php");


include('./system/db_querys_requiring_classes.php'); // indludes all client functions for example (send email)
include('./system/functions_requiring_classes.php');
include('./language/language.php'); // needs improvement
// if no page selected, open default


if(isset($currentUser) and $currentUser->user_access_tier_number > 8){
	
 		error_reporting(1);
 		error_reporting(E_ALL);
 		ini_set('display_errors', '1');
 }else{
 		error_reporting(0);	
 		ini_set('display_errors', '0');
}

// show errors
if(isset($_SESSION['error']) and $_SESSION['error'] != ""){
	
	$error = $_SESSION['error'];
	
	unset($_SESSION['error']);

}

// open page
$sql = "select page_title, page_template, page_description, page_key_words, page_url, page_tags, page_name, page_location from cms_page where page_publish = 1 and ";

if( !isset($page) or $page == "null" or $page == "index"){

	$sql .= "page_hash='" . md5('index.html') . "'";
}else{
	$sql .= "page_hash='$page'";
}


$result = fireSql($sql, "select" , $single = true);


if ($result == false){	
	
	$redirectUrl = $buildPage->getSettings("page_url");
	//redirectTo($redirectUrl);
	
}




  // get page title, description, content, key words, tags
	$page_title =  $result['page_title'];
	$page_template = $result['page_template'];
	$page_description = $result['page_description'];
	$page_key_words = $result['page_key_words'];
	$page_url = $result['page_url'];
	$page_tags = $result['page_tags'];
	$page_content = open_file($result['page_location']);







	
	// add standard blocks for all pages
	
	$buildPage->addBlock("title");
	$buildPage->addBlock("description");
	$buildPage->addBlock("keywords");

	$buildPage->addToBlock("title", $page_title);
	$buildPage->addToBlock("description", $page_description);
	$buildPage->addToBlock("keywords", $page_key_words);

	$buildPage->addBlock("header");
	$buildPage->addBlock("banner");
	$buildPage->addBlock("menu");
	$buildPage->addBlock("content");


	//set template
	$buildPage->setSettings('template' , $page_template);

	//include default functions
	$defaultSettingsFile = "themes/functions.php";
	if(is_file($defaultSettingsFile)){
		include($defaultSettingsFile);
	}

	//build page
	
	// 1. add page blocks for all themes 
	$templateContentForAllThemes = $buildPage->getSettings("theme_path") . '/template/' . $page_template . '/prepareContent.php';

	if(is_file($templateContentForAllThemes)){

		include($templateContentForAllThemes);

	}

	// 2. add default content to all templates

	include($buildPage->getSettings("theme_path")  . "/addContentToAllTemplates.php");

	// 3. add page_template specific blocks content to template (it may override point 2)
	$page_template_file = $buildPage->getSettings("theme_path") . "/template/" . $page_template . "/addContentToTemplate.php";
	
	if(is_file($page_template_file)){
		include($page_template_file);
	}else{
		$redirectUrl= $buildPage->settings['page_url'] . "index.html";
	}


	//set template name
	$buildPage->setSettings("template_name", $template_name);


	// 4. debug  and 

	//$buildPage->addToBlock("debug", print_r($buildPage, true));



	// 5. save classes,
	$saveClasses = true;
	include('./classes/autoload.php');

	// 6. redirect later 
	if(isset($redirectUrl) and isset($redirectLater) and $redirectLater == true){
		redirectTo($redirectUrl, $callback = false);
	}
	
	// 7. include selected template and render

	$buildPage->renderPage($page_template);



?>

<?php
/* this class is responsible for controlling page output */

class eventBuilder {
	 
	//public $idevent;

	public $details = array();
	public $pictures = array();
	public $dates = array();
	public $tickets = array();
	public $promoter = array();
	public $age_info = array();
	
	public $new_dates_temp = array();


	public $settings = array();

	public $ticketsActive = false;
	public $idvenue;



	public function __construct($idevent = null, $ticketsActive = null){



		global $buildPage;

		if($idevent != null){
			$this->addToDetails('idevent', $idevent);


			if($ticketsActive != null){
			$this->ticketsActive = $ticketsActive; 
			}
		


			// DETAILS
			$result = getEventDetails($idevent);


        	if($result != false){

                $this->addToDetails('idevent', $result['idevent']);
                $this->addToDetails('idevent_short', getEventShortId($result['idevent']));
                $this->addToDetails('title', $result['artistname']);
                $this->addToDetails('description', $result['artistdesc']);
                $this->addToDetails('active', $result['active']);
                $this->addToDetails('category', $result['category']);
                $this->addToDetails('organisation', $result['organisation']);
                $this->addToDetails('idpromoter', $result['idpromoter']);
                $this->addToDetails('privacy', $result['privacy']);
                $this->addToDetails('event_url', $result['event_url']);

                $shortId = getEventShortId($this->details['idevent']);
                $this->addToDetails('event_full_url', getRealEventUrl($shortId, $this->details['event_url']));

				$this->addToDetails('idevent_age_info', $result['idevent_age_info']);                
				$this->addToDetails('event_age_info_other_value', $result['event_age_info_other_value']);                
				$event_age_info_value = $result['event_age_info_other_value'];

                $this->addToSettings('event_use_editor', $result['event_use_editor']);

            }

            // AGE INFO

            if(isset($this->details['idevent_age_info'])){

            	$result = getAgeInfo($this->details['idevent_age_info']);

            	if(strtolower($result[0]['name']) == "other"){
            		$event_age_info_value = $event_age_info_value;

            	}else{
            		$event_age_info_value = $result[0]['value'];
            	}

            	$age_info = array(
            		"idevent_age_info" 	=> $result[0]['idevent_age_info'],
            		"name" 				=> $result[0]['name'],
            		"value" 				=> $event_age_info_value,
            		);
            	$this->age_info = $age_info;

            }


            // PROMOTER

            $result = getPromoterDetails($this->details['idpromoter']);

            
            if($result != false){

            	$this->promoter['idpromoter'] = $this->details['idpromoter'];
            	$this->promoter['first_name'] = $result['firstname'];
            	$this->promoter['last_name'] = $result['surname'];
            	$this->promoter['organisation'] = $result['organisation'];
            	$this->promoter['email'] = $result['email'];
            	$this->promoter['phone'] = $result['phone'];
            	$this->promoter['status'] = $result['status'];
            	$this->promoter['user_access_tier'] = $result['user_access_tier'];

            }

            //PICTURES

            $result = getEventImages($idevent);

            if($result != false){

            	$i=0;

            	foreach($result as $res){

            		$picture = array(
					"url" => '' . $buildPage->settings['eventImageLocation']  . $idevent . '/' . $res['event_image_filename'],
					"urlThumb" => '' . $buildPage->settings['eventImageLocation'] . $idevent . '/thumb/' . $res['event_image_filename'],
					"urlMedium" => '' . $buildPage->settings['eventImageLocation'] . $idevent . '/thumb/medium-' . $res['event_image_filename'],
					"urlCrop" => '' . $buildPage->settings['eventImageLocation'] . $idevent . '/thumb/crop-' . $res['event_image_filename'],
					"alt" => $this->details['title']
					
					);

            		if($res['event_image_default'] == 1){
            			$this->addToPictures('default', $i);
            		}

					$this->addToPictures('picture', $picture, "push");

					$i++;

            	}

            }else{
            	$picture = array(
					"default_1200" => '' . $buildPage->settings['eventImageLocation'] . 'default_1200.jpg',
					"default_800" => '' . $buildPage->settings['eventImageLocation'] . 'default_800.jpg',
					"default_600" => '' . $buildPage->settings['eventImageLocation'] . 'default_600.jpg',		
					"default_2_1" => '' . $buildPage->settings['eventImageLocation'] . 'default_2_1.jpg'		
					);

            	$this->addToPictures('no_image', $picture, "push");
            }


            // VENUE
	        if($this->idvenue != ""){

	            $result = getVenue($this->idvenue);



	            if($result != false){

	            		$this->addToVenue('idvenue', $result['idvenue']);
	            		$this->addToVenue('name', $result['name']);
	            		$this->addToVenue('address', $result['address']);
	            		$this->addToVenue('venue_city', $result['venue_city']);
	            	

	            }
	        }


                
            // DATES
            $result = findEventDates($idevent, $option = "upcoming", $groupBy = null, $orderBy = "event_date_date ASC , event_date_time ASC");

            $eventDatesForSave = array();
            
            if($result != false){

            	foreach($result as $res){

					$datesArray = array(
					'idevent_date' => $res['idevent_date'],
					'idevent' => $res['idevent'],
					'event_date_date' => $res['event_date_date'],
					'event_date_time' => $res['event_date_time'],
					'idvenue' => $res['idvenue']
					);

            		//find tickets

            		$tickets = getEventTickets($res['idevent_date'], $ticketsActive = $this->ticketsActive);
            		if($tickets != false){
            			$datesArray['tickets'] = $tickets;
            		}
            		

					$eventDatesForSave['dates'][] = $datesArray;

				}

				//array_sort_by_column($eventDatesForSave['dates'], 'event_date_date');
	
				$this->addToDates("event_date_dates", $eventDatesForSave, "push");
            }

		}
	}





	public function addToDetails($name = null, $value = null ){

		$this->details[$name] = $value;
		
	}

	public function removeFromDetails($name = null){

		unset($this->details[$name]);

	}

	public function getFromDetails($name = null){

		return $this->details[$name];

	}

	public function addToPictures($name = null, $value = null, $push = null ){
		if($push == null){
			$this->pictures[$name] = $value;
		}else{

			$this->pictures[$name][] = $value;
		}
	}

	public function removeFromPicutres($name = null){

		unset($this->pictures[$name]);

	}

	public function addToDates($name = null, $value = null, $push = null ){
		
		if($push == "push"){
			$this->dates[$name][] = $value;
		}else{

		$this->dates[$name] = $value;
		
		}
	}

	public function removeFromDates($name = null, $child1 = null, $child2 = null, $child3 = null, $child4 = null, $child5 = null){
		
		if($child1 == null){
			unset($this->dates[$name]);
		}elseif($child2 == null){
			unset($this->dates[$name][$child1]);
		}elseif($child3 == null){
			unset($this->dates[$name][$child1][$child2]);
		}elseif($child4 == null){
			unset($this->dates[$name][$child1][$child2][$child3]);
		}elseif($child5 == null){
			unset($this->dates[$name][$child1][$child2][$child3][$child4]);
		}else{
			unset($this->dates[$name][$child1][$child2][$child3][$child4][$child5]);
		}

	}

	public function getFromDates($name = null){

		return $this->dates[$name];

	}

	public function addToTickets($name = null, $value = null, $push = null){

		if($push != null){
			$this->tickets[$name][] = $value;
		}else{
			$this->tickets[$name] = $value;
		}
	}

	public function removeFromTickets($name = null, $child1 = null, $child2 = null, $child3 = null, $child4 = null, $child5 = null){

		if($child1 == null){
			unset($this->tickets[$name]);
		}elseif($child2 == null){
			unset($this->tickets[$name][$child1]);
		}elseif($child3 == null){
			unset($this->tickets[$name][$child1][$child2]);
		}elseif($child4 == null){
			unset($this->tickets[$name][$child1][$child2][$child3]);
		}elseif($child5 == null){
			unset($this->tickets[$name][$child1][$child2][$child3][$child4]);
		}else{
			unset($this->tickets[$name][$child1][$child2][$child3][$child4][$child5]);
		}

	}
	public function getFromTickets($name){
		return $this->tickets[$name];
	}

	public function addToSettings($name = null, $value = null ){

		$this->settings[$name] = $value;
		
	}

	public function removeFromSettings($name = null){

		unset($this->settings[$name]);

	}

	





}
<?php

if(!isset($saveClasses)){
	require_once('buildPage.php');
	require_once('currentUser.php');
	require_once('eventBuilder.php');
	require_once('eventDetails.php');
	require_once('order.php');



	// build page is not saved as it has to be rendered every time, 
	// in the future it will be used to cache pages.
	$buildPage = new buildPage();

	$currentIdEvent = null;

	//EVENT BUILDER
	$file_location = "./sessions/event_build_temp/";
	if(isset($_GET['build']) and $_GET['build'] == "events"){
		if(isset($_GET['edit']) and $_GET['edit'] != ""){


			$currentIdEvent = clearPost($_GET['edit']);

			$eventBuilder = new eventBuilder($currentIdEvent);

			$eventBuilder->new_dates_temp = unserialize(open_file($file_location . $currentIdEvent . '.session'));
		


		}else{
			
			$currentIdEvent = null;
			$eventBuilder = new eventBuilder();
			
		}
	} 

	if($currentIdEvent != null and is_file($file_location . $currentIdEvent . '.session') and !isset($_GET['resetEventSession'])){
		
		

		//$eventBuilder = new eventBuilder($currentIdEvent);
//showVar($eventBuilder);
		// dates must be unset
		//unset($eventBuilder->dates['event_date_add_dates']);

	}else{
		
		// if(isset($_SESSION['eventBuilder'])){
		// 	$eventBuilder = unserialize($_SESSION['eventBuilder']);

		// 	if(isset($_GET['edit'])){$currentIdEvent = $_GET['edit'];}
		// 	if(isset($_GET['edit_event_date'])){$currentIdEvent = $_GET['edit_event_date'];}
		// 	if($currentIdEvent != $eventBuilder->details['idevent'] and $currentIdEvent != null){
		// 		//echo "Hi";
		// 		$eventBuilder = new eventBuilder();
		//	}

		// }else{
		// 	$eventBuilder = new eventBuilder();
		// }
		
	}



	// CURRENT USER
	if(isset($_SESSION['iduser'])){
		$currentUser = new currentUser($_SESSION['iduser']);
	}else{
		$currentUser = new currentUser(null);
	}

	// CURRENT ORDER
	if(isset($_SESSION['order'])){
		$order = unserialize($_SESSION['order']);
	}else{
		$order = new order();
	}

	// CURRENT eventDetals
	if(isset($_SESSION['eventDetails'])){
		$eventDetails = unserialize($_SESSION['eventDetails']);
	}else{
		$eventDetails = new eventDetails();
	}


}

// save classes
if(isset($saveClasses) and $saveClasses == true and $dontSaveClass != true ){



	if(!empty($eventBuilder->new_dates_temp)){

		
	//exit();

		saveSession('eventBuilder', $eventBuilder->new_dates_temp, 'event_build_temp' , $eventBuilder->details['idevent']);
	}else{
		
	}
	
	if($currentUser){
		saveSession('currentUser', $currentUser, null, null);
	}	

	if($eventDetails){
		saveSession('eventDetails', $eventDetails, null, null);
	}

	if($order and isset($order->idorder)){
		saveSession('order', $order, 'orders', $order->idorder);

		
	}
}



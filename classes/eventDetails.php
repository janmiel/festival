<?php

// this is event date class

class eventDetails {

	public $eventDetails = array();
	public $eventDateDetails = array();
	public $pictures = array();
	public $tickets = array();
	public $times = array();
	public $venue = array();
	public $promoter = array();
	public $age_info = array();

	public $settings = array();

	public $idevent = "";
	public $idevent_date = "";
	public $event_url = "";
	public $ticketsActive = false;



	public function __construct($idevent_date = null, $ticketsActive = null){

		global $buildPage;
		$this->idevent_date = $idevent_date;

		if($ticketsActive != null){
			$this->ticketsActive = $ticketsActive; 
		}
		
		if($idevent_date != null){
			$this->addToEventDateDetails('idevent_date', $idevent_date);

			$resultDate = getEventDate($this->idevent_date);

			$this->idevent = $resultDate['idevent'];

			
            // EVENT DATE DETAILS

            if($resultDate != false){
            	
            	$this->addToEventDateDetails('idtime', $resultDate['idevent_date_for_time']);
            	$this->addToEventDateDetails('date', $resultDate['event_date_date']);
            	$this->addToEventDateDetails('time', $resultDate['event_date_time']);
            	$this->addToEventDateDetails('status', $resultDate['event_date_status']);
            	$this->addToEventDateDetails('idvenue', $resultDate['idvenue']);
            	$this->addToEventDateDetails('description', $resultDate['event_date_desc']);

            	$event_day_name = getDateForMe("getNameShort", null, $resultDate['event_date_date']);
				$event_day_day = getDateForMe("getDay", null, $resultDate['event_date_date']);
				$event_year = getDateForMe("year", null, $resultDate['event_date_date']);
				$event_month = getDateForMe("month", null, $resultDate['event_date_date']);
				$event_month_name = getDateForMe("getMonthName", null, $resultDate['event_date_date']);

				$this->addToEventDateDetails('date_day_name', $event_day_name);
				$this->addToEventDateDetails('date_day_day', $event_day_day);
				$this->addToEventDateDetails('date_year', $event_year);
				$this->addToEventDateDetails('date_month', $event_month);
				$this->addToEventDateDetails('date_month_name', $event_month_name);


            }

			// EVENT DETAILS
			$result = getEventDetails($this->idevent);


        	if($result != false){

        		$categoryName = getCategory($idCategory = $result['category']);	

                $this->addToEventDetails('idevent', $result['idevent']);
                $this->addToEventDetails('title', $result['artistname']);
                $this->addToEventDetails('event_url', $result['event_url']);
                $this->addToEventDetails('description', $result['artistdesc']);
                $this->addToEventDetails('active', $result['active']);
                $this->addToEventDetails('category', $result['category']);
                $this->addToEventDetails('categoryName', $categoryName['name']);
                $this->addToEventDetails('organisation', $result['organisation']);
                $this->addToEventDetails('idpromoter', $result['idpromoter']);
                $this->addToEventDetails('privacy', $result['privacy']);
                $this->addToEventDetails('idevent_age_info', $result['idevent_age_info']);
                $event_age_info_value = $result['event_age_info_other_value'];

                
                $this->addToSettings('event_use_editor', $result['event_use_editor']);

            }

            // PROMOTER

            	$this->promoter = new currentUser($this->eventDetails['idpromoter']);

             // AGE INFO

            if(isset($this->eventDetails['idevent_age_info'])){

            	$result = getAgeInfo($this->eventDetails['idevent_age_info']);

            	if(strtolower($result[0]['name']) == "other"){
            		$event_age_info_value = $event_age_info_value;

            	}else{
            		$event_age_info_value = $result[0]['value'];
            	}

            	$age_info = array(
            		"idevent_age_info" 	=> $result[0]['idevent_age_info'],
            		"name" 				=> $result[0]['name'],
            		"value" 				=> $event_age_info_value
            		);
            	$this->age_info = $age_info;

            }

				//URL

				$event_short_id =  getEventShortId($this->idevent);
				$this->addToEventDateDetails('url', './?event/' . $event_short_id . '/' . $this->eventDetails['event_url']);





            //PICTURES (EVENT AND EVENTT DATE)

            $result = getEventImages($this->idevent);

            if($result != false){

            	$i=0;

            	foreach($result as $res){

            		$picture = array(
					"url" => '.' . $buildPage->settings['eventImageLocation']  . $this->idevent . '/' . $res['event_image_filename'],
					"urlThumb" => '.' . $buildPage->settings['eventImageLocation'] . $this->idevent . '/thumb/' . $res['event_image_filename'],
					"urlCrop" => '.' . $buildPage->settings['eventImageLocation'] . $this->idevent . '/thumb/crop-' . $res['event_image_filename'],
					"urlMedium" => '.' . $buildPage->settings['eventImageLocation'] . $this->idevent . '/thumb/medium-' . $res['event_image_filename'],
					
					"alt" => $this->eventDetails['title']
					
					);

            		if($res['event_image_default'] == 1){
            			$this->addToPictures('defaultEvent', $i);
            		}

					$this->addToPictures('pictureEvent', $picture, "push");

					$i++;

            	}

            }else{
            	$picture = array(
					"default_1200" => '.' . $buildPage->settings['eventImageLocation'] . 'default_1200.jpg',
					"default_800" => '.' . $buildPage->settings['eventImageLocation'] . 'default_800.jpg',
					"default_600" => '.' . $buildPage->settings['eventImageLocation'] . 'default_600.jpg',		
					"default_2_1" => '.' . $buildPage->settings['eventImageLocation'] . 'default_2_1.jpg'		
					);

            	$this->addToPictures('no_image', $picture, "push");
            }

            $result = getEventImages($this->idevent, $this->idevent_date);

            if($result != false){

            	$i=0;

            	foreach($result as $res){

            		$picture = array(
					"url" => '.' . $buildPage->settings['eventImageLocation']  . $this->idevent . '/' . $res['event_image_filename'],
					"urlThumb" => '.' . $buildPage->settings['eventImageLocation'] . $this->idevent . '/thumb/' . $res['event_image_filename'],
					"alt" => $this->details['title']
					
					);

            		if($res['event_image_default'] == 1){
            			$this->addToPictures('defaultEventDate', $i);
            		}

					$this->addToPictures('pictureEventDate', $picture, "push");

					$i++;

            	}

            }

                
            
            
            // TICKETS

            $result = getEventTickets($this->idevent_date, $this->ticketsActive);

            $eventTicketTotal = $eventTicketTotal_commission = 0;

  

            if($result != false){

            	foreach($result as $res){

            		$soldTickets = getNumberSoldTickets($this->eventDateDetails['idevent_date'], $res['idevent_ticket']);

            		$ticket = array(
            		"idticket" => $res['idevent_ticket'],
					"name" => $res['event_ticket_name'],
					"quantity" => $res['event_ticket_quantity'],
					"price" => $res['event_ticket_price'],
					'currency' => $res['event_ticket_currency'],
					"description" => $res['event_ticket_description'],
					"commission" => $res['event_ticket_commission'],
					"active" => $res['event_ticket_active'],
					"settings" => array(
						"more_info" => $res['event_ticket_settings_more_info'],
						"delay" => $res['event_ticket_settings_delay'],
						"delay_days" => $res['event_ticket_settings_delay_days'],
						"delay_hours" => $res['event_ticket_settings_delay_hours'],
						"delay_timestamp" => $res['event_ticket_settings_delay_timestamp'],
						),
					"sold" => $soldTickets
					);
					
					$this->addToTickets("ticket", $ticket, "push");

					$eventTicketTotal += ($ticket['sold'] * $ticket['price']);
					$eventTicketTotal_commission += ($ticket['sold'] * ($ticket['price'] + $ticket['commission'])); 

            	}

            	$this->addToEventDateDetails('ticketTotal_no_commission', $eventTicketTotal);
            	$this->addToEventDateDetails('ticketTotal_with_commission', $eventTicketTotal_commission);

            }





            // VENUE

            $result = getVenue($this->eventDateDetails['idvenue']);



            if($result != false){

            		$this->addToVenue('idvenue', $result['idvenue']);
            		$this->addToVenue('name', $result['name']);
            		$this->addToVenue('address', $result['address']);
            		$this->addToVenue('city', $result['venue_city']);
            		$this->addToVenue('county', $result['venue_county']);
            	

            }




		}
	}



	// EVENT DETAILS

	public function addToEventDetails($name = null, $value = null ){

		$this->eventDetails[$name] = $value;
		
	}

	public function removeFromEventDetails($name = null){

		unset($this->eventDetails[$name]);

	}

	public function getFromEventDetails($name = null){

		return $this->eventDetails[$name];

	}

	// TIMES

	public function addToTimes($name = null, $value = null ){

		$this->times[$name] = $value;
		
	}

	public function removeFromTimes($name = null){

		unset($this->times[$name]);

	}

	public function getFromTimes($name = null){

		return $this->times[$name];

	}

	// EVENT DATE DETAILS

	public function addToEventDateDetails($name = null, $value = null ){

		$this->eventDateDetails[$name] = $value;
		
	}

	public function removeFromEventDateDetails($name = null){

		unset($this->eventDateDetails[$name]);

	}

	public function getFromEventDateDetails($name = null){

		return $this->eventDateDetails[$name];

	}


	// PICTURES

	public function addToPictures($name = null, $value = null, $push = null ){
		if($push == null){
			$this->pictures[$name] = $value;
		}else{

			$this->pictures[$name][] = $value;
		}
	}

	public function removeFromPicutres($name = null){

		unset($this->pictures[$name]);

	}

	// DATES

	public function addToDates($name = null, $value = null, $push = null ){
		
		if($push == "push"){
			$this->dates[$name][] = $value;
		}else{

		$this->dates[$name] = $value;
		
		}
	}

	public function removeFromDates($name = null, $child1 = null, $child2 = null, $child3 = null, $child4 = null, $child5 = null){
		
		if($child1 == null){
			unset($this->dates[$name]);
		}elseif($child2 == null){
			unset($this->dates[$name][$child1]);
		}elseif($child3 == null){
			unset($this->dates[$name][$child1][$child2]);
		}elseif($child4 == null){
			unset($this->dates[$name][$child1][$child2][$child3]);
		}elseif($child5 == null){
			unset($this->dates[$name][$child1][$child2][$child3][$child4]);
		}else{
			unset($this->dates[$name][$child1][$child2][$child3][$child4][$child5]);
		}

	}

	public function getFromDates($name = null){

		return $this->dates[$name];

	}

	// TICKETS

	public function addToTickets($name = null, $value = null, $push = null){

		if($push != null){
			$this->tickets[$name][] = $value;
		}else{
			$this->tickets[$name] = $value;
		}
	}

	public function removeFromTickets($name = null, $child1 = null, $child2 = null, $child3 = null, $child4 = null, $child5 = null){

		if($child1 == null){
			unset($this->tickets[$name]);
		}elseif($child2 == null){
			unset($this->tickets[$name][$child1]);
		}elseif($child3 == null){
			unset($this->tickets[$name][$child1][$child2]);
		}elseif($child4 == null){
			unset($this->tickets[$name][$child1][$child2][$child3]);
		}elseif($child5 == null){
			unset($this->tickets[$name][$child1][$child2][$child3][$child4]);
		}else{
			unset($this->tickets[$name][$child1][$child2][$child3][$child4][$child5]);
		}

	}
	public function getFromTickets($name){
		return $this->tickets[$name];
	}

	// VENUE

	public function addToVenue($name = null, $value = null){

		$this->venue[$name] = $value;

	}

	// SETTING

	public function addToSettings($name = null, $value = null ){

		$this->settings[$name] = $value;
		
	}

	public function removeFromSettings($name = null){

		unset($this->settings[$name]);

	}

	





}
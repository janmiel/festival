<?php

class order{
	

	public $idorder = "";
	public $idevent_date = "";

	public $order_number = array();

	
	
	public $order_commission = 0;
	public $order_payment_fees = 0;
	public $order_total = 0;
	
	public $order_payment_status = "";

	public $buyer_first_name = "";
	public $buyer_last_name ="";
	public $buyer_email = "";

	public $buyerObject = array();



	public $paymentObject = array(
		"stripe" => array(
			"stripeToken" => "", 
			"stripeTokenType" => "",
			"stripeEmail" => "",
			"stripeChargeObject" => ""		
			)
		);

	public $ticketObject = array();

	public $eventDateObject = array();



	public function setOrderId(){

		$idorder = createId();
		$this->idorder = $idorder;

	}

	public function setEventDateId($value){
		$this->idevent_date = $value;
	}
	public function getOrderId(){
		return $this->idorder;
	}

	public function getEventDateId(){

		return $this->idevent_date;

	}
	public function setEventTicket($value){
		$this->event_ticket[] = $value;
	}

	public function getEventTicket(){
		return $this->event_ticket;
	}

	// public function setTicketsQuantity($value){
	// 	$this->tickets_quantity = $value;
	// }
	// public function getTicketsQuantity(){
	// 	return $this->tickets_quantity;
	// }

	// public function setTicketPrice($value){
	// 	$this->ticket_price = $value;
	// }
	// public function getTicketPrice(){
	// 	return $this->tickets_price;
	// }

	public function setOrderCommission($value){
		$this->order_commission = $value;
	}
	public function getOrderCommission(){
		return $this->order_commission;
	}
	
	public function setOrderTotal($value){
		$this->order_total = $value;
	}
	public function getOrderTotal(){
		return $this->order_total;
	}

	public function setPaymentStatus($value){
		$this->order_payment_status = $value;
	}
	public function getPaymentStatus(){
		return $this->order_payment_status;
	}

	public function setPaymentObject($paymentProvName, $name, $value){
		$this->paymentObject[$paymentProvName][$name] = $value;
	}
	public function getPaymentObject(){
		return $this->paymentObject;
	}

	public function setBuyerObject($value){
		$this->buyerObject = $value;
	}	

	public function getBuyerObject(){
		return $this->buyerObject;
	}
	
	public function setTicketObject($value){
		$this->ticketObject[] = $value;
	}	

	public function getTicketObject(){
		return $this->ticketObject;
	}

	public function setEventDateObject($value){
		$this->eventDateObject = $value;
	}	

	public function getEventDateObject(){
		return $this->eventDateObject;
	}

	public function setOrderNumber($value){
		$this->order_number = $value;
	}	

	public function getOrderNumber(){
		return $this->order_number;
	}
	
	public function calculateCommission(){

		$commission = 0; 

		foreach($this->ticketObject as $ticket){

			$commission += $ticket['details']['event_ticket_commission'] * $ticket['quantity'];

		}

		
		return $commission;
	}

	public function calculateTotal(){

		$commission = 0;
		$total = 0;

		$commission = $this->order_commission; 

		foreach($this->ticketObject as $ticket){

			$total += $ticket['total_ticket_type_price'];

		}


		return $total;
	}




}
<?php
/* this class is responsible for controlling page output */

class buildPage{
	

	public $PAGE = array();

	public $settings = array();


	public function __construct(){

		// load settings
		$sql = "select settings_name , settings_value from cms_settings";
		$result = fireSql($sql , "select", false);

		if($result != false){

			foreach($result as $res){
				$this->setSettings($res['settings_name'], $res['settings_value']);
			}
		}
	}


	public function renderPage($ifAdminTemplate = null, $adminTemplate = null ){

		if($pageParts = $this->getPageParts()){

			// load template
			if($ifAdminTemplate == "admin"){

				$templateHeader = "backend/theme/template/template_header.phtml";
				$templateFooter = "backend/theme/template/template_footer.phtml";
				$template = "backend/theme/template/" . $adminTemplate;
			
			}else{
				$theme = $this->settings['theme'];

				$templateHeader = "themes/" . $theme . "/template_header.phtml";
				$templateFooter = "themes/" . $theme . "/template_footer.phtml";
				
				$pageTemplate = $this->getSettings("template");
				$templateName = $this->getSettings("template_name");

				if($templateName == ""){
					$templateName = "template";
				}

				if($templateName == "template_error"){

					$template = "themes/" . $theme . "/template/" . $templateName . ".phtml"; 			
				}else{
					$template = "themes/" . $theme . "/template/" . $pageTemplate . "/" . $templateName . ".phtml"; 
				}

				

				
			}

			if(is_file($template)){

				//get template content
				$templateContent = file_get_contents($templateHeader);
				$templateContent .= file_get_contents($template);
				$templateContent .= file_get_contents($templateFooter);

				foreach($pageParts as $part){

					$partContent = implode("\n" ,$this->PAGE[$part]);
					$templateContent = str_replace("<<<{" . $part . "}>>>" , $partContent , $templateContent);

				}

			}

			echo  $templateContent;

		}
		else{

		}

	}

	
	public function setSettings($name, $value){
		$this->settings[$name] = $value;
	}
	public function getSettings($name){
		return $this->settings[$name];
	}


	public function addBlock($block){

		$this->PAGE[$block] = array();

	}

	public function addToBlock($block , $value = null){

		$this->PAGE[$block][] = $value;

	}



	public function getPageParts(){

		if(!empty($this->PAGE)){
			return array_keys($this->PAGE);
		}else{
			return false;
		}

	}




}
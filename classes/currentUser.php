<?php

class currentUser{

	public $first_name = "";
	public $last_name = "";
	public $organisation = "";
	public $iduser = "";
	public $status = "";
	public $user_access_tier = "";
	public $user_access_tier_number = 0;
	public $email = "";
	public $phone_number = "";
	public $country = "";
	public $postcode ="";

public function __construct($iduser){

	if($iduser != null){
	
		$this->iduser = $iduser;

		$user = getPromoterDetails($iduser);

		$this->first_name = $user['firstname'];
		$this->last_name = $user['surname'];
		$this->organisation  = $user['organisation'];
		$this->address1 = $user['address1'];
		$this->address2 = $user['address2'];
		$this->city = $user['town'];
		$this->county = $user['county'];
		//$this->country = $user['country'];
		//$this->postcode = $user['postcode'];
		$this->phone = $user['phone'];
		$this->email = $user['email'];
		$this->idstripe = $user['idstripe'];

		$this->user_access_tier = $user['user_access_tier'];

		if($this->user_access_tier == "admin"){
		    $this->user_access_tier_number = 8;
		}
		if($this->user_access_tier == "adminsuperuser"){
		    $this->user_access_tier_number = 9;
		}	
	}
}





}
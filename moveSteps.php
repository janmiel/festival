<?php

//modify charset
// ALTER TABLE talbename CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci

include("./system/dbc.php");
include("./system/functions.php");	


// create unique id's
if($_GET['step'] == 1){
	$sql = "SELECT idevent FROM events ";

	$response = fireSql($sql , 'select', false);

	foreach ($response as $row) {

		$uniqueid = createID();
		//$query2 = "UPDATE events SET idevent = '$uniqueid' WHERE idevent = $row[idevent]";
		fireSql($query2);

		echo ">";

	}
	echo "done";
}


// create event url  for old events
if($_GET['step'] == 2){
	$sql = "SELECT idevent, eventid, artistname FROM events";

	$result = fireSql($sql, "select", false);

	foreach($result as $row) {

		$eventUrl = replace_spaces(remove_comas(remove_quotas(trim($row['artistname']))), "_");
		$query2 = "UPDATE events SET event_url = '$eventUrl' WHERE idevent = '$row[idevent]'";
		fireSql($query2, "UPDATE", false);

	}
	echo "done";
}

// create page hash
if($_GET['step'] == 3){
	$sql = "SELECT idevent, eventid, event_url FROM events";

	$result = fireSql($sql, "select", false);


	foreach($result as $row) {


		$eventShortId = getEventShortId($row[eventid]);
		$eventHash = createPageHash($eventShortId, $row['event_url']);
		echo $query2 = "UPDATE events SET event_hash = '$eventHash' WHERE idevent = '$row[idevent]'";
		fireSql($query2, "UPDATE", false);

	}
}
// move events img to event_image
if($_GET['step'] == 4){
	$sql = "SELECT idevent, img, event_url FROM events";

	$result = fireSql($sql, "select", false);


	foreach($result as $row) {

		$urlArray = explode("/", $row['img']);
		$img = array_pop($urlArray);
		$id = createID();
		
		echo $query2 = "INSERT INTO event_image (idevent_image, idevent, event_image_name, event_image_filename, event_image_default) VALUES ('$id', '$row[idevent]', '$row[event_url]', '$img', '1') ";
		fireSql($query2, "insert", false);


		echo $img . " -> Added<br>";

	}
}
// create folders and copy images
if($_GET['step'] == 5){	
	$sql = "SELECT idevent, event_image_filename FROM event_image";

	$result = fireSql($sql, "select", false);


	foreach($result as $row) {

		//check if folder exists
		if(!is_dir("./images/events/" . $row['idevent'])){

			mkdir("./images/events/" . $row['idevent']);
			mkdir("./images/events/" . $row['idevent'] . "/thumb");
			echo "folder created<br>";
			copy("./images/events_ory/" . $row['event_image_filename'] , "./images/events/" . $row['idevent'] . "/" . $row['event_image_filename']);
			copy("./images/events_ory/thumbs/" . $row['event_image_filename'] , "./images/events/" . $row['idevent'] . "/thumb/" . $row['event_image_filename']);
			echo "file copied<br>";
		}
		

	}
}

// // move event->price to event_date and event_ticket
if($_GET['step'] == 6){
	echo $sql = "SELECT E.idevent, E.date, E.time , E.tickettype, E.price, E.qty, E.commission, E.category, E.active, E.venue FROM events E";

	$r = fireSql($sql, "select", false);


	foreach($r as $result) {


		$id = createID();
		
		echo $query2 = "INSERT INTO event_date (idevent_date, idevent, event_date_date, event_date_time, event_date_status, event_date_venue) VALUES ('$id', '$result[idevent]', '$result[date]', '$result[time]', '$result[active]', '$result[venue]') ";
		fireSql($query2, "insert", false);


		echo " -> ticket date created<br>";

		$id2 = createID();

		echo $query3 = "INSERT INTO event_ticket (idevent_ticket, idevent_date, event_ticket_name, event_ticket_price, event_ticket_quantity, event_ticket_commission)  VALUES ('$id2', '$id', '$result[tickettype]', '$result[price]', '$result[qty]', '$result[commission]') ";
		fireSql($query3, "insert", false);

		echo " -> ticket created<br>";
		
	}
}

// update event_date status 
if($_GET['step'] == 7){
	$sql = "SELECT idevent_date, event_date_status FROM event_date";

	$r = fireSql($sql, "select", false);


	foreach($r as $result) {


		if($result['event_date_status'] == "yes"){
			$status = "active";
		}elseif($result['event_date_status'] == "no"){
			$status = "notActive";
		}else{
			$status = $result['event_date_status'];
		}
		
		$query2 = "UPDATE event_date SET event_date_status = '$status' WHERE idevent_date = '$result[idevent_date]' ";
		fireSql($query2, "update", false);


		echo " -> updated<br>";


		
	}
}


// // move event venues to venues
if($_GET['step'] == 8){
	$sql = "SELECT idevent, promoter, venue FROM events where venue <> ''";

	$r = fireSql($sql, "select", false);


	foreach($r as $result) {

		$address = explode("," , $result['venue']);

		$venueName = $address[0];	
		$id = createID();

		$query1 = "SELECT idvenue FROM venues WHERE name = '$venueName' && idpromoter = '$result[promoter]'";
		$res = fireSql($query1, "select", false);

		if($res == false){

		ECHO $query2 = "INSERT INTO  venues (idvenue, idpromoter, name, address) VALUES ('$id', '$result[promoter]', '$venueName', '$result[venue]')";
		fireSql($query2, "insert", false);
		echo $venueName . " ->added<br>";

			// //updae event_date
			// $sql = "UPDATE event_date SET event_date_venue = '$id' WHERE idevent = '$result[idevent]'";
			// fireSql($sql, "upade");
			// echo ">>> event date updated<br>";
		}
		else {
			echo "skipped<br>";
		}

	}
}

// update event_date_venues


if($_GET['step'] == 9){
	$sql = "SELECT idvenue, address FROM venues";

	$r = fireSql($sql, "select", false);


	foreach($r as $result) {

		
		

			//updae event_date
			$sql = "UPDATE event_date SET event_date_venue = '$result[idvenue]' WHERE event_date_venue = '$result[address]'";
			fireSql($sql, "update");
			echo ">>> event date updated<br>";
		


	}
} 

// set all events as active (we do not copy evetns_temp)

if($_GET['step'] == 10){
	$sql = "UPDATE events SET active = 'active'";

	fireSql($sql, "select", false);

} 

if($_GET['step'] == 11){
	
	$sql = "SELECT ET.idevent_date , ET.idevent_ticket, ED.idevent FROM event_date ED join event_ticket ET ON ED.idevent_date = ET.idevent_date";

	$result = fireSql($sql, "select", false);

	foreach ($result as $r)
	{
	echo $sql2 = "UPDATE orders SET idevent_date = '" . $r['idevent_date'] . "', idevent_ticket = '" . $r['idevent_ticket'] . "' WHERE idevent = '" . $r['idevent'] . "'";
	fireSql($sql2, 'update', false);
	}
} 

// clean events_temp and prepare it to save temporary sessions

if($_GET['step'] == 12){

	$sql = "DROP TABLE  events_temp;";
	fireSql($sql, "delete", false);

}

// venue address, split address to save city separately

if($_GET['step'] == 13){

	$sql = "SELECT idvenue, address from venues";
	$result = fireSql($sql, 'select', false);
	

	foreach($result as $res){
		showVar($res);
		$town_array = explode(',' , $res['address']);
		$index = sizeof($town_array) - 2;
		$town = $town_array[$index];
		
		$sql = "UPDATE venues SET venue_city = '$town' where idvenue = '" . $res['idvenue'] . "'";
		fireSql($sql, 'update', false);
	}

}
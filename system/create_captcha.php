<?php
session_start() ;
// Create a 100*30 image
$im = imagecreate(100, 14);

// White background and blue text
$bg = imagecolorallocate($im, 255, 255, 255);
$textcolor = imagecolorallocate($im, 0, 0, 255);

$string = $_SESSION['captcha_code'];
// Write the string at the top left
imagestring($im, 5, 0, 0, $string, $textcolor);

// Output the image
header('Content-type: image/png');

imagepng($im);
imagedestroy($im);
?>
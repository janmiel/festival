<?php

$buildPage->setSettings("images_page", "./images/page/");
$buildPage->setSettings("site_folder", "/festival/");
$buildPage->setSettings("page_url", "http://" . $_SERVER['HTTP_HOST'] . $buildPage->settings['site_folder']);

if($buildPage->getSettings('theme') != ""){
		// theme is not set
		$theme = $buildPage->getSettings('theme');
		
		$buildPage->setSettings("theme" , $theme);
		$buildPage->setSettings("theme_path" , "themes/" . $theme);

		if(!isset($template_name)){
			$template_name = "template";
		}
}else{
		$theme = "defalut";
		$buildPage->setSettings("theme" , $theme);
		$buildPage->setSettings("theme_path" , "themes/" . $theme);
		
}

$dontSaveClass = false;

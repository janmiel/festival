<?php  

function buildResponse($data = "", $target = "", $script = "", $toggle = ""){


	
	ob_clean();
	print_r(json_encode(array($data, $target, $script, $toggle)));
	


}

function saveSession($sessionName, $objectName, $folderName = null, $fileName = null){

	$_SESSION[$sessionName] = serialize($objectName);

	if($fileName != null){
		$file_location = "./sessions/" . $folderName . "/" . $fileName . ".session";
		save_file($file_location, serialize($objectName));
	}

}

function deleteSession($sessionName, $objectName = null, $folderName = null, $fileName = null){

	

	if($fileName != null){
		$file_location = "./sessions/" . $folderName . "/" . $fileName . ".session";
		if(is_file($file_location)){
			delete_file($file_location);
		}
	}

}


function setPromoterAccess($userProfile){

	global $currentUser;

		if($userProfile != ""){
		$_SESSION['iduser'] = $userProfile['idpromoter'];
	    $_SESSION['logon'] = true;
	    $_SESSION['user_access_tier'] = $userProfile['user_access_tier'];

	    $currentUser->first_name = $userProfile['firstname'];
	    $currentUser->last_name = $userProfile['surname'];
	    $currentUser->status = $userProfile['status'];
	    $currentUser->organisation = $userProfile['organisation'];
	    $currentUser->iduser = $userProfile['idpromoter'];
	    $currentUser->user_access_tier = $userProfile['user_access_tier'];

	    if($currentUser->user_access_tier == "admin"){
	    	$currentUser->user_access_tier_number = 8;
	    }
	    if($currentUser->user_access_tier == "adminsuperuser"){
	    $currentUser->user_access_tier_number = 9;
	    }	


	    return true;
	}else{
		return false;
	}

}
function setUserAccess($userProfile, $user_access_tier){

	global $currentUser;

	$_SESSION['iduser'] = $userProfile['idpromoter'];
    $_SESSION['logon'] = true;
    $_SESSION['user_access_tier'] = $user_access_tier;

    $currentUser->first_name = $userProfile['users_first_name'];
    $currentUser->last_name = $userProfile['users_last_name'];
    
    $currentUser->iduser = $userProfile['idusers'];
    $currentUser->user_access_tier = $user_access_tier;
    
    if($currentUser->user_access_tier == "admin"){
    	$currentUser->user_access_tier_number = 8;
    }
    if($currentUser->user_access_tier == "adminsuperuser"){
    $currentUser->user_access_tier_number = 9;
    }	


    return true;

}

function encodePassword($password, $login){

	return md5($password . $login);

}


function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

function redirectTo($location, $callback = false){

	if($callback == false){	
		header("Location:" . $location);
	die();
	}
}

function validateDate($date = null)
{
	
    $date_timestamp = strtotime($date);
    $date_from_timestamp = date("Y-m-d", $date_timestamp);
    
    
   
    if($date == $date_from_timestamp){
    	//echo "date correct";
    	return true;
    }else{
    	//echo "date false";
    	return false;
    }
    
}

function changeDateFormat($date, $option){

	if($option == "dd.FF"){
		return date("d F", strtotime($date)); 
	}

}

function findNextDate($date = null, $jump = null, $day = null){

	$next_date = false;

	if($date != null){
		
		//create date of first of month (important for strtotime)
		$date_year = getDateForMe("year", null, $date);
		$date_month = getDateForMe("month", null,  $date);
		$previous_date = $date_year . "-" . $date_month . "-01";
		
		if($jump == "month" and $day != null){
			// find next month
			$next_date_firstofmonth = getDateForMe("getNext", " month ", $previous_date);
			
			$temp_year = getDateForMe("year", null, $next_date_firstofmonth);
			$temp_month = getDateForMe("month", null, $next_date_firstofmonth);

			$next_date = $temp_year . "-" . $temp_month . "-" . $day;
			
			if(validateDate($next_date)){
				return array($next_date, true);
			}else{
				return array($next_date_firstofmonth, false);
			}
			

		}
		
	}

	

}

function convertEventStatus($event_status){

		switch ($event_status) {
	 	case 'active':
	 		$event_status = "<span>Active<span>";
	 		break;
	 	
	 	case 'notActive':
	 		$event_status = "<span class='red-text darken-4'>Waiting for approval</span>";
	 		break;

	 	case 'pending':
	 		$event_status = "<span>Pending</span>";
	 		break;

	 	default:
	 		$event_status = "<span class='red-text darken-4'>Waiting for approval</span>";
	 		break;
	 }

	 return $event_status; 
}

function convertCurrency($currencyName = null){
	
		switch ($currencyName){

			case "euro":
				$currencyToDisplay = "&euro;";
				break;

			default:
				$currencyToDisplay = "&euro;";
				break;
		}

		return $currencyToDisplay;
	
}

function createPageHash($shortEventId, $eventUrl){

	$pageHash = md5($shortEventId . $eventUrl);

	return $pageHash;
}

function getEventShortId($eventid){
	
	$shortEventId = substr($eventid, -6);

	return $shortEventId;
}

function getRealEventUrl($shortEventId, $event_url){

	return "?event/" . $shortEventId . "/" . $event_url;
}


function fireSql($sql, $option = "select", $single = false){

	global $dbc;

	$data = array();
	$dbc = $dbc;
	$result = $dbc->query($sql);
	
	if($option == "select" and $result != false){
		
		$numberOfRows = $result->num_rows;

		if($numberOfRows == 0){
			return false;
		}
		elseif ($single == true){
			
			$data = $result->fetch_assoc();
			return $data;
		}
		elseif($single == false){
			
			while($res = $result->fetch_assoc()){
				$data[] = $res;
			}

			return $data;

		}
		else{
			return false;
		}
	}else{
		return $result;
	}
	
	$dbc->close;
}

function clearPost($text){
		
	return str_replace("'", "&#039;", strip_tags(trim($text)));
	
} 

function listFolders($directory){

	$folderList = array();

	if (is_dir($directory) and $handle = opendir($directory)) {

    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != ".." && is_dir($directory . $entry)) {
            $folderList[] = $entry;
        }
    }
    closedir($handle);

    return $folderList;
	}
	else{
		return false;
	}

}

function findFiles($folder, $ext = null){	
		
	$files = null;
	$file = null;
	$filesarray = null; 
	$i = 0;
	

	//get all  files with a extension.
	$files = glob($folder . "*.".$ext);
	 
	foreach($files as $file)
	{
	$filesarray[$i] = $file;
	$i++;
	}
	
	
	return $filesarray;	
}	

function replace_spaces($text , $separator){
	$text_no_spaces = preg_replace('/\s+/',$separator,$text);
	return $text_no_spaces;
		
		
}

function loadSettingsFromDatabase($buildPage){
	$sql = "select settings_name , settings_value from cms_settings";
	$result = fireSql($sql , "select", false);


	if($result == false){

	}else{
		foreach($result as $res){
			$buildPage->setSettings($res['settings_name'], $res['settings_value']);
		}
	}

}





  //  ID
function createID()
  {	
  	$znaki = array('a','s','h','rt','d','j','q','y','x','qe','y','x','q','yn','x','k','d','tx','g','a','w','q','c','b','ma','k','e','o','h','ys','f','gu');
	$uqid = uniqid();
  	$temp = time().'ts'.$uqid;
  return $temp;
  }

function createTicketNumber($idevent){

	$id = createID();
	$ticketNumber = "TS" . $id;
	$ticketNumberShort = "TS" . substr(time(), -6);
	

	while(checkIfEventTicketExist($ticketNumberShort, $idevent) != false){

		echo $ticketNumberShort = "TS" . rand(100000, substr($ticketNumberShort, 2));

	}

	$ticket = array(

			"ticketNumber" => $ticketNumber,
			"ticketNumberShort" => $ticketNumberShort

			);	
		

	return $ticket;

}
	
function getDateForMe($option = NULL, $time = NULL, $startDate = null){
	$month =  date("m");

	$day =  date("d");

	$year =  date("Y");

	$current_date = $current_date= $year."-".$month."-".$day;
	$current_time=date('H').":".date('i').":".date('s');

	if($startDate == null){
		$start_date= $year."-".$month."-".$day;
		$start_time=date('H').":".date('i').":".date('s');
	}else{
		$start_date = $startDate;
		
	}	

	if ($option == NULL){
		$current_timestamp = $current_date.' '.$current_time;
	return $current_timestamp;}
	
	if($option == 'subtractTime'){
		$new_time = date("Y-m-d H:i:s", strtotime($startDate - $time));
	return $new_time;
	}
	if($option == 'addTime'){
		$new_time = date("Y-m-d H:i:s", strtotime($startDate + $time));
	return $new_time;
	}
	if($option == 'addDays'){
		$new_time = date("Y-m-d", strtotime($startDate . $time));
	return $new_time;
	}
	if($option == 'getNext'){
		$startDate = ($startDate == null) ? $current_date : $startDate;
		$next_date = date("Y-m-d", strtotime($startDate . ' next ' . $time));
	return $next_date;
	}
	if($option == 'date'){
		$current_date= $year."-".$month."-".$day;
	return $current_date;
	}
	if($option == 'hour'){
		$current_time=date('H').":".date('i').":".date('s');
	return $current_time;
	}
	if($option == 'year'){
		$startDate = ($startDate == null) ? $current_date : $startDate;
		$date = date("Y", strtotime($startDate));
	return $date;
	}
	if($option == 'month'){

		$startDate = ($startDate == null) ? $current_date : $startDate;

		$date = date("m", strtotime($startDate));
	return $date;
	}

	if($option == 'getMonthName'){
		$startDate = ($startDate == null) ? $current_date : $startDate;

		$result = date("F", strtotime($startDate));
	return $result;

	}

	if($option == 'dayName'){
		$startDate = ($startDate == null) ? $current_date : $startDate;

		$day = date("l", strtotime($startDate));
	return $day;

	}

	if($option == 'getDay'){
		$startDate = ($startDate == null) ? $current_date : $startDate;

		$day = date("d", strtotime($startDate));
	return $day;
	}

	if($option == 'getNameShort'){
		$startDate = ($startDate == null) ? $current_date : $startDate;

		$day = date("D", strtotime($startDate));
	return $day;
	}
	
	

}
	

  
  
function setMyCookie($nazwaCookie, $wartoscCookie)
  {
  	setcookie($nazwaCookie,$wartoscCookie);
  }
  function delMyCookie($nazwaCookie)
  {
  	setcookie($nazwaCookie , "" ,time()-3600);
  }
  
     

 



	
function uploadImage($image, $imageFolder, $folderName, $newFileName, $wmax, $hmax ){
		
	if (is_dir($imageFolder."/".$folderName)==FALSE){mkdir($imageFolder."/".$folderName,0775, true); chmod($imageFolder."/".$folderName, 0755); }
	if (is_dir($imageFolder."/".$folderName."/thumb/")==FALSE){mkdir($imageFolder."/".$folderName."/thumb/",0775, true); chmod($imageFolder."/".$folderName."/thumb/", 0755); }
	//kopiowanie zdjecia	
		

									
		$fileName = $image["name"]; // The file name
		$fileTmpLoc = $image["tmp_name"]; // File in the PHP tmp folder
		$fileType = $image["type"]; // The type of file it is
		$fileSize = $image["size"]; // File size in bytes
		$fileErrorMsg = $image["error"]; // 0 for false... and 1 for true
		$kaboom = explode(".", $fileName); // Split file name into an array using the dot
		$fileExt = end($kaboom); // Now target the last array element to get the file extension
		// START PHP Image Upload Error Handling --------------------------------------------------
		if (!$fileTmpLoc) { // if file not chosen
		    return array(false, "ERROR: Please browse for a file before clicking the upload button.");
		    exit();
		} else if($fileSize > 5242880) { // if file size is larger than 5 Megabytes
		    return array(false, "ERROR: Your file was larger than 5 Megabytes in size.");
		    unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
		    exit();
		} else if (!preg_match("/.(gif|jpg|jpeg|png)$/i", $fileName) ) {
		     // This condition is only if you wish to allow uploading of specific file types    
		     return array(false, "ERROR: Your image was not .gif, .jpg, or .png.");
		     unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
		     exit();
		} else if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
		    return array(false, "ERROR: An error occured while processing the file. Try again.");
		    exit();
		}
		// END PHP Image Upload Error Handling ----------------------------------------------------
		// Place it into your "uploads" folder mow using the move_uploaded_file() function
		$moveResult = move_uploaded_file($fileTmpLoc, $imageFolder."/".$folderName."/".$newFileName.".".$fileExt);
		// Check to make sure the move result is true before continuing
		if ($moveResult != true) {
		    return array(false, "ERROR: File not uploaded. Try again.");
		    unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
		    exit();
		}
		//unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
	
		
		$target_file = $imageFolder."/".$folderName."/".$newFileName.".".$fileExt;
		$resized_file = $imageFolder."/".$folderName."/thumb/".$newFileName.".".$fileExt;
		$resized_medium_file = $imageFolder."/".$folderName."/thumb/medium-".$newFileName.".".$fileExt;
		
		$uploadResult = ak_img_resize($target_file, $resized_file, $wmax, $hmax, $fileExt);
		$uploadResult = ak_img_resize($target_file, $resized_medium_file, $wmax + 200, $hmax + 200, $fileExt);

		if($uploadResult == true){
			return array(true, $newFileName . "." . $fileExt);
		}
								
		
	}

function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
           $w = $h * $scale_ratio;
    } else {
           $h = $w / $scale_ratio;
    }
    $img = "";

    $tci = imagecreatetruecolor($w, $h);

    $ext = strtolower($ext);
    if ($ext == "gif"){

      	$img = imagecreatefromgif($target);
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);

    	//save file
    	imagegif($tci, $newcopy);

    	return true;

    } elseif($ext =="png"){ 

      	$img = imagecreatefrompng($target);
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);

    	//save file
    	imagepng($tci, $newcopy, 9);

    	return true;

    } else { 

      	$img = imagecreatefromjpeg($target);
      	imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);

    	//save file
    	imagejpeg($tci, $newcopy, 80);

    	return true;

    }

    imagedestroy($img);
    
    

    
}
	




function crop_image($imgPath, $cropArray){



	$fileInfo = pathinfo($imgPath);

	

	$ext = $fileInfo['extension'];
	$folder = $fileInfo['dirname'];
	$fileName = $fileInfo['basename'];
	

	$destinationFile = $folder . "/thumb/crop-" . $fileName;




	if(file_exists($imgPath)){



		//delete old file
		unlink(realpath($destinationFile));

		$ext = strtolower($ext);
	    if ($ext == "gif"){
	      $img = imagecreatefromgif($imgPath);
	      $imgCroped = imagecrop($img, $cropArray);
	      imagegif($imgCroped, $destinationFile);

	    } else if($ext =="png"){ 
	      $img = imagecreatefrompng($imgPath);
	      $imgCroped = imagecrop($img, $cropArray);
	      imagepng($imgCroped, $destinationFile);
	    } else { 
	      $img = imagecreatefromjpeg($imgPath);
	      $imgCroped = imagecrop($img, $cropArray);
	      imagejpeg($imgCroped, $destinationFile, 100);
	    }

		imagedestroy($img);
		imagedestroy($imgCroped);

	    
	}else{
		$_SESSION['error'] = "File does not exist";
	}

}
	
 function cut_string($str, $n = 0, $delim, $end = NULL, $stripTags = false) {
   $len = strlen($str);
   

   if($stripTags == true){

   		$str = strip_tags($str);

		if ($len > $n) {

			return strtok(wordwrap($str, $n, $delim), "\n");

		}
		else {
			return $str;
		}


   }

   if ($end == NULL){

       if ($len > $n) {

		return $str;

	    }
	    else {
			 
		return $str;
	    }
    }
   else{
    
    $out = stristr($str, $end, true);
    

    
    if (strlen($out)+ 30 > strlen($str)){
      return $out;
    }else{
    return $out.$delim;
    }
  }
  }
 

	function open_file($file_name){

		if(file_exists($file_name)){
			$text = file_get_contents($file_name);
			return $text;
		}
		else{return false;}
	}

	function save_file($file_location, $text){
		file_put_contents($file_location, $text);
	
	}

	function delete_file($file_location){

		$file = is_file($file_location);
	
		
		if($file){
			unlink($file_location);
		}

	
	}

	function clean_text($text){
		$old = array('ę', 'ó', 'ą', 'ś', 'ł', 'ż', 'ź', 'ć', 'ń', 'Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń');
		$new = array('e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n', 'e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n');
		$text_clean = str_replace($old, $new, $text);
		
		return $text_clean;
		
		
	}
	
	function remove_quotas($text){
		$old = array("'", '"');
		$new = array('', '');
		$text_clean = str_replace($old, $new, $text);
		
		return $text_clean;
	}
	
	function replace_quotas($text){
		$old = array("'", '"');
		$new = array('&#39;', '&#34;');
		$text_clean = str_replace($old, $new, $text);
		
		return $text_clean;
	}
	
	function remove_comas($text){
		$old = array(",");
		$new = array('');
		$text_clean = str_replace($old, $new, $text);
		
		return $text_clean;
	}
	
	function remove_spaces($text){
		$text_no_spaces = preg_replace('/\s+/','_',$text);
		
		return $text_no_spaces;
		
		
	}

	function replace_text($sign, $replace_with, $text){

		return str_replace($sign, $replace_with, $text);
	}

	function replace_texts($replace_array = array(), $text){

		foreach($replace_array as $replaceWhat => $replaceWith){

			$text = str_replace($replaceWhat, $replaceWith, $text);

		}

		return $text;
	}

	function remove_special_characters($string) {
	   
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	function check_variable($var, $type){
		$result = false;

		if($type = "int"){
			$tempVar = filter_var($var, FILTER_VALIDATE_INT);
			if($tempVar !== false){				
				$result = true;
			}
		}

		if($type = "float"){
			$tempVar = filter_var($var, FILTER_VALIDATE_FLOAT);
			if($tempVar !== false){
				$result = true;
				
			}
		}
		return $result;

	}

	
function getPage($pageName){
		
			
			if(is_file("./page/".$pageName.".php"))					
			{echo '<div class="'.$pageName.'">';include("./page/".$pageName.".php"); echo '</div>' ;}
			
	}
	


function getImageLocation($idimage = null, $folderName = null){
	if($idimage != null and $folderName != null){

		
		$sql = "SELECT event_image_filename, idevent FROM event_image WHERE idevent_image = '$idimage'";
		$result = fireSql($sql, "select", true);

		if($result != false){


		$imageLocation[0] = "./images/events/" . $folderName . "/" . $result['event_image_filename'];
		$imageLocation[1] = "./images/events/" . $folderName . "/thumb/" . $result['event_image_filename'];

		return $imageLocation;
		}else{
			return false;
		}
	}

	return false;
}

function checkIfImageExist($idimage = null){

	if($idimage != null){

		$sql = "SELECT idevent_image FROM event_image WHERE idevent_image = '$idimage'";
		$result = fireSql($sql, "select", true);

		if($result != false){
			return true;
		}else{
			return false;
		}

	}

}

function showVar($variable)
{
	if($_SESSION['user_access_tier'] == 'adminsuperuser'){
		print("<pre>");
		$result = htmlentities(print_r($variable, true));
		print_r($result);
		print("</pre>");
	}
}
	

function decodeObject($object){

	return unserialize(base64_decode($object));

}

function updateFacebook($real_event_url, $page_url, $respose = false){

	$fc_array = json_decode(open_file("./vendors/facebook/access_token.facebook"));

	$access_tokent_date = $fc_array->date;

	if(time() - $access_tokent_date > 4320000){
		$access_token = refreshFacebookAccessToken();
	}else{
		$access_token = $fc_array->access_token;
	}

	$url = "https://graph.facebook.com";
	$post = array(
			"id" => $page_url . '/' . $real_event_url,
			"scrape" => true,
			"access_token" => $access_token
			);

	$post = json_encode($post);


	
	$response = curlIt($url, $post, $no_response = true);
	$response = json_decode($response);


	

}

function refreshFacebookAccessToken(){

	$url = "https://graph.facebook.com/oauth/access_token";
	$post = '
	{
	"client_id":"1267136720078917",
	"client_secret":"346044c458628461ca8de2fa95d8647f",
	"grant_type":"client_credentials"
	}
	';

	$response = curlIt($url, $post, $no_response = false);
	$response = json_decode($response);
	$access_token = $response['access_token'];

	$data = '{
		"access_token": "' . $access_token . '",
		"date": "' . time() . '"
		}';
	save_file("./vendors/facebook/access_token.facebook", $data);

	return $access_token;
}


function curlIt($url, $post, $no_response = false){

	 // create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
        if($no_response == true){
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, false); 
        	curl_setopt($ch, CURLOPT_TIMEOUT_MS, 2000); 
        }else{
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $post);

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch);      
        
    return $output;
}

function logIt($text, $type = null){

	$date = getDateForMe();
	$text = json_encode($text);

	if($type == "email"){
		file_put_contents("./logs/log_email.txt", $date . " " . $text . "\n", FILE_APPEND);	
	}
	if($type == "payment" or $type == null){ 
		file_put_contents("./logs/log_payment.txt", $date . " " . $text . "\n", FILE_APPEND);
	}

}

function validateEmail($email = null){
	if($email != null){

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	  		return false;
		}else{
			return true;
		}
	}else{
		return false;
	}
}


function encodeTextForScanner($textForScanner){

	$string = "SADFSHWTOYHHDWRGOWHRDASERLYHST";

	$result = $string[rand(0, 29)] . $string[rand(0, 29)] . base64_encode($textForScanner) . $string[rand(0, 29)] . $string[rand(0, 29)];

	return $result;

}


function decodeTextForScanner($textFromScanner){

	$result = base64_decode(sutstr($textFromScanner, 2, -2));

	return $result;

}


function priceFormat($number, $intAfterComma = 2){

	$temp = number_format((float)$number, 2, '.', '');

return $temp;
}


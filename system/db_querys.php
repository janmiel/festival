<?php

// AGE INFO

function getAgeInfo($idevent_age_info = null){

	if($idevent_age_info != null){
		$where = " and AI.idevent_age_info = '$idevent_age_info'";
	}else{
		$where = "";
	}

	$sql = '
	SELECT AI.idevent_age_info, AI.name, AI.value 
	FROM event_age_info as AI 
	WHERE AI.idevent_age_info <> "" ' . $where 
	;

	$result = fireSql($sql, "select", $single = false);

	return $result;

}

//TICKETS

function checkIfEventTicketExist($ticketNumberShort, $idevent_date){
	$sql = "SELECT idticket FROM orders WHERE ticket_number_short = '$ticketNumberShort' and idevent_date = '$idevent_date'";
	$result = fireSql($sql, 'select', $single = true);

	return $result;
}


function addTicketToDate($idevent_date,  $event_ticket_name, $event_ticket_price, $event_ticket_quantity, $event_ticket_description, $event_ticket_commission, $event_ticket_settings){

	$idevent_ticket = createID();
	
	$sql = "INSERT INTO event_ticket (idevent_ticket, idevent_date, event_ticket_name, event_ticket_price, event_ticket_quantity, event_ticket_description, event_ticket_commission, event_ticket_active, event_ticket_settings_more_info, event_ticket_settings_delay, event_ticket_settings_delay_days, event_ticket_settings_delay_hours, event_ticket_settings_delay_timestamp)";
	$sql .= " VALUES('$idevent_ticket', '$idevent_date', '$event_ticket_name', '$event_ticket_price', '$event_ticket_quantity', '$event_ticket_description', '$event_ticket_commission', 1 , '" . $event_ticket_settings['more_info'] . "', '" . $event_ticket_settings['delay'] . "', '" . $event_ticket_settings['delay_days'] . "', '" . $event_ticket_settings['delay_hours'] . "', '" . $event_ticket_settings['delay_timestamp'] . "')";


	fireSql($sql, 'insert', false);

}


function getTicketDetails($idevent_ticket){

	$sql = "SELECT ET.idevent_ticket, ET.event_ticket_name, ET.event_ticket_price, ET.event_ticket_quantity, ET.event_ticket_commission FROM event_ticket AS ET WHERE ET.idevent_ticket ='$idevent_ticket'";

	$result = fireSql($sql, "select", $single = true);
	return $result;

}
//select event tickets
function getEventTickets($idEventDate, $ticketsActive = null, $ticketOrder = "ASC"){


	if($ticketsActive == true){
		$ticketsActive = " and ET.event_ticket_active = '1' ";
	}else{
		$ticketsActive = "";
	}

	$sql = "SELECT ET.idevent_ticket, ET.event_ticket_name, ET.event_ticket_description, ET.event_ticket_price, ET.event_ticket_currency, ET.event_ticket_quantity, ET.event_ticket_commission, ET.event_ticket_settings_more_info, ET.event_ticket_settings_delay, ET.event_ticket_settings_delay_days, ET.event_ticket_settings_delay_hours, ET.event_ticket_settings_delay_timestamp, ET.event_ticket_active FROM event_ticket AS ET WHERE ET.idevent_date ='$idEventDate' " . $ticketsActive . " ORDER BY ET.event_ticket_price " . $ticketOrder;
	$result = fireSql($sql, "select", $single = false);
	
	return $result;
}

function getDateTicketsDetails($idevent_date = null){

	if($idevent_date != null){
		$sql = "SELECT ET.idevent_ticket, ET.idevent_date, ET.event_ticket_name, ET.event_ticket_quantity, (SELECT sum(qty) FROM orders where idevent_date = '$idevent_date' and idevent_ticket = ET.idevent_ticket) as qty FROM event_ticket as ET ";
		$sql .= " WHERE ET.idevent_date = '$idevent_date'";

		return fireSql($sql, "select", $single = false);
	}

}

function getTicketDetailsForBuyer($ticket_number = null, $buyerEmail = null){

	if($ticket_number != null){

		$sql = "SELECT O.idorder, O.order_object, O.email FROM orders O WHERE ticket_number = '$ticket_number'";

		return fireSql($sql, "select", $single = true);
	}

}


function getNumberSoldTickets($event_date = null, $event_ticket = null)
{

	if($event_ticket != null){

		$event_ticket = " and O.idevent_ticket = '" . $event_ticket . "'";

	}

	if($event_date != null)
	{
		$sql = "SELECT SUM(O.qty) as qty from orders O WHERE O.idevent_date = '" . $event_date . "'" . $event_ticket;
		$result = fireSql($sql, "select", true);
		
		return $result['qty'] == "" ? 0 : $result['qty'];
	}
}

function getNumberAvailableTickets($event_date = "", $event_ticket = ""){

	if($event_date != "")
	{

		$sql = "SELECT SUM(ET.event_ticket_quantity) as event_ticket_quantity from event_ticket ET WHERE ET.idevent_date = '" . $event_date . "'";
		$result = fireSql($sql, "select", true);
		
		return $result['event_ticket_quantity'] == "" ? 0 : $result['event_ticket_quantity'];

	}
}


function modifyTicket($idticket = null, $option, $newValue = null){

	if($option == "quantity" and $idticket != null and $newValue != null){

		$sql = "UPDATE event_ticket
				SET  event_ticket_quantity = '" . $newValue. "' 
				WHERE idevent_ticket = '" . $idticket . "'
				";

		$result = fireSql($sql, "UPDATE", $single = false);

		return $result;

	}


	if($option == "settings" and $idticket != null and $newValue != null){

		$active = clearPost($newValue['active']);
		$more_info = clearPost($newValue['more_info']);
		$delay = clearPost($newValue['delay']); 
		$delay_days = clearPost($newValue['delay_days']);
		$delay_hours = clearPost($newValue['delay_hours']);
		$delay_timestamp = clearPost($newValue['delay_timestamp']);


		$sql = "UPDATE event_ticket
				SET  event_ticket_active = '" . $active. "',
				     event_ticket_settings_more_info = '" . $more_info. "',
					 event_ticket_settings_delay = '" . $delay . "',  
					 event_ticket_settings_delay_days = '" . $delay_days . "',  
					 event_ticket_settings_delay_hours = '" . $delay_hours . "',  
					 event_ticket_settings_delay_timestamp = '" . $delay_timestamp . "'
				WHERE idevent_ticket = '" . $idticket . "'
				";
		
		$result = fireSql($sql, "UPDATE", $single = false);

		return $result;

	}




}






// DATES

function addDateToEvent($idevent_date, $idevent_date_for_time, $idevent, $event_date_date, $event_date_time, $event_date_timestamp, $idvenue, $event_date_status){

	

	$sql = "INSERT INTO event_date (idevent_date, idevent_date_for_time, idevent, event_date_date, event_date_time, event_date_timestamp, idvenue, event_date_status) ";
	$sql .= "VALUES ('$idevent_date', '$idevent_date_for_time', '$idevent', '$event_date_date' , '$event_date_time', '$event_date_timestamp', '$idvenue', '$event_date_status')";

	fireSql($sql, "insert", false);
}

function findTemplateForPage($pageName){
	$pageName = clearPost($pageName);
	$sql = "SELECT page_template from cms_page where page_url = '$pageName'";

	$result = fireSql($sql, "select", true);;

	if($result != false){
		return $result;		
	}else{
		return false;
	}

}


// USERS

function createPromoterAccount($uid, $uemail, $firstname, $surname, $uProviderName, $user_access_tier, $organisation = "", $password = "", $phone_number = ""){

	global $currentUser;

	if(!empty($currentUser->iduser)){
		$idpromoter = $currentUser->iduser;
	}else{	
		$idpromoter = createId();
	}
    
    $sql = "INSERT INTO promoters (idpromoter, firstname, surname, email, provider, uid, status, user_access_tier, password, organisation, phone ) VALUES ('$idpromoter', '$firstname', '$surname', '$uemail', '$uProviderName', '$uid', 'active', '$user_access_tier', '$password', '$organisation', '$phone_number')";

    $result = fireSql($sql, 'insert', true);

    return $result;

}

function updatePromoter($promoter){



	global $currentUser;

	$sql = "UPDATE promoters SET 
			firstname = '" . $promoter['first_name'] . "',
			surname = '" . $promoter['last_name'] . "',
			organisation = '" . $promoter['organisation'] . "',
			address1 = '" . $promoter['address1'] . "',
			address2 = '" . $promoter['address2'] . "',
			town = '" . $promoter['town'] . "',
			county = '" . $promoter['county'] . "',
			phone = '" . $promoter['phone'] . "',
			email = '" . $promoter['email'] . "'

			WHERE idpromoter = '" . $currentUser->iduser . "'

	";



	fireSql($sql, "update", $single = false);


}

function getPromoterDetails($idpromoter = null){
	if($idpromoter != null){

		$sql = "SELECT idpromoter, firstname, surname, address1, address2, town, county, organisation, phone, email, status, user_access_tier, idstripe FROM promoters WHERE idpromoter = '$idpromoter'";
		$result = fireSql($sql, "select", $single = true);

		return $result;

	}
}

function getPromoters($promoterId = null){

	if($promoterId == null){
		$where = "";
		$single = false;
	}else{
		$where = " where idpromoter = '$promoterId'";
		$single = true;
	}
	
	$sql = "SELECT idpromoter, organisation from promoters" . $where . " ORDER BY organisation";
	$result = fireSql($sql, "select", $single);
	
	return $result;

}

//find user by provider id

function findUserByProviderId($userId = null, $userEmail = null, $provider = null){

	if($userId != null){

		$sql = "SELECT idpromoter, email, provider, organisation, firstname, surname, status, user_access_tier FROM promoters WHERE uid = '$userId'";
		$userTemp = fireSql($sql, "select", true);

		if($userTemp == false){
			return false;
		}else{
			return $userTemp;
		}

	}

}

// get promoter id having event_date id

function getPromoterIdFromEventDate($idevent_date = null){
	if($idevent_date != null){

		$sql = "SELECT E.promoter FROM event_date ED JOIN events E WHERE E.idevent = ED.idevent and ED.idevent_date = '$idevent_date'";

		$result = fireSql($sql, "select", $single = false);

		return $result;

	}else{return false;}
}

//find user by  id

function findUserByUserId($userId = null, $userEmail = null){

	if($userId != null){

		$sql = "SELECT idpromoter, email, provider, organisation, firstname, surname, status, user_access_tier FROM promoters WHERE idpromoter = '$userId'";
		$userTemp = fireSql($sql, "select", true);

		if($userTemp == false){
			return false;
		}else{
			return $userTemp;
		}

	}

}

function getBuyerDetails($idbuyer = null){

	if($idbuyer != null){

		$sql = "SELECT idbuyer, buyer_firs_name, buyer_surname, buyer_email FROM buyer WHERE idbuyer = '$idbuyer'";
		$result = fireSql($sql, "select", $single = true);

		return $result;
		
	}


}

function saveBuyer($buyer = null){

	if($buyer != null){

		$idbuyer = createID();

		$sql = "INSERT INTO buyer (idbuyer, buyer_firs_name, buyer_last_name, buyer_email, buyer_temporary) VALUES
		('$idbuyer', 
		'" . $buyer['buyer_first_name'] . "',
		'" . $buyer['buyer_last_name'] . "',
		'" . $buyer['buyer_email'] . "',
		'true'
		)";

	}
}

function getCmsUsers(){

	$sql = "select idusers from cms_users";

	$result = fireSql($sql, "select", $single = false);

}


// STRIPE

function updateStripeId($promoter = null, $idstripe = null){

	if($promoter != null and $idstripe != null ){
		$sql = "UPDATE promoters SET idstripe = '$idstripe' WHERE idpromoter = '$promoter'";
		$result = fireSql($sql, "update", $single = false);

		return $result;
	}else{
		return false;
	}

}


// CATEGORIES
function getCategries(){
	
	$sql = "SELECT * FROM categories";
	$result = fireSql($sql);
	
	return $result;
}

function getCategory($idCategory = null){
	if($idCategory != null){
		$sql = "SELECT id, name from categories where id = '$idCategory'";
		$result = fireSql($sql, "select", $single = true);

		return $result;
	}
}

// VENUES
function getVenues($idpromoter = null){

	if ($idpromoter != null){
		$where = " WHERE idpromoter='$idpromoter'";
	}else{
		$where = "";
	}
	$sql = "SELECT idvenue, name, address FROM venues " . $where . 'ORDER BY name';
	$result = fireSql($sql, "select", false); 
	
	return $result;
}

function getVenue($idvenue){

	$where = " WHERE idvenue='$idvenue'";

	$sql = "SELECT idvenue, name, address, venue_city, venue_county FROM venues " . $where;
	$result = fireSql($sql, "select", true);
	
	return $result;
}



// EVENTS


function findEventByHash($eventHash){
	$sql = "SELECT idevent from events where event_hash = '$eventHash'";
	$result = fireSql($sql, "select", $single = true);

	return $result;
}


function findEventDates($idevent, $option = null, $groupBy = null, $orderBy = null){

	if($option == "upcoming"){

		$event_date_timestamp = time();

		$date = "and (event_date_timestamp >= '$event_date_timestamp' and event_date_date <> '0000-00-00')";
	}elseif($option == "archive"){

		$date = "and (event_date_date = '0000-00-00' or event_date_date < CURDATE())";
	}else{ 
		$date = "";
	}

	if($groupBy != null){
		$groupBy = " GROUP BY " . $groupBy ;
		//$group_concat = " GROUP_CONCAT(event_date_time ORDER BY event_date_time) as";
	}else{
		$groupBy = "";
		$group_concat = "";
	}

	if($orderBy != null){
		$orderBy = " ORDER BY " . $orderBy;
	}
	$sql = "SELECT idevent, idevent_date, event_date_date, idvenue, event_date_time FROM event_date where idevent = '$idevent' " . $date . " " . $groupBy . $orderBy;
	$result = fireSql($sql, 'select', false);

	return $result;
}

function getEventDate($ideventDate = null, $status = null, $limit = null, $orderBy = null, $date = null, $groupBy = null, $privacy = null, $event_active = null, $promoter = null){

	global $currentUser;

	$where = "";
	$single = false;

	if($ideventDate != null){

		$where .= " and ED.idevent_date = '" . $ideventDate . "'";
		$single = true;

	}
	if($status != null){

		$where .= " and ED.event_date_status = '" . $status . "'";

	}

	if($event_active != null){

		$where .= " and E.active = '" . $event_active . "'";

	}

	if($limit != null){

		$limit = " LIMIT " . $limit;

	}
	if($orderBy != null){

		$orderBy = " ORDER BY " . $orderBy;

	}

	if($date != null){

		$where .= " and ED.event_date_timestamp " . $date;

	}
	if($groupBy != null){

		$groupBy = " GROUP BY " . $groupBy ;

	}
	if($privacy != null){

		$where .= " and E.privacy = '" . $privacy . "'" ;

	}

	if($promoter != null){

		$where .= " and E.promoter = '" . $promoter . "'";

	}

	$sql = "SELECT ED.idevent_date, ED.idevent_date_for_time, ED.idevent, ED.event_date_date, ED.event_date_time, ED.event_date_status, ED.idvenue, ED.event_date_img, ED.event_date_desc 
			FROM event_date AS ED JOIN events AS E ON ED.idevent = E.idevent where ED.idevent_date <> '' " . $where . $groupBy . $orderBy .  $limit;

	$result = fireSql($sql, 'select', $single);

	return $result;
}

function getEvents($active = null, $limit = null, $orderBy = null, $date = null, $groupBy = null, $mainEventOnly = false){

	global $currentUser;

	$where = "";

	if($limit != null){
		$limit = "LIMIT " . $limit;
	}
	if($orderBy != null){
		$orderBy = "ORDER BY " . $orderBy;
	}
		if($groupBy != null){
		$groupBy = "GROUP BY " . $groupBy;
	}

	if($date != null){
		$where .= " and ED.event_date_date " . $date;
	}

	if($active != null){
		$where .= " and E.active = '" . $active . "'";
	}
	
	if($_SESSION['user_access_tier'] == "promoter"){

		$where .= " and E.promoter = '" . $currentUser->iduser . "'";

	}

	if ($mainEventOnly == false){

		$sql = "SELECT E.idevent, E.artistname, E.event_url, EI.event_image_filename, ED.idevent_date, ED.event_date_date, ED.event_date_time, ED.event_date_status, ED.idvenue, SUM(ET.event_ticket_quantity) AS event_tickets_all FROM events E JOIN event_image EI ON E.idevent = EI.idevent JOIN event_date ED ON EI.idevent = ED.idevent JOIN event_ticket ET ON ED.idevent_date = ET.idevent_date  WHERE EI.event_image_default = '1' " .  $where . " " . $groupBy . " ". $orderBy . " " . $limit;
		
	}
	
	if($mainEventOnly == true){

		$sql = "SELECT E.idevent, E.artistname, E.active from events E WHERE E.idevent <> '' $where $orderBy $limit";

	}


	
	$result = fireSql($sql , "select", $single = false);
	
	return $result;
}

function getEventDetails($idEvent = null, $promoter = null){

	if($idEvent != null){
		 $sql = "
			SELECT
			E.idevent,
			E.artistname,
			E.artistdesc, 
			E.active,
			E.privacy,
			E.category,
			E.event_url,
			P.organisation,
			P.idpromoter,
			E.event_use_editor,
			E.idevent_age_info,
			E.event_age_info_other_value
			FROM 
			events as E JOIN promoters as P ON E.promoter = P.idpromoter
			where E.idevent = '$idEvent' ";

			$resultEvent = fireSql($sql , "select" , true);

		return $resultEvent;
	}else{
		return false;
	}

}

function getEventDatesByCategory($idCategory = null, $option = null, $event_date_timestamp = null){

	if($idCategory != null){

			if($option == "upcomming"){

				$start_date_time = " and ED.event_date_timestamp >= $event_date_timestamp ";
			}

			$sql = "
			SELECT ED.idevent_date
			FROM event_date ED join events E ON ED.idevent = E.idevent 
			WHERE E.category = '$idCategory'" . $start_date_time . "
			ORDER BY ED.event_date_date, ED.event_date_time
			";

			$result = fireSql($sql , "select" , $single = false);

		return $result;
	}else{
		return false;
	}
	

}

function findCategoryDates($idevents = null, $category = null){


	if($category != null and $idevents != null){

		$i=0;
		$eventIDS = "(";
		foreach($idevents as $idevent){

			if($i > 0){
				$eventIDS .= " or ";
			}
			$eventIDS .= "idevent = 'idevent'";

		$i++;
		}
		


		echo $sql = "SELECT idevent_date
				FROM event_date 
				WHERE " . $eventIDS . " and (event_date_date >= CURDATE() and event_date_date <> '0000-00-00') 
				ORDER BY event_date_date
				";

		$result = fireSql($sql, 'select', false);

		return $result;

	}else{
		return false;
	}
}

///////////////////////////

function getEventImages($idevent = null, $ideventDate = null, $default = false){
	
	if($idevent != null){
		$sql = "SELECT idevent_image, idevent_date, event_image_name, event_image_url, event_image_filename, event_image_default FROM event_image WHERE idevent = '" . $idevent . "'";

		if($ideventDate != null){
			$sql .= " and idevent_date = '" . $ideventDate . "'";
		}else
		{
			$sql .= " and idevent_date = ''";
		}
		if($default == true){
			$sql .= " and event_image_default = '1'";
		}

	
	$imagesArray = fireSql($sql, "select", false);
	
	return $imagesArray;
	
	}else{

		return false;
	}


}



function getEventReport($ideventDate){

	$sql = "SELECT O.ticket_number, O.ticket_number_short, O.item_price, O.qty, O.total, O.commission, O.paymentstatus, O.name, O.email, E.artistname, ET.event_ticket_name,  O.date, ED.event_date_date, ED.event_date_time FROM orders O join event_date ED ON O.idevent_date = ED.idevent_date join event_ticket ET ON O.idevent_ticket = ET.idevent_ticket join events E ON O.idevent = E.idevent WHERE ET.idevent_date = '$ideventDate'";
	$result = fireSql($sql, 'select', false);

	return $result;
}










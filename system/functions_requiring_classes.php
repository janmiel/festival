<?php  

function calculateCommission($ticketPrice){

	global $buildPage;

	if($ticketPrice > 0.01){

		$commission_percent = $buildPage->getSettings('commission_percent');
		$commission_fixed_value = $buildPage->getSettings('commission');
		

		return number_format(round( ($ticketPrice * $commission_percent / 100 ) + $commission_fixed_value, 2 ,PHP_ROUND_HALF_UP),2);
	}else{
		return "0.00";
	}

} 

function calculateFinalCommission(){

	global $order;

	$realStripeFees = calculateStripeFees($order->order_total + $order->order_payment_fees);
	$promoterFee = $order->order_total - $order->order_commission;

	$realOrderTotal = $order->order_total + $order->order_payment_fees;
	
	$commission = $realOrderTotal - $promoterFee - $realStripeFees;
		

	return $commission;

} 



function calculateStripeFees($total, $stripeFeeFixed = 0, $stripeFeePercentage = 0){

	global $buildPage;

	if($total >= 0.01){

		$stripeFeeFixed = $buildPage->settings['stripe_fee_fixed'];
		$stripeFeePercentage = $buildPage->settings['stripe_fee_percentage'];

		$stripe_fees = ($total * $stripeFeePercentage / 100) + $stripeFeeFixed;

		
		$stripe_tax = $stripe_fees * 23 / 100;

			$stripe_fees = 
				number_format(
					round( 

					$stripe_fees + $stripe_tax ,

					3, PHP_ROUND_HALF_UP),
				 2) ;
	}else{
		$stripe_fees = 0;
	}

	return $stripe_fees;
}


// send mail 
function sendMail($fromEmail, $fromName, $subject, $body, $toEmail, $toName){


	require './mailSystem/PHPMailerAutoload.php';

	$mail = new PHPMailer;

	$mail->SMTPDebug = 0;                               // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $buildPage->settings['email_host'];     //'roman.hostinghouse.pl';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = $buildPage->settings['email_email'];//'info.ts@jmiel.vxm.pl';                 // SMTP username
	$mail->Password = $buildPage->settings['email_password']; //'jantan';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	
	// $mail->SMTPOptions = array(
 //    'ssl' => array(
 //        'verify_peer' => false,
 //        'verify_peer_name' => false,
 //        'allow_self_signed' => true
 //    )
	// );

	$mail->XMailer = ' ';

	$mail->Port = 587;                                    // TCP port to connect to


	$mail->setFrom($fromEmail, $fromName);
	$mail->addAddress($toEmail, $toName);     // Add a recipient
	//$mail->addAddress('ellen@example.com');               // Name is optional
	$mail->addReplyTo($fromEmail, $fromName);
	//$mail->addCC('cc@example.com');
	//$mail->addBCC('bcc@example.com');

	//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $subject;
	$mail->Body    = $body;
	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	if(!$mail->send()) {
	    
	    //echo 'Mailer Error: ' . $mail->ErrorInfo;
	    return false;
	} else {
	    return true;
	}
   

}

	
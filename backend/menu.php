<?php
$menuHeader ='
  <nav class="black">
    <div class="nav-wrapper">';

$menuLogo ='<a href="./promoter.html?build=dashboard" class="brand-logo"><div class="logo-wrapper valign-wrapper"><div class="valign"><img class="responsive-img logo" src="' . $buildPage->settings['logo'] . '"/></div></div></a>';




$menu_my_account = '
        <li class="divider"></li>
        <li><a href="promoter.html?build=promoter">My Profile</a></li>
        <li><a href="promoter.html?build=promoter#stripe">Stripe</a></li>
        <li class="divider"></li>       
        <li><a href="promoter.html?logout">Logout</a></li>

        ';

$menu_cms = '
        <li><a href="backend.html?build=themes&dashboard">Themes</a></li>
        <li class="divider"></li>
        <li><a href="backend.html?build=page&dashboard">Page</a></li>
        <li class="divider"></li>       
        <li><a href="backend.html?build=gallery&dashboard">Gallery</a></li> 
        <li class="divider"></li>     
        <li><a href="backend.html?build=settings&dashboard">Settings</a></li>
        <li class="divider"></li>
        <li><a href="backend.html?site=help" name="page_specyfic">Help</a></li>
        ';

$menu_configuration = '
        <li><a href="backend.html?build=age_info&dashboard">Age Info</a></li>
       
        ';







$menu ='
      <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>

      <ul class="nav-mobile right hide-on-med-and-down">';
     

      if($currentUser->user_access_tier == "adminsuperuser"){
        $menu .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown-configuration">Configuration<i class="material-icons right">arrow_drop_down</i></a></li>';
        $menu .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown-cms">CMS<i class="material-icons right">arrow_drop_down</i></a></li>';
      }

      $menu .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown-myaccount">MY ACCOUNT<i class="material-icons right">arrow_drop_down</i></a></li>';
      $menu .= '
      </ul>
';



// submenu desktop
$menu .='

      <ul id="dropdown-myaccount" class="dropdown-content">';
      $menu .= $menu_my_account;
      $menu .= '        
      </ul>

      <ul id="dropdown-cms" class="dropdown-content">';
      $menu .= $menu_cms;
      $menu .= '
      </ul>
      <ul id="dropdown-configuration" class="dropdown-content">';
      $menu .= $menu_configuration;
      $menu .= '
      </ul>
';
// mobile
$menu .='

      <ul class="side-nav" id="mobile-menu">';
      //$menu .= $menu_single;

      if($currentUser->user_access_tier_number >= 8){
        $menu .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown-cms-mobile">CMS<i class="material-icons right">arrow_drop_down</i></a></li>';
      }
      
      $menu .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown-my-account">MY ACCOUNT<i class="material-icons right">arrow_drop_down</i></a></li>';
      $menu .= '
      </ul>
';
// submenu mobile
$menu.='

      <ul id="dropdown-my-account" class="dropdown-content">';

      $menu .= $menu_my_account;
      $menu .= '
      </ul>';

      if($currentUser->user_access_tier == "admin" or $currentUser->user_access_tier == "adminsuperuser"){
        $menu .= '<ul id="dropdown-cms-mobile" class="dropdown-content">';
        $menu .= $menu_cms;
        $menu .= '</ul>';
      }

$menuFooter = '      

    </div>
  </nav>
';

if(isset($_SESSION['logon']) and ($_SESSION['user_access_tier'] == "adminsuperuser" or $_SESSION['user_access_tier'] == "admin" or $_SESSION['user_access_tier'] == "promoter") ){

$buildPage->addToBlock("menu", $menuHeader . $menuLogo . $menu . $menuFooter);  

}





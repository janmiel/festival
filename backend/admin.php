<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
session_start();
ob_start();

include('system/dbc.php');

include('./backend/system/config.php');
//logout
include("./backend/system/logout.php");
include('./system/functions.php');

include('./system/db_querys.php');

//autoload all classes
require_once("./classes/autoload.php");

include('./system/db_querys_requiring_classes.php');

include('./system/functions_requiring_classes.php');

include('./system/settings.php');

// login
include("./backend/system/login.php");



include('./system/url_translator.php');



$dontSaveClass = false;
$doNotRedirect = false;


$buildPage->addToBlock("debug", "");


// load settings
loadSettingsFromDatabase($buildPage);



//check if requesting callback only

if(isset($_GET['callback']) or isset($_POST['callback'])){
	$callback = true;
}else{
	$callback = false;
}



//build page
if (isset($_SESSION['logon']) and $currentUser->user_access_tier != ""){
		// PAGE VIEW
        if(isset($_GET['build']))
		{
		    $build = $_GET['build']; 
			$path_webpage_build = './backend/theme/template/' . $build;
			$file = $path_webpage_build.'/main.php';
			if(is_file($file)){
				require_once($file);		
			}else{
				$redirectUrl = "backend.html?build=dashboard";
			}
		}else{
				$redirectUrl = "backend.html?build=dashboard";

		}		
}


//self select template for home page
	
	if(!isset($admin_template_folder)){
		
		$admin_template_folder = "/default/";
		$admin_template_file = "template.phtml";
	}	
	// 1. add default blocks content to template

	include("./backend/theme/template/addContentToAllTemplates.php");

	// 2. add page_template specific blocks content to template
	$local_template = "./backend/theme/template" . $admin_template_folder . "addContentToTemplate.php";
	include($local_template);


// include callback only if required
if($callback == true){
	include('./backend/function/callback.php');
}


$saveClasses = true;
include('./classes/autoload.php');

// redirect if redirecturl set and callback false

if(!isset($doNotRedirect)){$doNotRedirect == false;}

if(isset($redirectUrl) and $redirectUrl != "" and $doNotRedirect != true){
	redirectTo($redirectUrl, $callback);
}

//debug
if(isset($_SESSION['user_access_tier']) and ($_SESSION['user_access_tier'] == 'admin' or $_SESSION['user_access_tier'] == 'adminsuperuser')){
	$buildPage->addToBlock("debug", print_r($currentUser, true));
}
//$buildPage->addToBlock("debug", print_r($eventBuilder, true));

// show errors
if(isset($_SESSION['error']['string']) and $_SESSION['error']['string'] != ""){

	
	$error = $_SESSION['error']['string'];



	$buildPage->addToBlock("template_footer_after", "<script>Materialize.toast('" . $error . "', 10000, 'red-background')</script>");

	unset($_SESSION['error']);

}


// render page only if not ajax 
if($callback == false){

	$buildPage->renderPage("admin", $admin_template_folder . $admin_template_file);
}else{
	exit();
}














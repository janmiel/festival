<?php

$admin_template_folder = "/age_info/";
$admin_template_file = "age_info_edit.phtml";

if (isset($_GET['edit'])){

	$id = $_GET['edit'];

	
	$result = getAgeInfo($id);
	
	if($result != false){
		// form
		$name = $result[0]['name'];

		$settings_value = '
			<textarea id="ckeditor" type="text" name="value">' . $result[0]['value'] . '</textarea>
			
			';

		$settings_save = '
			<input class="btn submit" type="submit" name="save" value="Save" />
			';
	}
}

$buildPage->addToBlock("form1_name", "Edit: " . $name);

$buildPage->addToBlock("value", $settings_value);
$buildPage->addToBlock("save", $settings_save);

<?php

// PROFILE

   if(isset($_POST['promoter_save'])){

      $first_name = clearPost($_POST['promoter_first_name']);
      $last_name = clearPost($_POST['promoter_last_name']);
      $email = clearPost($_POST['promoter_email']);
      $phone = clearPost($_POST['promoter_phone']);
      $organisation = clearPost($_POST['promoter_organisation']);
      $address1 = clearPost($_POST['promoter_address1']);
      $address2 = clearPost($_POST['promoter_address2']);
      $town = clearPost($_POST['promoter_city']);
      $county = clearPost($_POST['promoter_county']);

      $promoter_array = array(

         "first_name"   => $first_name,
         "last_name"    => $last_name,
         "email"        => $email,
         "phone"        => $phone,
         "organisation" => $organisation,
         "address1"     => $address1,
         "address2"     => $address2,
         "town"         => $town,
         "county"       => $county

         );

      updatePromoter($promoter_array);

      $currentUser = new currentUser($currentUser->iduser);
      
   }


// STRIPE

   if(isset($_GET['scope']) and isset($_GET['code']) and $_GET['code'] != ""){

      $url = "https://connect.stripe.com/oauth/token";
      $code = clearPost($_GET['code']);

      $token_request_body = array(
         'grant_type' => 'authorization_code',
         'code' => $code,
         'client_secret' => $buildPage->settings['stripe_sk']
      );

      
      $response =curlIt($url, http_build_query($token_request_body), $no_reponse = false);




      if(isset($response->error) or $response == false or empty($response)){
         $buildPage->addToBlock("info", "Stripe account not connected. Please try again.");

      }else{


         $result = updateStripeId($promoter = $currentUser->iduser, $idstripe = $response->stripe_user_id);

         if($result != false){

            $redirectUrl = "./promoter.html?build=promoter";


         }
         
         
      }
      


   }



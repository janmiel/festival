<?php

$admin_template_folder = "/promoter/";
$admin_template_file = "promoter.phtml";


//include functions
include('./backend/function/function.php');


   

    require_once("./backend/submenu.php");
    
    $buildPage->addToBlock("welcome_text_left", "Promoter Profile");
    $buildPage->addToBlock("welcome_text_right", "");
    $buildPage->addToBlock("welcome_bottom", "");


////
 // ACCOUNTS
if($_SESSION['logon'] == true and ($_SESSION['iduser'] == $currentUser->iduser or $_SESSION['users_access_tier'] >= '8')){

}else{
   $redirectUrl = "?index.html";
   redirectTo($redirectUrl);
}

//ADDRESS

   $promoter_first_name = '
      <input type="text" name="promoter_first_name" value="' . $currentUser->first_name . '">

   ';
   $buildPage->addToBlock("promoter_first_name", $promoter_first_name);

   $promoter_last_name = '
      <input type="text" name="promoter_last_name" value="' . $currentUser->last_name . '">

   ';
   $buildPage->addToBlock("promoter_last_name", $promoter_last_name);

   $promoter_organisation = '
      <input type="text" name="promoter_organisation" value="' . $currentUser->organisation . '">

   ';
   $buildPage->addToBlock("promoter_organisation", $promoter_organisation);


   $promoter_email = '
      <input type="text" name="promoter_email" value="' . $currentUser->email . '">

   ';
   $buildPage->addToBlock("promoter_email", $promoter_email);

   $promoter_phone = '
      <input type="text" name="promoter_phone" value="' . $currentUser->phone . '">

   ';
   $buildPage->addToBlock("promoter_phone", $promoter_phone);

   $promoter_address1 = '
      <input type="text" name="promoter_address1" value="' . $currentUser->address1 . '">

   ';
   $buildPage->addToBlock("promoter_address1", $promoter_address1);

   $promoter_address2 = '
      <input type="text" name="promoter_address2" value="' . $currentUser->address2 . '">

   ';
   $buildPage->addToBlock("promoter_address2", $promoter_address2);

   $promoter_city = '
      <input type="text" name="promoter_city" value="' . $currentUser->city . '">

   ';
   $buildPage->addToBlock("promoter_city", $promoter_city);

   $promoter_county = '
      <input type="text" name="promoter_county" value="' . $currentUser->county . '">

   ';
   $buildPage->addToBlock("promoter_county", $promoter_county);


   $promoter_action_options = '

      <input type="submit" class="btn-floating btn-large red white-text text-bold " data-validate="1,1,1,0,1,0,0,0,0" value="Save" name="promoter_save">
     

   ';
   $buildPage->addToBlock("promoter_action_options", $promoter_action_options);






// stripe 
   if($currentUser->idstripe == ""){
      $stripe_connect = '
         <a href="' . $buildPage->settings['Stripe_connect_url'] . '"><img src="./images/providers/stripe.png"></a>
      ';
   }
   else{$stripe_connect = '<p class="btn blue lighten-2">Stripe Account is connected.</p>';}
   $buildPage->addToBlock("stripe_connect", $stripe_connect);
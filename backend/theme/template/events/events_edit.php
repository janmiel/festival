<?php

$admin_template_folder = "/events/";
$admin_template_file = "event_edit.phtml";


        

    $buildPage->addBlock("details_active");
    $buildPage->addBlock("pictures_active");
    $buildPage->addBlock("dates_active");
    $buildPage->addBlock("tickets_active");
    $buildPage->addBlock("venue_active");
    $buildPage->addBlock("venue_address_show_hide");
    $buildPage->addBlock("hideIfModify");
    $buildPage->addBlock("hideIfAddDate");
    $buildPage->addBlock("event_ticket");


$event_title = $eventVenuesAutocomplete = $monday_selected = $tuesday_selected = $wednesday_selected = $thursday_selected = $friday_selected = $saturday_selected = $sunday_selected = $eventDateAddDates = "";


    if(empty($idEvent) and isset($_GET['add'])){

        
        $thisIsNewEvent = true;
        $idEvent = createId();

        //because it is special situaltion, new object will be needed
        $eventBuilder = new eventBuilder();
        $eventBuilder->addToDetails('idevent', $idEvent);
        $eventBuilder->addToDetails('idpromoter', $currentUser->iduser);
        $buildPage->addToBlock("hideIfNew", "hide");
        $buildPage->addToBlock("details_active", " active");




    }else{
        $thisIsNewEvent = false;
        $idEvent = clearPost($_GET['edit']);
    

    }




    require_once("./backend/submenu.php");

    $buildPage->addToBlock("welcome_text_left", $eventBuilder->details['title']);
    $buildPage->addToBlock("welcome_text_right", "");
    $buildPage->addToBlock("welcome_bottom", "");


    if(isset($_GET['modify'])){
         
         $buildPage->addToBlock("hideIfModify", "hideMe");

    }elseif(isset($_GET['addDate'])){

    	$buildPage->addToBlock("hideIfAddDate", "hideMe");



//COPY DATE

        
        if(isset($_GET['copy']) and $_GET['copy'] != ""){
            //add right submenu
            $buildPage->addToBlock("welcome_text_right", "Copy existing event");
        
            if(!$eventBuilder->new_dates_temp['tickets']){
           
               
               $copy = clearPost($_GET['copy']);
               $array_dates = $eventBuilder->dates['event_date_dates'][0]['dates'];
               
               $dateToCopy = array_search($copy, array_column($array_dates, "idevent_date"));

               foreach ($array_dates[$dateToCopy]['tickets'] as $singleTicket) {
                   $eventBuilder->new_dates_temp['tickets'][] = $singleTicket;

               }

               $eventBuilder->new_dates_temp['idvenue'] = $array_dates[$dateToCopy]['idvenue'];

               

               
            }
        }
    }


    $resultEvent = getEventDetails($idEvent);


    if($eventBuilder and $resultEvent != false){
        //JUST DO THE CODE BELOW

    }else{
        $redirectUrl = "./404.html";
        redirectTo($redirectUrl);
    }


//CHECK IF EVENT IS FREE AND DOES NOT REQUIRE STRIPE
    if(isset($_GET['free'])){
        $eventBuilder->new_dates_temp['free_event'] = true;
        unset($_SESSION['free_event']);
        
    }



                //$eventBuilder = new eventBuilder($idEvent);        

                // $eventBuilder->addToDetails('idevent', $resultEvent['idevent']);
                // $eventBuilder->addToDetails('title', $resultEvent['artistname']);
                // $eventBuilder->addToDetails('description', $resultEvent['artistdesc']);
                // $eventBuilder->addToDetails('active', $resultEvent['active']);
                // $eventBuilder->addToDetails('category', $resultEvent['category']);
                // $eventBuilder->addToDetails('organisation', $resultEvent['organisation']);
                // $eventBuilder->addToDetails('idpromoter', $resultEvent['idpromoter']);


                // $eventBuilder->addToSettings('event_use_editor', $resultEvent['event_use_editor']);
    

   
         
    $event_title = '

        <input id="event_artistname" name="event_artistname" class="" type="text" value="' .
     
            $event_title .= $eventBuilder->details['title']
        . '">
       

    ';
        $buildPage->addBlock("event_title");
        $buildPage->addToBlock("event_title" , $event_title);


    if($eventBuilder->details['event_url'] != ""){
        $event_url = '<div class="col m3 s12 right-align ">
                        <span class="text-bold">EVENT URL: </span>
                </div>
                <div class="col m9 s12">    
                       <a target="blank" href="' . $buildPage->settings['page_url'] . $eventBuilder->details['event_full_url'] . '">' . $eventBuilder->details['event_url'] . '</a>
                    
                </div>';

            $buildPage->addBlock("event_url");
            $buildPage->addToBlock("event_url" , $event_url);
    }else{
        $buildPage->addToBlock("event_url", "");
    }


    $event_status = '
        <select name="event_status">
            <option value="pending" '; if($eventBuilder->details['active'] == 'pending'){ $event_status .= " selected";} $event_status .= '>Pending</option>
            <option value="active" '; if($eventBuilder->details['active'] == 'active'){ $event_status .= " selected";} $event_status .= '>Active</option>
            <option value="notActive" '; if($eventBuilder->details['active'] == 'notActive'){ $event_status .= " selected";} $event_status .= '>Not active</option>
        </select>
        <label>Event Status</label>
    ';
    $buildPage->addBlock("event_status");

    if($currentUser->user_access_tier_number >= 8){
        $buildPage->addToBlock("event_status" , $event_status);
    }




    $event_promoter = '
        <input id="event_promoter" type="text" value="';
        if($eventBuilder->details['organisation'] == ""){
            $event_promoter .= $eventBuilder->promoter['first_name'] . ' ' . $eventBuilder->promoter['last_name'];
        }else{
            $event_promoter .= $eventBuilder->promoter['organisation'];
        }
        $event_promoter .= '">
        <label for="event_name" class="active">Promoter</label>
    ';
    $buildPage->addBlock("event_promoter");
    if($currentUser->user_access_tier == "admin" or $currentUser->user_access_tier == "adminsuperuser"){
       
        $buildPage->addToBlock("event_promoter", $event_promoter);
    }



    $event_category = '
         <select name="event_category">
                    <option value="">--select--</option>
    ';
                    $categories = getCategries();
                    foreach($categories as $cat){
                        $event_category .= '<option value="' . $cat['id'] . '"';
                        if($eventBuilder->details['category'] == $cat['id']){
                            $event_category .= " selected";
                        }
                        $event_category .= '>' . $cat['name'] . '</option>';
                    }
                    $event_category .= '
            </select>
            <label>Category</label>
    ';

    $buildPage->addBlock("event_category");
    $buildPage->addToBlock("event_category", $event_category);


    $event_privacy = '
        <select name="event_privacy">
            <option value="public" '; if($eventBuilder->details['privacy'] == 'public'){ $event_privacy .= " selected";} $event_privacy .= '>Public</option>
            <option value="private" '; if($eventBuilder->details['privacy'] == 'private'){ $event_privacy .= " selected";} $event_privacy .= '>Private</option>
            
        </select>
        <label>Event Privacy</label>
    ';
    $buildPage->addToBlock("event_privacy", $event_privacy);

//EVENT RESTRICTION
    $event_age_info = '
         <select name="event_age_info" id="event_age_info">
                    <option value="">--select--</option>
    ';
                    $result = getAgeInfo();
                    foreach($result as $res){

                        if(strtolower($res['name']) == "other"){
                            $value = $eventBuilder->details['event_age_info_other_value'];
                        }else{
                            $value = $res['value'];
                        }

                       
                        $event_age_info .= '<option value="' . $res['idevent_age_info'] . '" data-value="' . $value . '"';
                       
                        
                        if($eventBuilder->details['idevent_age_info'] == $res['idevent_age_info']){
                            $event_age_info .= " selected";

                            $selected_age_info = array($res['name'], $value);

                        }
                        
                        $event_age_info .= '>' . $res['name'] . '</option>';
                        
                    }



                    $event_age_info .= '
            </select>
            <label>Age Info</label>
    ';

    $buildPage->addBlock("event_age_info");
    $buildPage->addToBlock("event_age_info", $event_age_info);

    
    $buildPage->addBlock('event_age_info_value');


    // age info content

    if(isset($selected_age_info)){

        $event_age_info_value = '
        <textarea ';
            if(strtolower($selected_age_info[0]) != "other"){$event_age_info_value .= ' disabled ' ;}
            $event_age_info_value .= ' class="materialize-textarea" type="text" name="event_age_info_description" id="event_age_info_description">
        ' . $selected_age_info[1] . '</textarea>
            <label>Age Info Content</label>';
        $buildPage->addToBlock('event_age_info_value', $event_age_info_value);
    }
    

    

    // feel in age info contact if age info changes

    $event_age_info_script = '
    <script>
        $("#event_age_info").on("change", function(){
            
            if($(this).find(":selected").text().toLowerCase() == "other"){
            
                $("#event_age_info_description").removeAttr("disabled");
            }else{
                $("#event_age_info_description").prop("disabled", true);
            }
            $("#event_age_info_description").val($(this).find(":selected").data("value"));
            $("#event_age_info_description").trigger("autoresize");

        });
    </script>';

    $buildPage->addToBlock("template_footer_after", $event_age_info_script);
    

    
    

// EVENT DESCRIPTION
    $event_description = '
        <textarea id="ckeditor" type="text" name="event_artistdesc" >' .
            $eventBuilder->details['description']
        . '</textarea>
        
    ';
    $buildPage->addBlock("event_description");
    $buildPage->addToBlock("event_description", $event_description);

   
//IMAGES

    $images = getEventImages($eventBuilder->getFromDetails('idevent'));

    $cropImageArray = array();
    $i = 0;
    $event_images = '

            
            <div class="event_edit_images_wrapper ">';

        if($images != false){
            $i++;
            foreach($images as $img){

            $src_ory = './images/events/' . $eventBuilder->details['idevent'] . '/' . $img['event_image_filename'];
            $src_image = './images/events/' . $eventBuilder->details['idevent'] . '/thumb/' . $img['event_image_filename'];
            $src_crop = './images/events/' . $eventBuilder->details['idevent'] . '/thumb/crop-' . $img['event_image_filename'];

    
            $src_img = $src_image;
            if(is_file($src_crop)){
                $src_img = $src_crop;
            }

            $src = './images/events/' . $eventBuilder->details['idevent'] . '/' . $img['event_image_filename'];

            $event_images .= '<div class="event_edit_images z-depth-2">
                            <input class="src_image" type="hidden" value="' . $src_ory . '">
                            <div class="event_edit_images_image">
                                <img class="event_edit_image responsive-img" src="' . $src_img . '?' . print_r(uniqid(), true) . '" >
                            </div>
                            <div class="event_edit_images_options">';

                            if($img['event_image_default'] == 0){
                                $event_images .= '<a class="left callback_get tooltipped" data-tooltip="Set as main picutre" data-position="bottom" href="promoter.html?build=events&edit=' . $eventBuilder->details['idevent'] . '&image=' . $img['idevent_image'] . '&defaultEventImage"><i class="material-icons black-text"  style="font-size:2em">check_box_outline_blank</i></i></a> ';
                            }else{
                                 $event_images .= '<i class="left material-icons black-text text-darken-1 " style="font-size:2em ;">check_box</i></i>';
                            }
                            $event_images .= '
                            <a class="callback_get tooltipped" data-position="bottom" data-tooltip="Delete picture" href="promoter.html?build=events&edit=' . $eventBuilder->details['idevent'] . '&image=' . $img['idevent_image'] . '&deleteImage"><i class="material-icons black-text" style="font-size:2em;">delete</i></a>

                            <br>
                            <div class="center">
                            <a class="center black-text crop-image" href="/promoter.html?build=events&edit=' . $eventBuilder->details['idevent'] . "&crop=" . $i . '">Crop Image</a>
                            </div>
                            </div>
                        </div>';
            $cropImageArray[$i] = $src;
            $i++;
            }

        }
        $event_images .='
        </div>';


 



    $buildPage->addBlock("event_images");
    $buildPage->addToBlock("event_images", $event_images);


    $buildPage->addToBlock("template_footer_after_scripts", '<link rel="stylesheet" type="text/css" href="./backend/theme/plugins/cropphoto/css/imgareaselect-default.css" />
        <script src="./backend/theme/plugins/cropphoto/scripts/jquery.imgareaselect.pack.js"></script>');
    

    $scriptUploadImage = '

        

        <script>
        $(document).ready(function(){
            $(document).on("click" , "#addEventImage", function(event){

                event.preventDefault();

                if($("#event_img").val() != ""){

                    
                    var formData = new FormData();
                    formData.append("callback", "true");
                    formData.append("event_add_picture", "true");
                    formData.append("event_img", $("input[type=file]")[0].files[0]); 
                    

                    callAjax("POST", formData, "", null);
                   

                }
                        

            });



            
            $(document).on("click", ".crop-image", function(event){

                event.preventDefault();




                var img_src = $(this).closest(".event_edit_images").find(".src_image").attr("value");
                console.log(img_src);

                var fullscreenImageloaderContent = `
                
                        <img id="imageToCrop" src="` + img_src + `" style="max-width:90%; max-height:80vh;">
                        <div class="divider widthPr-100"></div>
                        <button class="left btn black submit-crop-image"  name="crop_image">Crop Image</button>
                        <!--a href="#" class="right hideImageFullScreen btn black">X Close</a-->


                `;

                $("#fullscreenImageloaderContent").html(fullscreenImageloaderContent);

                $("#fullscreenImageloader").removeClass("hideMe");



                $("#imageToCrop").on("load", function(){

                    var imageW = $("#imageToCrop").prop("naturalWidth");
                    var imageH = $("#imageToCrop").prop("naturalHeight");



                    console.log(imageW);
                    console.log(imageH);
                    
                    var width = $("#imageToCrop").width();
                    var height = $("#imageToCrop").height();

                    height = height/2; 


                    $("#fullscreenImageloader img").imgAreaSelect({ 
                                aspectRatio: "2:1",
                                handles: true,
                                show:true, 
                                x1: 100, y1: 100, x2: height*2+100, y2: height+100,                               
                                parent: "#fullscreenImageloaderContent",
                                onSelectEnd: copyCoordinates,
                                imageWidth: imageW,
                                imageHeight: imageH

                            });

                        function copyCoordinates(img, selection){
                            $("#x").val(selection.x1);
                            $("#y").val(selection.y1);
                            $("#w").val(selection.width);
                            $("#h").val(selection.height);
                            $("#img_to_crop").val(img_src);
                            
                        }
                });
                


               



            });


        $(document).on("click", ".hideImageFullScreen", function(event){
            event.preventDefault();
            $("#fullscreenImageloader").addClass("hideMe");
            $("#fullscreenImageloaderContent").html("");
            $("#img_to_crop").val("");
        });

        
        $(document).on("click", ".submit-crop-image", function(event){

            event.preventDefault();

            var x = $("#x").val();
            var y = $("#y").val();
            var w = $("#w").val();
            var h = $("#h").val();
            var img_to_crop = $("#img_to_crop").val();

            var formData = new FormData();
            formData.append("callback", "true");
            formData.append("crop_image", "true");
            formData.append("x", x); 
            formData.append("y", y); 
            formData.append("w", w); 
            formData.append("h", h); 
            formData.append("img_to_crop", img_to_crop); 
            

            callAjax("POST", formData, "", null);

        });

    });

        </script>
            

    ';
    $buildPage->addToBlock("template_footer_after_scripts", $scriptUploadImage);


    //crop image

    if(isset($_GET['crop'])){
        
        $imageNumber = $_GET['crop'];
        // $i is the max amount of images
        if($imageNumber > 0 and $imageNumber <= $i and $cropImageArray[$imageNumber] != ""){
          

            $imageToCrop = '

            <img src="' . $cropImageArray[$imageNumber] . '" style="max-width:90%; max-height:80vh;">
            <div class="divider widthPr-100"></div>
            <button class="left btn black" type="submit" name="crop_image">Crop Image</button>
            <a href="#" class="right hideImageFullScreen btn black">X Close</a>


            ';

            $buildPage->addToBlock("fullScreenLoaderContent", $imageToCrop);

            $imageSizeArray = getimagesize($cropImageArray[$imageNumber]);


            $imageToCropScript = '
                <link rel="stylesheet" type="text/css" href="./backend/theme/plugins/cropphoto/css/imgareaselect-default.css" />
                <script src="./backend/theme/plugins/cropphoto/scripts/jquery.imgareaselect.pack.js"></script>

                <script>
                    $("#fullscreenImageloader").removeClass("hideMe");

                    
                        $("#fullscreenImageloader img").imgAreaSelect({ 
                            aspectRatio: "2:1",
                            handles: true,
                            parent: "#fullscreenImageloader",
                            onSelectEnd: copyCoordinates,
                            imageWidth: ' . $imageSizeArray[0] . ',
                            imageHeight: ' . $imageSizeArray[1] . ',
                            show: true

                        });


                    function copyCoordinates(img, selection){
                        $("#x").val(selection.x1);
                        $("#y").val(selection.y1);
                        $("#w").val(selection.width);
                        $("#h").val(selection.height);
                        $("#img_to_crop").val("' . $cropImageArray[$imageNumber] . '");
                        
                    }


                    $(".hideImageFullScreen").on("click", function(){
                        $("#fullscreenImageloader").addClass("hideMe");
                        $("#img_to_crop").val("");
                    });

                </script>             
                

            ';

            $buildPage->addToBlock("template_footer_after", $imageToCropScript);

        }
    }

  

  
  










///////////////////////////////////////////
    // EDIT EVENT DATE 


     // event venue

     
          
    $getVenues = getVenues();
    if($getVenues != false){
            foreach($getVenues as $venue){
                $eventDatesVenueSelect[$venue['address']] = null;
                $eventDatesVenueHashMap[$venue['address']] = $venue['idvenue'];
        }
    }

    $eventVenuesAutocomplete .= '

    <script>
    $(document).ready(function(){

        // venues map 
        VENUES = $.parseJSON(\'' . json_encode($eventDatesVenueHashMap) . '\');


        $("input.autocomplete").autocomplete({
            data: ' . json_encode($eventDatesVenueSelect) . ',
            minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
            onAutocomplete: function(val){

                $(".venue_address_show_hide").hide();
                venue_id = VENUES[val];
                $("#event_date_add_venue_id").val(venue_id);




                $.ajax({
                  type: "POST",
                  url: window.location,
                  data:  "event_date_add_venue_id=" + venue_id                 
                });

               

            },
          });

        $("#event_date_add_venue_name").on("keyup", function(){
            if($("#event_date_add_venue_name").val().length < 2 ){
                $(".venue_address_show_hide").show(); 
                $("#event_date_add_venue_id").val("");

                 $.ajax({
                  type: "POST",
                  url: window.location,
                  data:  "event_date_add_venue_id="                 
                });
            }
        });
           
        
    });
    </script>';

    $buildPage->addToBlock("template_footer_after", $eventVenuesAutocomplete);

    $buildPage->addBlock("event_date_add_venue_id");
    $buildPage->addBlock("event_date_add_venue_name");


    if(isset($eventBuilder->new_dates_temp['idvenue'])){
        $venue_id = $eventBuilder->new_dates_temp['idvenue'];
        $buildPage->addToBlock("event_date_add_venue_id", $venue_id);

        $venue_name = array_search($venue_id, $eventDatesVenueHashMap);

        $buildPage->addToBlock("event_date_add_venue_name", $venue_name);
        $buildPage->addToBlock("venue_address_show_hide", "hideMe");
    }




    //end of event venue



    //add new date
    // $eventDatesOptions .= '

    //     <a id="showAddDateForm" class="tooltipped" data-tooltip="Add new date"><i class="material-icons medium">add_circle_outline</i></a>

    // ';







////END OF - DON'T DELETE THIS AS AARON MAY CHANGE HIS MIND 




    $eventDatesStartDate = '
        
        <input type="text" placeholder="Start date" class="white datepicker browser-default width-100" name="event_date_add_start_date" id="event_date_add_start_date" >
        ';

    $buildPage->addToBlock("event_date_add_start_date", $eventDatesStartDate);



	$eventDatesEndDate ='

	<input type="text" placeholder="End date" class="white datepicker browser-default width-100" name="event_date_add_end_date" id="event_date_add_end_date" >
	';

	$buildPage->addToBlock("event_date_add_end_date", $eventDatesEndDate);


    $eventDatesTime ='

        <select class="margin-0 padding-2 height-auto   border-grey" name="event_date_add_start_time[]" id="event_date_add_start_time">
            <option value="" disabled="disabled" selected="selected">Select time</option>';

            $start_of_day = strtotime("00:00");
            $end_of_day = strtotime("23:30");
            $jump = "+30 minutes";
            $stop = 0;
            while($start_of_day <= $end_of_day){

                $start_of_day = date("H:i", $start_of_day);

                $eventDatesTime .= '<option value="' . $start_of_day . '"'; 
                  
                $eventDatesTime .= '>' . $start_of_day . '</option>';
            $stop++;
            //if($stop == 10){exit();}

            $start_of_day = strtotime($jump, strtotime($start_of_day));
            
            }
     
        $eventDatesTime .= '</select>
       ';

    $buildPage->addToBlock("event_date_add_time", $eventDatesTime);


    $eventDatesWeekDays ='

    <select class="margin-0 padding-2 height-auto  border-grey" multiple name="event_date_add_week_days[]" id="event_date_add_week_days">

        <option value="monday"' . $monday_selected . '>Monday</option>
        <option value="tuesday"' . $tuesday_selected . '>Tuesday</option>
        <option value="wednesday"' . $wednesday_selected . '>Wednesday</option>
        <option value="thursday"' . $thursday_selected . '>Thursday</option>
        <option value="friday"' . $friday_selected . '>Friday</option>
        <option value="saturday"' . $saturday_selected . '>Saturday</option>
        <option value="sunday"' . $sunday_selected . '>Sunday</option>

    </select>';

    $buildPage->addToBlock("event_date_add_week_days", $eventDatesWeekDays);

  



    $scriptMultipleDates = '
    <script>
    	$("#multiple_events").on("click", function(e){

            e.preventDefault(); 
    		
            if($("#event_date_add_start_time").prop("multiple") == false){

        		$(".multiple_dates").show();
                $("#event_date_add_start_time").prop("multiple", true);
                $("#multiple_events").val("1");
                $("select").material_select();


            }else{

                $(".multiple_dates").hide();
                $("#event_date_add_start_time").removeAttr("multiple");
                $("#multiple_events").val("");
                $("select").material_select();

            }
    	});
    	
    </script>
    ';

    $buildPage->addToBlock("template_footer_after", $scriptMultipleDates);






    $eventDatesDelete ='
        <a ><i class="material-icons black-text text-size-30">delete</i></a>
    ';

    $buildPage->addToBlock("event_date_add_date_delete", $eventDatesDelete);




    // show batches of dates from session
    $numberOfDates = 0;
    if(isset($eventBuilder->new_dates_temp['dates'])){

    



        $eventDatesArray = $eventBuilder->new_dates_temp['dates'];

        

        foreach($eventDatesArray as $key => $value){

            
                $eventDateAddDates .= '
                
                <div class="col s6 height-46 border-top border-left height-50 padding-0 center">
                   <div class="widthPr-100 padding-vw-1">' . $value['event_date_date'] . '</div>
                </div>
                <div class="col s5 height-46  border-top border-left height-50 padding-0 center">
                    <div class=" widthPr-100 padding-vw-1">' . $value['event_date_time'] . '</div>
                </div>
                <div class="col s1 height-46 border-top border-left border-right height-50 padding-0 center">
                    <a class="callback_get  padding-vw-1 " href="./promoter.html?build=events&edit=' . $eventBuilder->details['idevent'] . '&delete_date=' . $key . '"><i class="material-icons  black-text padding-vw-1">cancel</i>
                    </a>
                </div>
                    
             ';
               
             $numberOfDates++;
           
        }
        if($numberOfDates > 0){
            $eventDateAddDates .= '<input type="hidden" id="event_existing_dates" value="existing">';
        }


       
        

    }

    $buildPage->addToBlock("event_date_add_dates", $eventDateAddDates);


    //save date button

    if($numberOfDates > 0){
        $hideSaveForm = " hideMe";
        $hideShowForm = "";
    }else{
        $hideSaveForm = ""; 
        $hideShowForm = " hideMe";

        $script = '<script> $("#addDateForm").slideToggle(); </script>';
        $buildPage->addToBlock("template_footer_after_scripts", $script);
    }

    $saveDateButton = '

    <input class="' . $hideSaveForm . ' btn browser-default width-200 right black submit-data-to-collect callback_post" type="submit" id="add_event_date" value="+ADD ANOTHER DATE" name="event_date_add_save">
    <input class="' . $hideShowForm . ' btn browser-default width-200 right black  " onclick="showAddDateForm()" type="none" id="slide_event_date" value="+ADD ANOTHER DATE" >


    ';

    $buildPage->addToBlock("save_date_button", $saveDateButton);


    // end of add new date




    //TICKETS

    $numberOfTickets = 0;

    if(isset($eventBuilder->new_dates_temp['tickets'])){
        $event_tickets_array = $eventBuilder->new_dates_temp['tickets'];
        $eventTickets = "";
        
            if(!empty($event_tickets_array)){

// <div class="col s6 center border-top border-left height-50 padding-0 center">
//                    <div class="widthPr-100 padding-vw-1">' . $value['event_date_date'] . '</div>
//                 </div>
//                 <div class="col s5 center border-top border-left height-50 padding-0 center">
//                     <div class=" widthPr-100 padding-vw-1">' . $value['event_date_time'] . '</div>
//                 </div>
//                 <div class="col s1 border-top border-left border-right height-50 padding-0 center">
//                     <a class="callback_get  padding-vw-1 " href="./promoter.html?build=events&edit=' . $eventBuilder->details['idevent'] . '&delete_date=' . $key . '"><i class="material-icons  black-text padding-vw-1">cancel</i>
//                     </a>
//                 </div>

                $eventTickets = "";
                foreach($event_tickets_array as $key => $singleTicket){

                    $currency = convertCurrency($singleTicket['event_ticket_currency']);
                    
                    $eventTickets .= '
                    <div class="row margin-0 background-color-whitegray" >
                   
                        <div class="col s4 height-46 border-top border-left height-50 padding-0 center">
                            <div class="  padding-vw-1">' . $singleTicket['event_ticket_name'] . '</div>
                        </div>
                        <div class="col s3 height-46 border-top border-left height-50 padding-0 center">
                            <div class="padding-vw-1">' . $singleTicket['event_ticket_quantity'] . '</div>
                        </div>
                        <div class="col s3 height-46 border-top border-left height-50 padding-0 center">
                            <div class="padding-vw-1">'. $currency . $singleTicket['event_ticket_price'] . '</div>
                        </div>
                        <div class="col s2 border-top border-left border-right height-50 padding-0 center">
                        	<a href="#tickets" class="open-ticket-settings"><i class="material-icons black-text padding-vw-1">settings</i></a>
                        	<a class="callback_get callback_get  " href="promoter.html?build=events&edit=' . $eventBuilder->details['idevent'] . '&deleteTicket=' . $key . '"><i class="material-icons black-text padding-vw-1 " style="font-size:1.8em;">delete</i></a>
                        </div>
                        <div class="col s12 ticket-settings hideMe">                   
                        	

                                    <div class="row grey lighten-2 data-to-collect">

                                        <input type="hidden" id="event_existing_tickets" value="existing" >
                                        <input type="hidden" name="event_ticket_number" value="' . $key . '">

                                        <div class="col s12 m3 right-align">
                                            <div class="text-bold" style="padding-top:1rem">MORE INFO: </div>
                                        </div>

                                        <div class="col s12 m9 ">

                                            <div class="input-field col s12">
                                                 <input name="event_ticket_settings_ticket_information" type="text" value="'; if (isset($singleTicket['settings']['more_info'])) {$eventTickets .= $singleTicket['settings']['more_info']; } $eventTickets .= '" data-length="100" >
                                            </div>
                                            
                                        </div>

                                        <div class="col s12 m3 right-align">
                                            <div class="text-bold" style="padding-top:1rem">MORE OPTIONS: </div>
                                        </div>

                                        <div class="col s12 m9">
                                            <div class="row">

                                                <div class="input-field col s12">
                                                    <input type="checkbox" name="event_ticket_settings_ticket_delay" id="event_ticket_settings_ticket_delay_' . $key . '" ';
                                                    if(isset($singleTicket['settings']['delay']) and $singleTicket['settings']['delay'] == true){
                                                        $eventTickets .= 'checked';
                                                    }
                                                    $eventTickets .= '>
                                                    <label for="event_ticket_settings_ticket_delay_' . $key . '">Limit sales dates</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s12 input-field inline browser-default">
                                            
                                                    <b>Start Sales: </b>
                                                        <input type="text" style="width:20px" class="browser-default" placeholder="0" name="event_ticket_settings_ticket_delay_days" value="'; if (isset($singleTicket['settings']['delay_days'])){ $eventTickets .= $singleTicket['settings']['delay_days'] ; } $eventTickets .= '">
                                                         days and 
                                                         <input type="text" style="width:20px" class="browser-default" placeholder="0" name="event_ticket_settings_ticket_delay_hours" value="'; if (isset($singleTicket['settings']['delay_hours'])){ $eventTickets .= $singleTicket['settings']['delay_hours'];} $eventTickets .= '">
                                                          hours before the event starts.
                                                    


                                                </div>
                                                 <div class="row"> 
                                                    <div class="col s12 right-align"> 
                                                        <button name="event_ticket_settings_save" class="btn btn-large submit-data-to-collect callback_post">SAVE</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>



                                    </div>



                        </div>
                    </div>
                    ';
                    $numberOfTickets++;
                }
            } 

        $buildPage->addToBlock("event_ticket", $eventTickets);  

        
    }

    $scriptTicketSettings = '
        <script>
            $(document).on("click", ".open-ticket-settings", function(){

                event.preventDefault();
                           
                $(this).parent().parent().find(".ticket-settings").slideToggle("slow");

            });
            
        </script>
        ';

        $buildPage->addToBlock("template_footer_after", $scriptTicketSettings);

    //EVENT PRICE INPUT

    $event_ticket_price_input = "";
    if(!isset($eventBuilder->new_dates_temp['free_event']) or $eventBuilder->new_dates_temp['free_event'] != true){
        $event_ticket_price_input .= '<input type="text" class="width-80  browser-default" name="event_ticket_price" placeholder="0.00 = free" id="event_ticket_price">';
    }else{
        $event_ticket_price_input .= '<input type="text" class="width-80  browser-default" name="event_ticket_price" value="0.00" disabled="disabled" id="event_ticket_price">';
    }

    $buildPage->addToBlock("event_ticket_price", $event_ticket_price_input);


     //save date button

    if($numberOfTickets > 0){
        $hideSaveForm = " hideMe";
        $hideShowForm = "";
    }else{
        $hideSaveForm = ""; 
        $hideShowForm = " hideMe";

        $script = '<script> $("#addTicketForm").slideToggle(); </script>';
        $buildPage->addToBlock("template_footer_after_scripts", $script);
    }

     $saveTicketButton = '

    <input class="' . $hideSaveForm . ' btn browser-default right black  submit-data-to-collect callback_post" type="submit" id="add_event_ticket" value="+ADD ANOTHER TICKET" name="event_ticket_settings_save">
    <input class="' . $hideShowForm . ' btn browser-default width-200  right black  " onclick="showAddTicketForm()" type="none" id="slide_event_ticket" value="+ADD ANOTHER TICKET" >


    ';

    $buildPage->addToBlock("save_ticket_button", $saveTicketButton);






    // end tickets


 
    $checkCircle = '<i class="material-icons green right">check_circle</i>';
    
    $buildPage->addBlock("dates_checked");
    $buildPage->addBlock("tickets_checked");
    $buildPage->addBlock("venue_checked");


    $all_required_fileds_correct = 0; // just to check if all fileds are checked 
   
    if(isset($eventBuilder->new_dates_temp['idvenue'])){
        $buildPage->addToBlock("venue_checked", $checkCircle);
        $all_required_fileds_correct++;
    }
    if(isset($eventBuilder->new_dates_temp['dates']) and sizeof($eventBuilder->new_dates_temp['dates']) > 0){
        $buildPage->addToBlock("dates_checked", $checkCircle);
        $all_required_fileds_correct++;
    }
    if(isset($eventBuilder->new_dates_temp['tickets']) and sizeof($eventBuilder->new_dates_temp['tickets']) > 0){
        $buildPage->addToBlock("tickets_checked", $checkCircle);
        $all_required_fileds_correct++;
    }

    // EVENT ACTION OPTIONS

    $event_action_options ='';


     $event_action_options .= '
         <div class="fixed-action-btn horizontal active">
       
    ';

    if($all_required_fileds_correct == 3 and !isset($_GET['modify'])){

        $event_action_options .= '
       
          <a href="#" class="btn btn-large green trigger-modal" data-text="You are going to create ' . $numberOfDates . ' dates for this event. Proceed?" data-yes="' . $page_to_open . '?build=events&edit=' . $eventBuilder->details['idevent'] . '&publish=true" >Publish</a></a>
         <a class="btn btn-large blue" target="blank" href="' . $buildPage->settings['page_url'] . '?event/' . $eventBuilder->details['idevent_short'] . '/' . $eventBuilder->details['event_url'] . '/preview">Preview</a></a>
         
       
        ';
  
    }

    $event_action_options .= '
        <a class="btn btn-large red">
          <input type="submit" name="save_event" id="save_event" class="btn btn-large red no-box-shaddow" value="Save">
        </a>

        </div>
    ';


    $buildPage->addBlock("event_action_options");
    $buildPage->addToBlock("event_action_options", $event_action_options);








// javascript form validation

$eventFormValidation = '
<script>


$(document).on("click", "#save_event", function(event){

    $(".border-red").removeClass("border-red");

    var error = false;

    if($("#event_artistname").val() == ""){error = true; Materialize.toast("Event name missing",  5000, "red-background");}

    if(CKEDITOR.instances["ckeditor"].getData() == ""){error = true; Materialize.toast("Event description missing",  5000, "red-background");}

    if($("#event_date_add_venue_id").val() == "" ){
        
        if($("#event_date_add_venue_name").val() == ""){error = true; Materialize.toast("Venue name missing",  5000, "red-background"); $("#event_date_add_venue_name").addClass("border-red");}

        if($("#event_date_add_venue_street").val() == ""){error = true; Materialize.toast("Venue street missing",  5000, "red-background");$("#event_date_add_venue_street").addClass("border-red");}

        if($("#event_date_add_venue_city").val() == ""){error = true; Materialize.toast("Venue city missing",  5000, "red-background"); $("#event_date_add_venue_city").addClass("border-red");}

        if($("#event_date_add_venue_county").val() == ""){error = true; Materialize.toast("Venue county missing",  5000, "red-background"); $("#event_date_add_venue_county").addClass("border-red");}

    }else{
        if($("#event_date_add_venue_name").val() == ""){error = true; Materialize.toast("Venue not selected",  5000, "red-background"); $("#event_date_add_venue_name").addClass("border-red");}
    }

    if($("#event_existing_dates").val() != "existing"){

        if($("#event_date_add_start_time").val() == null){error = true; Materialize.toast("Time missing",  5000, "red-background"); $("#event_date_add_start_time").addClass("border-red");}
        if($("#event_date_add_start_date").val() == ""){error = true; Materialize.toast("Date missing",  5000, "red-background"); $("#event_date_add_start_date").addClass("border-red");}
    }

    if($("#event_existing_tickets").val() != "existing"){
        if($("#event_ticket_name").val() == ""){error = true; Materialize.toast("Ticket name missing",  5000, "red-background"); $("#event_ticket_name").addClass("border-red");}
        if($("#event_ticket_quantity").val() == ""){error = true; Materialize.toast("Ticket quantity missing",  5000, "red-background"); $("#event_ticket_quantity").addClass("border-red");}
        if($("#event_ticket_price").val() == ""){error = true; Materialize.toast("Ticket price missing",  5000, "red-background"); $("#event_ticket_price").addClass("border-red");}
    }
    

    if(error == true){
        event.preventDefault();
    }
});


</script>
';


if(!isset($_GET['modify'])){
    $buildPage->addToBlock("template_footer_after", $eventFormValidation);
}



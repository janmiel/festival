<?php

$ckeditor = '<script src="./backend/ckeditor/ckeditor.js"></script>';


$ckeditSetup = '
	<script>
  		CKEDITOR.replace("ckeditor", {
  		extraPlugins: "wordcount",
    	
    	skin: "moono-lisa",
    	

  		});
  		
  	</script>
  ';





 $chipsFunction = '
	 <script>

	 var tags = ""	; 

	  $(".chips").on("chip.add", function(e, chip){
	    var chips_data = $(".chips-initial").material_chip("data");

	    tags = "";
	    $.each(chips_data, function(key, value){
	    	tags += value.tag + ":::";
	    });

	    $("#page_tags").val(tags);

	  });

	  $(".chips").on("chip.delete", function(e, chip){
	    
	   	var chips_data = $(".chips-initial").material_chip("data");

	    tags = "";
	    $.each(chips_data, function(key, value){
	    	tags += value.tag + ":::";
	    });

	    $("#page_tags").val(tags);

	  });
	</script>
 ';



// add only on edit page
if(isset($_GET['edit'])){

$buildPage->addToBlock("template_footer_after", $ckeditor);

$buildPage->addToBlock("template_footer_after", $ckeditSetup);




}

if(isset($_GET['edit_event_date'])){
	$buildPage->addToBlock("template_footer_after", $detectRepeatType);
}

if(isset($_GET['report'])){

	$css = '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>';

	$buildPage->addToBlock('afterHeader', $css);

	$script = '	
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
	<script>
		$(".sortable").DataTable();
	</script>
	';

	$buildPage->addToBlock('template_footer', $script );

}

if(isset($_GET['report'])){

	$script = '	
	<script type="text/javascript" src="./backend/theme/js/table2csv.js"></script>
	<script>

	$(".export2csv").on("click", function() {
		$(".table2csv").table2csv();
	});

	</script>
	';

	$buildPage->addToBlock('template_footer', $script );

}




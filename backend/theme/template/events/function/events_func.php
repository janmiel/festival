<?php

if(isset($eventBuilder) and (!empty($eventBuilder->details) or isset($_POST['add']))){


// default redirectURL


	if(isset($eventBuilder->details['idevent'])){

		$redirectUrlTemp = "./promoter.html?build=events&edit=" . $eventBuilder->details['idevent'];	


		if(isset($_GET['modify'])){
			$redirectUrlTemp = "./promoter.html?build=events&edit=" . $eventBuilder->details['idevent'] . "&modify";	
		}elseif(isset($_GET['addDate'])){
			$redirectUrlTemp = "./promoter.html?build=events&edit=" . $eventBuilder->details['idevent'] . "&addDate";	
		}
	}else{
		$redirectUrlTemp = "./promoter.html?build=dashboard";
	}

	




	if(isset ($_POST['add']) and ($currentUser->idstripe != "" or $_POST['free_event'] == true))
	{
	                	
	           
		$idevent = createID();
				
		if($_POST['free_event'] == true){
			$free_event = "&free";
		}else{
			$free_event = "";
		}

	    $date = getDateForMe('date');

	    $promoter = $currentUser->iduser; 
	    $artistname = 'change me into real event name';
			 
		$sql = "INSERT INTO events (idevent, artistname , date, promoter, active, event_use_editor) VALUES('$idevent', '$artistname', '$date', '$promoter', 'active', '1')";

		$result = fireSql($sql , "insert", false);
	
			if ($result != false){
			$_SESSION['info'] = "Event added.";
			
			$redirectUrl = './promoter.html?build=events&edit=' . $idevent . $free_event;
			$saveClasses = true;
			include("./classes/autoload.php");
			redirectTo($redirectUrl);
			
		}else{
			$_SESSION['info'] = "Something went wrong.";
		}				
	}

	

		
		// zapisz zmiany 
	 if(isset ($_POST['save_event'])){



		$idevent = $eventBuilder->details['idevent'];
	    $event_status = clearPost($_POST['event_status']);
	    $event_privacy = clearPost($_POST['event_privacy']);
	    $event_category	 = clearPost($_POST['event_category']);
	    $event_artistname = clearPost($_POST['event_artistname']);
	    $event_artistdesc = replace_quotas($_POST['event_artistdesc']);

	    $event_age_info = clearPost($_POST['event_age_info']);


	    $event_img = $_FILES['event_img'];
		$event_use_editor = isset($_POST['event_use_editor']) ? 1 : 0;
				 					 
		$event_page_location = "";
		$event_url = replace_spaces($_POST['event_artistname'] , "-");
		$event_url = remove_special_characters($event_url);
		$event_hash = createPageHash(getEventShortId($idevent),  $event_url);

		$real_event_url = getRealEventUrl(getEventShortId($idevent), $event_url);

		//check if age info = other
		$tmp = getAgeInfo($event_age_info);
		if(strtolower($tmp[0]['name']) == "other"){
			$event_age_info_description = clearPost($_POST['event_age_info_description']);

			$event_age_info_description = ", event_age_info_other_value = '" . $event_age_info_description . "'";
		}else{
			$event_age_info_description = "";
		}
		
		
		$sql = "
		UPDATE events
		SET artistname = '$event_artistname', artistdesc = '$event_artistdesc', active = '$event_status', privacy = '$event_privacy', category = '$event_category', event_use_editor = '$event_use_editor', event_url = '$event_url', event_hash = '$event_hash', idevent_age_info = '$event_age_info' " . $event_age_info_description . "
		WHERE idevent = '$idevent'";

		fireSql($sql, "update", $single=false);
	
		
		
	 
	}


//IMAGES
	if(isset($_FILES['event_img']) ){



		$idevent = $eventBuilder->getFromDetails('idevent');
		

		$event_artistname = $eventBuilder->details['title'];
		$event_url =  $eventBuilder->details['event_url'];
		$event_img = $_FILES['event_img'];

			if($event_img['name']){

				

				$imageFoder = "./images/events/";
				$folderName = $idevent;
				$newFileName = $event_url . "_" . uniqid();
				$wmax = 600;
				$hmax = 600;
				$uploadResult = uploadImage($event_img, $imageFoder, $folderName, $newFileName, $wmax, $hmax);

				if($uploadResult[0] != false){
					//check if first image
					$result = getEventImages($idevent);
					if($result == false){
						$defaultImage = 1;

						// update facebook
						$real_event_url = getRealEventUrl(getEventShortId($idevent), $event_url);
						updateFacebook($real_event_url, $buildPage->settings['page_url']);

					}else{
						$defaultImage = 0;
					}

					$id = createID();
					$sql = "INSERT INTO event_image (idevent_image, idevent, event_image_filename, event_image_url, event_image_default) VALUES ('$id', '$idevent', '$uploadResult[1]', '$event_url', '$defaultImage')";
					fireSql($sql, "insert");



					
					

				}else{
					$_SESSION['error']['image'] = $uploadResult[1];
				}

			}

		
			
	
		
	}






	if(isset($_POST['crop_image'])){




		if(isset($_POST['x']) and isset($_POST['y']) and isset($_POST['w']) and isset($_POST['h'])){

			$x = $_POST['x'];
			$y = $_POST['y'];
			$width = $_POST['w'];
			$height = $_POST['h'];
			$img = $_POST['img_to_crop'];

			if($x >= 0 and $y >= 0 and $height > 0 and $width > 0) {
			
				$cropArray = array(
					"x" => $x,
					"y" => $y,
					"width" => $width,
					"height" => $height
				);

				crop_image($img, $cropArray);

				//update facebook

				$idevent = $eventBuilder->details['idevent'];
				$event_url =  $eventBuilder->details['event_url'];

				$real_event_url = getRealEventUrl(getEventShortId($idevent), $event_url);
				updateFacebook($real_event_url, $buildPage->settings['page_url'], $response = false);

				
			}


		}

		

	}




	if(isset($_GET['deleteImage'])){	
		if($_GET['image'] != ""){

			$idevent_image = clearPost($_GET['image']);
			
			$imageLocation = getImageLocation($idevent_image, clearPost($_GET['edit']));

		
			if($imageLocation != false){

				
				//@unlink($imageLocation[0]); // full size
				//@unlink($imageLocation[1]); // thumb

				$sql = "DELETE FROM event_image WHERE idevent_image = '$idevent_image'";
				fireSql($sql, 'delete');
			}
		}
		
		
	}

	if(isset($_GET['deleteDateImage'])){
		if($_GET['image'] != ""){

			$idevent_image = clearPost($_GET['image']);
			$folderName = clearPost($_GET['edit_event_date']) . "/" . $idevent_image;

			$imageLocation = getImageLocation($idevent_image, $folderName);

		
			if($imageLocation != false){

				
				//@unlink($imageLocation[0]); // full size
				//@unlink($imageLocation[1]); // thumb

				$sql = "DELETE FROM event_image WHERE idevent_image = '$idevent_image'";
				fireSql($sql, 'delete');
			}
		}
		header("Location:promoter.html?build=events&edit_event_date=" . clearPost($_GET['edit_event_date']));
	}

	if(isset($_GET['defaultEventImage'])){
		if($_GET['image'] != ""){

			$idimage = clearPost($_GET['image']);
			$idevent = $eventBuilder->getFromDetails('idevent');
				
			if(checkIfImageExist($idimage)){

				// set all as not default
				$sql = "UPDATE event_image SET event_image_default = '0' WHERE idevent = '$idevent' "; 
				fireSql($sql, 'update');
				// set this image as default
				$sql = "UPDATE event_image SET event_image_default = '1' WHERE idevent_image = '$idimage' and idevent = '$idevent'";
				fireSql($sql, 'update');


				//update facebook

				$idevent = $eventBuilder->details['idevent'];
				$event_url =  $eventBuilder->details['event_url'];

				$real_event_url = getRealEventUrl(getEventShortId($idevent), $event_url);
				updateFacebook($real_event_url, $buildPage->settings['page_url'], $response = true);
			}
		}
		

		
	}



















	// VENUE

	if(isset($_POST['event_date_add_venue_id']) and $_POST['event_date_add_venue_id'] != "" ){

		
		$selectedIdVenue = clearPost($_POST['event_date_add_venue_id']);


		
		// save venue Id for further use
		$eventBuilder->new_dates_temp['idvenue'] =  $selectedIdVenue;
		
		$idEvent = $eventBuilder->getFromDetails('idevent');

		if(isset($eventBuilder->new_dates_temp['dates']) and $eventBuilder->new_dates_temp['dates'] != ""){
		
			foreach($eventBuilder->new_dates_temp['dates'] as &$date){

				$date['idvenue'] = $selectedIdVenue;

			}
		}
		
	}

	// add venue from form

	if(isset($_POST['event_date_add_venue_name']) and $_POST['event_date_add_venue_id'] == ""){



			$idVenue = createID();
			if(isset($_POST['event_date_add_venue_name'])) {$venueName = clearPost($_POST['event_date_add_venue_name']);}else{$venueName = "";}
			if(isset($_POST['event_date_add_venue_street'])) {$venueStreet = clearPost($_POST['event_date_add_venue_street']);}else{$venueStreet = "";}
			if(isset($_POST['event_date_add_venue_post_code'])) {$venuePostCode = clearPost($_POST['event_date_add_venue_post_code']);}else{$venuePostCode = "";}
			if(isset($_POST['event_date_add_venue_city'])) {$venueCity = clearPost($_POST['event_date_add_venue_city']);}else{$venueCity = "";}
			if(isset($_POST['event_date_add_venue_county'])) {$venueCounty = clearPost($_POST['event_date_add_venue_county']);}else{$venueCounty = "";}
			//$venueCountry = clearPost($_POST['event_date_add_venue_country']);
			//$venueGeoLocation = clearPost($_POST['event_date_add_venue_geo_location']);

			$venueAddres = $venueStreet . ', ' . $venuePostCode . ', ' . $venueCity . ', ' . $venueCounty;

			$idPromoter = $eventBuilder->details['idpromoter'];

			$formValid = true;

			if($venueName == ""){

				$_SESSION['error']['venue']['venue_name'] = "Venue name required";
				$error['venue'] = "Venue name required";
				$formValid = false;
			}

			if($venueStreet == ""){

				$_SESSION['error']['venue']['venue_street'] = "Street required";
				$formValid = false;
			}

			if($venueCity == ""){

				$_SESSION['error']['venue']['venue_city'] = "City name required";
				$formValid = false;
			}

			if($venueCounty == ""){

				$_SESSION['error']['venue']['venue_county'] = "County required";
				$formValid = false;
			}

		

			if($venueName != "" and $venueCity != "" and $venueCounty != "" and $formValid == true){

				$sql = "INSERT INTO venues(idvenue, name,  address, venue_street, venue_post_code, venue_county, venue_country, venue_city, idpromoter) VALUES ('$idVenue', '$venueName', '$venueAddres', '$venueStreet', '$venuePostCode', '$venueCounty', '$venueCountry', '$venueCity', '$idPromoter')";
				fireSql($sql, "insert", false);

			
				// save venue Id for further use


				$eventBuilder->new_dates_temp['idvenue'] =  $idVenue;

				if($eventBuilder->new_dates_temp['dates'] != ""){
					foreach($eventBuilder->new_dates_temp['dates'] as &$date){

						$date['idvenue'] = $idVenue;

					}
				}
			
			
			
			
			}
			

		
	
		
		
	}






	// DATE
	if(isset($_REQUEST['event_date_add_save']) or (isset($_POST['event_date_add_start_date']) and $_POST['event_date_add_start_date'] != ""))
	{  


			
		$idevent = $eventBuilder->getFromDetails('idevent');
		$eventDatesForSave = array();

		$validateDate = true;

		

			
			
 			if(isset($eventBuilder->new_dates_temp['idvenue'])){
				$idvenue = ($eventBuilder->new_dates_temp['idvenue'] != "") ? $eventBuilder->new_dates_temp['idvenue'] : "";
			}else{
				$idvenue = "";
			}

		
			if(!isset($_POST['multiple_events']) or $_POST['multiple_events'] == ""){

				if(isset($_POST['event_date_add_start_time'][0]) and isset($_POST['event_date_add_start_date'])){

					$event_date_start = clearPost($_POST['event_date_add_start_date']);
					$event_date_time = $_POST['event_date_add_start_time'][0];

					

					$event_date_limit = $buildPage->getSettings('event_date_days_limit');

						if(!validateDate($event_date_start)){
							$_SESSION['error']['date']['start_date'] = "Start date not valid";
							$error['date'] = "Start date not valid";

							
						}

						if(empty($event_date_time)){
							$_SESSION['error']['date']['start_time'] = "Time not valid";
							$error['date'] = "Time not valid";
							
						}

								
						$idevent_date = createID();
						

						$datesArray = array(
							'idevent_date' => $idevent_date,
							'idevent' => $idevent,
							'event_date_date' => $event_date_start,
							'event_date_time' => $event_date_time,
							'idvenue' => $idvenue
							);
						
						if(!isset($error['date'])){
							$eventBuilder->new_dates_temp['dates'][] = $datesArray;
						}
				}
					
			}



			if(isset($_POST['multiple_events'])){


			if(isset($_POST['event_date_add_start_date'])) {$event_date_start = clearPost($_POST['event_date_add_start_date']);} else{$event_date_start = null;}
			if(isset( $_POST['event_date_add_start_time'])) {$event_date_time = $_POST['event_date_add_start_time'];} else {$event_date_time = null;}

			$event_date_end = clearPost($_POST['event_date_add_end_date']);
			if(isset($_POST['event_date_add_week_days'])) {$event_date_week_day = $_POST['event_date_add_week_days'];}else{$event_date_week_day = null;}		

				if(!validateDate($event_date_start)){
					$_SESSION['error']['date']['start_date'] = "Start date not valid";

					$validateDate = false;
				}

				if(!validateDate($event_date_end)){
					$_SESSION['error']['date']['end_date'] = "End date not valid";

					$validateDate = false;
				}

				if(empty($event_date_time)){
					$_SESSION['error']['date']['start_time'] = "Time not valid";
					$validateDate = false;
				}

				if(empty($event_date_week_day)){
					$_SESSION['error']['date']['days'] = "Week days not valid";
					$validateDate = false;
				}



				if($event_date_start < getDateForMe('date')){
					$_SESSION['error']['date']['start_date'] = "Start date from passt";
					$validateDate = false;

				}


				if($validateDate == true){						
	
					foreach($event_date_week_day as $week_day){

						if(strtolower($week_day) != strtolower(getDateForMe("dayName", null, $event_date_start))){
							$event_date_week = getDateForMe("getNext", $week_day, $event_date_start);
						}else{
							$event_date_week = $event_date_start;
						}	

						
						
						while($event_date_week <= $event_date_end){

							foreach($event_date_time as $event_time){

								$idevent_date = createID();

								$datesArray = array(
									'idevent_date' => $idevent_date,
									'idevent' => $idevent,
									'event_date_date' => $event_date_week,
									'event_date_time' => $event_time,
									'idvenue' => $idvenue
									);
							
									$eventBuilder->new_dates_temp['dates'][] = $datesArray;
							}
							
							$event_date_week = getDateForMe("addDays", "+7 days", $event_date_week);

						}



						

					}
				}

				
								
											
								
		
			}		

 			
			
			//after job is done redirect back to dates
			$eventDatesForSave = $eventBuilder->new_dates_temp["dates"];
			if(!empty($eventDatesForSave)){


				array_sort_by_column($eventDatesForSave, 'event_date_date');
				
				//$eventBuilder->new_dates_temp['dates'] = $eventDatesForSave;


			}	


	}



	// delete dates from collection

	if(isset($_GET['delete_date'])){

		$idevent = $eventBuilder->getFromDetails['idevent'];
		$idDate = clearPost($_GET['delete_date']);

	
		unset($eventBuilder->new_dates_temp["dates"][$idDate]);

		
			
	}










	// add ticket to date

		if(isset($_POST['event_ticket_name']) and $_POST['event_ticket_name'] != ""){


			


			$_SESSION['error']['ticket'] = "";
			$idEvent = $eventBuilder->getFromDetails('idevent');
			$ticket_name = (isset($_POST['event_ticket_name'])) ? clearPost($_POST['event_ticket_name']) : "";
			$ticket_quantity = (isset($_POST['event_ticket_quantity'])) ? clearPost($_POST['event_ticket_quantity']) : "";
			$ticket_price = (isset($_POST['event_ticket_price'])) ? clearPost($_POST['event_ticket_price']) : "";
			$ticket_currency = (isset($_POST['event_ticket_currency'])) ? clearPost($_POST['event_ticket_currency']) : "";

		// IF NOT STRIPE ACCOUNT THEN ONLY FEEN TICKETS CAN BE ADDED
			if(isset($eventBuilder->new_dates_temp['free_event'])){
				$ticket_price = "0.00";
			}
			
			if(empty($ticket_name)){
				$error['ticket'] .= "Ticket name empty";
			}
			if(empty($ticket_quantity)){
				$error['ticket'] .= "<br>Ticket quantity empty";
			}
			if(!check_variable($ticket_quantity, "int")){
				$error['ticket'] .= "<br>Ticket quantity is not a number";
			}
			if($ticket_price == "" or !check_variable($ticket_price, "float")){
				$error['ticket'] .= "<br>Ticket price empty or format is not correct ( example 12.59 )";

			}else{
				$ticket_price = replace_text(",", ".", $ticket_price);
				$ticket_price = priceFormat(round($ticket_price, 2, PHP_ROUND_HALF_UP));
			}

			if(empty($error['ticket'])){

				

				$ticket = array(
					"event_ticket_name" => $ticket_name,
					"event_ticket_quantity" => $ticket_quantity,
					"event_ticket_price" => $ticket_price,
					"event_ticket_currency" => $ticket_currency,
					"event_ticket_commission" => calculateCommission($ticket_price),
					"settings" => array(
						"more_info" => "",
                        "delay" => "", 
                        "delay_days" => 0,
                        "delay_hours" => 0,
                        "delay_timestamp" => 0
                        )
					);
				$eventBuilder->new_dates_temp['tickets'][] = $ticket;


			
			}

		
	


		}

		if(isset($_POST['event_ticket_settings_save']) and isset($_POST['event_ticket_number'])){

			$ticket_settings = array();
			$key = clearPost($_POST['event_ticket_number']);

			if(isset($_POST['event_ticket_settings_ticket_information'])){
				$ticket_settings['more_info'] = clearPost($_POST['event_ticket_settings_ticket_information']);
			}

			if(isset($_POST['event_ticket_settings_ticket_delay'])){

				$delay_days = clearPost($_POST['event_ticket_settings_ticket_delay_days']);
				$delay_hours = clearPost($_POST['event_ticket_settings_ticket_delay_hours']);

				$ticket_settings['delay'] = true;
				$ticket_settings['delay_days'] = $delay_days;
				$ticket_settings['delay_hours'] = $delay_hours;
				$ticket_settings['delay_timestamp'] = ($delay_days * 24 * 60 * 60) + ($delay_hours * 60 * 60);
			}else{
				$ticket_settings['delay'] = false;
				$ticket_settings['delay_days'] = 0;
				$ticket_settings['delay_hours'] = 0;
				$ticket_settings['delay_timestamp'] = 0;
			}

			$eventBuilder->new_dates_temp['tickets'][$key]['settings'] = $ticket_settings;


			

		}

		if(isset($_GET['deleteTicket']) and $_GET['deleteTicket'] != ""){

			$deleteTicket = $_GET['deleteTicket'];

			if($eventBuilder->new_dates_temp['tickets'][$deleteTicket]){
				unset($eventBuilder->new_dates_temp['tickets'][$deleteTicket]);

			}
		
		

		}



		// ADD DATES TO EVENT

		if(isset($_GET['publish']) and $_GET['publish'] == "true"){


	

			if(sizeof($eventBuilder->new_dates_temp['dates']) > 0){

				

				foreach($eventBuilder->new_dates_temp['dates'] as $event_date){
					

					
							

							$event_date_timestamp = strtotime($event_date['event_date_date'] . " " . $event_date['event_date_time']);

							addDateToEvent(
							$event_date['idevent_date'], 
							$event_date['idevent_date'], // required for easier date / time reconision 
							$eventBuilder->details['idevent'], 
							$event_date['event_date_date'],
							$event_date['event_date_time'],
							$event_date_timestamp,
							$event_date['idvenue'],
							'active'
							);

							
							foreach($eventBuilder->new_dates_temp['tickets'] as $ticket){

								$ticket_commission = calculateCommission($ticket['event_ticket_price'], $ticket['event_ticket_quantity']);

								addTicketToDate(
									$event_date['idevent_date'],
									$ticket['event_ticket_name'],
									$ticket['event_ticket_price'],
									$ticket['event_ticket_quantity'],
									"",
									$ticket_commission,
									$ticket['settings']
								);
								
							}

						
						


						



				

				
				}
				// destroy saved eventBUilder session
		
				//unset($eventBuilder->new_dates_temp);
				//unset($new_dates_temp);
			}


		
		
		
		}


}



?>

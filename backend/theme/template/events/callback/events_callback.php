<?php

if($callback == true){

	//data = data to return
	//script = javascript to execue
	//target = where to show new data
	//toggle = id to show/hide
	

	$data = $script = $target = $toggle = $modifyThis = "" ;
	
//IMAGES
	if(isset($_FILES['event_img']) or isset($_REQUEST['event_add_picture'])){


		$data = $event_images;
		if(isset($error['image'])  and $error['image'] != ""){
			$script = "<script>Materialize.toast('" . $error['image'] . "', 5000,  'red-background');</script>";
		}else{
			$script = '<script>
			Materialize.toast("Picture added", 5000); 			
			$("#event_img").val("");
			var images_list = $(".event_edit_images_wrapper").find(".crop-image");
			images_list.last().trigger("click");
			
			</script>';
		}
		$target = "event_images_callback";
		


		buildResponse($data, $target, $script);

		


	}

	elseif(isset($_POST['crop_image'])){


		$data = $event_images;
		if(isset($error['image'])  and $error['image'] != ""){
			$script = "<script>Materialize.toast('" . $error['image'] . "', 5000,  'red-background');</script>";
		}else{
			$script = '<script>
			Materialize.toast("Picture croped", 5000); 
			
			$("#event_img").val(""); 
			$("#fullscreenImageloader").addClass("hideMe");
                    $("#fullscreenImageloaderContent").html("");
                    $("#img_to_crop").val(""); 
			</script>';
		}
		$target = "event_images_callback";
		


		buildResponse($data, $target, $script);


	}



	elseif(isset($_GET['deleteImage'])){

		$data = $event_images;
		$script = "<script>Materialize.toast('Picture removed', 3000); $('.tooltipped').tooltip('remove');</script>	";;
		$target = "event_images_callback";

		buildResponse($data, $target, $script);
		

	}

	elseif(isset($_GET['defaultEventImage'])){

		$data = $event_images;
		$script = "<script>Materialize.toast('Picture set as main', 3000); $(document).find('.tooltipped').tooltip('remove');</script>	";;
		$target = "event_images_callback";

		buildResponse($data, $target, $script);
		

	}

//DATES

	elseif(isset($_POST['event_date_add_save'])){

			$data = $eventDateAddDates;
			if(isset($error['date'])  and $error['date'] != ""){
				$script = "<script>Materialize.toast('" . $error['date'] . "',  3000, 'red-background');</script>";
			}else{
				$script = "<script>Materialize.toast('Date saved', 3000);
				$('#addDateForm :input[type=text]').val(''); 
				</script>";
			}
			
			$target = "event_dates_callback";
			$toggle = "event_venue_add";
			buildResponse($data, $target, $script);
			
	}

	elseif(isset($_GET['delete_date'])){

		$data = $eventDateAddDates;
		$script = "<script>Materialize.toast('Date removed.', 3000);</script>";
		$target = "event_dates_callback";		


		buildResponse($data, $target, $script);
		
	}

//VENUE
	elseif(isset($_POST['event_date_add_venue_select']) or isset($_POST['event_date_add_venue_name'])){

		$data = $venue_name;
		if(isset($error['venue'])  and $error['venue'] != ""){
			$script = "<script>Materialize.toast('" . $error['venue'] . "',  3000, 'red-background');</script>";
		}else{
			$script = "<script>Materialize.toast('Venue saved', 3000);</script>";
		}
		
		$target = "event_date_add_venue_name";
		$toggle = "venue_address_show_hide";
		buildResponse($data, $target, $script, $toggle);
		


	}

//TICKET
	elseif(isset($_POST['event_ticket_name']) or isset($_GET['deleteTicket'])){

		$data = $eventTickets;

		if($_POST['event_ticket_name']){$message = 'Ticket added';}
		if($_GET['deleteTicket']){$message = 'Ticket removed';}

		if(isset($error['ticket']) and $error['ticket'] != ""){
			$script = "<script>Materialize.toast('" . $error['ticket'] . "',  3000, 'red-background');</script>";
		}else{
			$script = "<script>
				Materialize.toast('" . $message . "', 3000); 
				$('#addTicketForm  :input[type=text]').val(''); 
				$('#event_ticket_price').val('0.00');
				</script>";
		}
		
		$target = "event_ticket_callback";
		$toggle = "";


		

		buildResponse($data, $target, $script);
	


	}

	elseif(isset($_POST['event_ticket_settings_save']) and isset($_POST['event_ticket_number'])){

		$data = $eventTickets;

		$message = 'Settings saved';
		

		if(isset($error['ticket']['settings']) and $error['ticket']['settings'] != ""){
			$script = "<script>Materialize.toast('" . $error['ticket']['settings'] . "',  3000, 'red-background');</script>";
		}else{
			$script = "<script>Materialize.toast('" . $message . "', 3000);</script>";
		}
		
		$target = "event_ticket_callback";
		$toggle = "";


		

		buildResponse($data, $target, $script);
	


	}


	else{
	

	// if all callbacks passed debug
	
	print_r(json_encode(array(array("post" => $_POST, "get" => $_GET), "debug", "")));
	}
	

}


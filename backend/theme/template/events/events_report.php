<?php

$admin_template_folder = "/events/";
$admin_template_file = "event_report.phtml";


require_once("./backend/submenu.php");
$buildPage->addToBlock("welcome_text_left", "");
$buildPage->addToBlock("welcome_text_right", "");
$buildPage->addToBlock("welcome_bottom", "");



$idevent_date = clearPost($_GET['report']);

$resultTickets = getEventReport($idevent_date);

$buildPage->addBlock("event_report_table_title");
$buildPage->addBlock("event_report_table_content");

$totalAllTickets = 0;
$totalQty = 0;



if ($resultTickets != false){

	$buildPage->addToBlock("event_report_table_title", "");



	$buildPage->addToBlock("welcome_text_left", $resultTickets[0]['artistname'] . " - Attendee's List ");
	$buildPage->addToBlock("welcome_text_right", $resultTickets[0]['event_date_date'] . " at " . $resultTickets[0]['event_date_time']);
	$buildPage->addToBlock("welcome_bottom", "");

	
	$tableContent = '

	';

	foreach ($resultTickets as $resTicket) {


	
		$tableContent .= '
	
		<tr>
			<td>' . $resTicket['ticket_number_short'] . '</td>
			<td>' . $resTicket['name'] . '</td>
			<td>' . $resTicket['email'] . '</td>
			<td>' . $resTicket['event_ticket_name'] . '</td>
			<td>' . $resTicket['qty'] * $resTicket['item_price'] . ' &euro;
				 (' . $resTicket['qty'] . 'x' . $resTicket['item_price'] . ' &euro;)
			</td>
			<td>' . $resTicket['paymentstatus'] . '</td>
			<td>' . $resTicket['date'] . '</td>

		</tr>
		';

		$totalAllTickets += $resTicket['total'];
		$totalQty += $resTicket['qty'];
			
	}


	$buildPage->addToBlock("event_report_table_content", $tableContent);

	$eventReportSummary = '
	Summary:' . $totalQty . ' tickets. ' . $totalAllTickets . '&euro;
			
				
	';
	
	$buildPage->addToBlock("event_report_summary", ""); //$eventReportSummary);	

}else{
	$_SESSION['info'] = "NO RESULTS";
}





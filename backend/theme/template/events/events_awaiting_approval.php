<?php

$submenu .= '
	<a href="admin.php?build=events&add">Add new Event</a>
';

$printOut .= '<div id="eventsNotApproved">';

// list all not approved events

$active = "notActive";
$limit = 100;
$orderBy = "idevent DESC";
$date = "";
$mainEventOnly = true;

$result = getEvents($active, $limit, $orderBy, $date, $mainEventOnly );


if($result != false){


$printOut .= '<div class="rowTitle center"><span>Not approved events</span></div>';


	foreach($result as $r)
 	{


	$printOut .= '
		<div class="eventDetails">
			<div class="eventDescription">
			<div class="eventName">' . $r[artistname] . '</div>
			<div class="eventDate">' . $r[event_date_date] . '</div>
		</div>
		<div class="eventOptions"> <a href="admin.php?build=' . $build . '&edit=' . $r[idevent] . '" >edit</a> <a onclick="return confirm(\"delete?\");"" href="admin.php?build=' . $build . '&delete=' . $r[idevent] . '">delete</a></div>
		
	</div>';

	}

}

// list all recently approved events

$active = "active";
$limit = 50;
$orderBy = "idevent DESC";
$date = "";
$mainEventOnly = true;

$result = getEvents($active, $limit, $orderBy, $date, $mainEventOnly);


if($result != false){


$printOut .= '<div class="rowTitle center"><span>Recently approved events (last ' . $limit . ')</span></div>';


	foreach($result as $r)
 	{

	$printOut .= '
		<div class="eventDetails">

		<div class="eventDescription">
			<div class="eventName">' . $r[artistname] . '</div>
			<div class="eventDate">' . $r[event_date_date] . '</div>
		</div>
		<div class="eventOptions"> <a href="admin.php?build=' . $build . '&edit=' . $r[idevent] . '" >edit</a> <a onclick="return confirm(\"delete?\");"" href="admin.php?build=' . $build . '&delete=' . $r[idevent] . '">delete</a></div>
		
	</div>';

	}

}

$printOut .= '</div>';



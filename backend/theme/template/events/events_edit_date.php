<?php

$admin_template_folder = "/events/";
$admin_template_file = "event_edit_date.phtml";


  
    $buildPage->addBlock("dates_active");
    $buildPage->addBlock("tickets_active");
    $buildPage->addBlock("venue_active");





    $idEvent = clearPost($_GET['edit_event_date']);



    



    //if($resultEvent != false and !isset($_SESSION['eventBuilder'])){

    if ($eventBuilder){

        //unset($eventBuilder->dates['event_date_add_dates']['dates']);

                // $eventBuilder->addToDetails('idevent', $resultEvent['idevent']);
                // $eventBuilder->addToDetails('title', $resultEvent['artistname']);
                // $eventBuilder->addToDetails('description', $resultEvent['artistdesc']);
                // $eventBuilder->addToDetails('active', $resultEvent['active']);
                // $eventBuilder->addToDetails('category', $resultEvent['category']);
                // $eventBuilder->addToDetails('organisation', $resultEvent['organisation']);
                // $eventBuilder->addToDetails('idpromoter', $resultEvent['idpromoter']);


                // $eventBuilder->addToSettings('event_use_editor', $resultEvent['event_use_editor']);
    }

    $form_name = $eventBuilder->details['title'];
    $buildPage->addToBlock('form1_name',$form_name);

  

    // event venue

    $eventDatesVenueSelect .='
        <select  searchable="search here.." class="force-callback" name="event_date_add_venue_select" >';
            $eventDatesVenueSelect .= '<option value="">--select--</option>';
            $getVenues = getVenues();
                if($getVenues != false){
                    foreach($getVenues as $venue){
                        $eventDatesVenueSelect .='<option value="' . $venue['idvenue'] . '"';
                        if($eventBuilder->getFromDetails('idvenue') and $venue['idvenue'] == $eventBuilder->getFromDetails('idvenue')) {$eventDatesVenueSelect .= ' selected';}
                        $eventDatesVenueSelect .= '>' . $venue['name'] . ' - ' . $venue['address'] . '</option>';
                    }
                }

    $eventDatesVenueSelect .='
        </select>
        <label>Venue</label>
    ';


    $eventDatesVenueAdd .= '
        
        <a class="tooltipped" data-tooltip="Add new venue" data-position="bottom" id="showAddVenueForm" >Add new venue</a>

    ';


    $eventDatesVenueName .='
        
        <input type="text" name="event_date_add_venue_name" id="event_date_add_venue_name">
        <label for="event_date_add_venue_name">Name</label>

    ';

    $eventDatesVenueStreet .='
        
        <input type="text" name="event_date_add_venue_street" id="event_date_add_venue_street">
        <label for="event_date_add_venue_street">Street</label>

    ';

    $eventDatesVenuePostCode .='
        
        <input type="text" name="event_date_add_venue_post_code" id="event_date_add_venue_post_code">
        <label for="event_date_add_venue_post_code">Post Code</label>

    ';

    $eventDatesVenueCity .='
        
        <input type="text" name="event_date_add_venue_city" id="event_date_add_venue_city">
        <label for="event_date_add_venue_city">City</label>

    ';

    $eventDatesVenueCounty .='
        
        <input type="text" name="event_date_add_venue_county" id="event_date_add_venue_county">
        <label for="event_date_add_venue_county">County</label>

    ';

    $eventDatesVenueCountry .='
        
        <input type="text" name="event_date_add_venue_country" id="event_date_add_venue_country">
        <label for="event_date_add_venue_country">Country</label>

    ';

    $eventDatesVenueGeoLocation .='
        
        <input type="text" name="event_date_add_venue_geo_location" id="event_date_add_venue_geo_location">
        <label for="event_date_add_venue_geo_location">Geo Location</label>

    ';

        



    $eventDatesVenue = '


        <div class="row">
            <form id="event_venue_select"  method="post">                       
                <div class="input-field col s12" >
                    ' . $eventDatesVenueSelect . '
                </div>
               
               
                <div class="submit-field">
                        <input class="btn right callback_post hideMe" data-target="event_venue_select" type="submit" name="event_date_add_venue_save" value="Add">
                    </div>
            </form>
        </div>




        <div class="row">

            <div class="event_dates_options openHidden center-align" data-openId="event_venue_add" >' . $eventDatesVenueAdd . '</div>
        </div>
        
        <div class="row">

        <form id="event_venue_add" class="hideMe" method="post"> 
                <div  >
                    <div class="input-field"> 
                        ' . $eventDatesVenueName . '
                    </div>
                    <div class="input-field"> 
                        ' . $eventDatesVenueStreet . '
                    </div>
                    <div class="input-field">
                        ' . $eventDatesVenuePostCode . '
                    </div>
                    <div class="input-field">
                        ' . $eventDatesVenueCity . '
                    </div>
                    <div class="input-field">
                        ' . $eventDatesVenueCounty . '
                    </div>
                    <div class="input-field">
                        
                    </div>
                    <div class="input-field">
                        ' . $eventDatesVenueGeoLocation . '
                    </div>
                    <div class="submit-field center_align">
                        <input class="btn callback_post" data-target="event_venue_add"  type="submit" name="event_date_add_venue_save" value="Add">
                    </div>
                </div>
        </form>

        </div>';

    $event_venue_navigation = '

        
        <a class="right btn waves-effect waves-light" href="dates&promoter.html?build=events&edit_event_date=' . $eventBuilder->details['idevent'] . '">Next</a>
    ';


    $buildPage->addToBlock("event_date_add_venue", $eventDatesVenue);
    $buildPage->addToBlock("event_venue_navigation", $event_venue_navigation);



    //end of event venue



    //add new date
    // $eventDatesOptions .= '

    //     <a id="showAddDateForm" class="tooltipped" data-tooltip="Add new date"><i class="material-icons medium">add_circle_outline</i></a>

    // ';

    if(!$eventBuilder->getFromDates('event_date_add_dates')){

        $eventDatesStartDate .='
            
            <input type="date" class="datepicker" name="event_date_add_start_date" id="event_date_add_start_date" value="';
                        if(isset($eventBuilder->dates['event_date_add_start_date'])) {$eventDatesStartDate .= $eventBuilder->dates['event_date_add_start_date'];}
            $eventDatesStartDate .='">
            <label for="event_date_add_start_date"><span class="date_form_input_show_hide daily monthly weekly">Start</span> Date (YYYY-MM-DD)</label>

        ';

        $eventDatesEndDate .='
            
            <input type="date" class="datepicker" name="event_date_add_end_date" id="event_date_add_end_date" value="';
                if(isset($eventBuilder->dates['event_date_add_end_date'])) {$eventDatesEndDate .= $eventBuilder->dates['event_date_add_end_date'];}
            $eventDatesEndDate .='">
            <label for="event_date_add_end_date">End Date (YYYY-MM-DD)</label>


        ';

        $eventDatesMonthDay .='
            
            <select name="event_date_add_month_day" id="event_date_add_month_day">';

                for($i = 1; $i <= 31; $i++){
                    $eventDatesMonthDay .= '<option value="' . $i . '"'; 
                        if($i == $eventBuilder->dates['event_date_add_month_day']){ $eventDatesMonthDay .= ' selected';}
                    $eventDatesMonthDay .= '>' . $i . '</option>';


                }
            $eventDatesMonthDay .= '<option value="last_of_month">Last of monnth</option>';
            $eventDatesMonthDay .= '</select>
            <label for="event_date_add_month_day">Select Day</label>';
           

        $eventDatesTime .='

            <select multiple name="event_date_add_start_time[]" id="event_date_add_start_time">
                <option value="" disabled selected>Select time</option>';

                $start_of_day = strtotime("00:00");
                $end_of_day = strtotime("23:30");
                $jump = "+30 minutes";
                $stop = 0;
                while($start_of_day <= $end_of_day){

                    $start_of_day = date("H:i", $start_of_day);

                    $eventDatesTime .= '<option value="' . $start_of_day . '"'; 
                        if(isset($eventBuilder->dates['event_date_add_start_time']) and in_array($start_of_day , $eventBuilder->dates['event_date_add_start_time'])){ $eventDatesTime .= ' selected';}
                    $eventDatesTime .= '>' . $start_of_day . '</option>';
                $stop++;
                //if($stop == 10){exit();}

                $start_of_day = strtotime($jump, strtotime($start_of_day));
                
                }
         
            $eventDatesTime .= '</select>
            <label for="event_date_add_start_time">Select Time</label>';


        if($eventBuilder->getFromDates('event_date_add_repeat_type')){
            $repeatType = $eventBuilder->getFromDates('event_date_add_repeat_type');

                    $selected_single = "";
                    $selected_daily = "";
                    $selected_weekly = "";
                    $selected_monthly = "";

                switch($repeatType){

                    case "single":
                        $selected_single = " selected";
                    break;
                    
                    case "daily":
                        $selected_daily = " selected";
                    break;
                    
                    case "weekly":
                        $selected_weekly = " selected";
                    break;
                    
                    case "monthly":
                        $selected_monthly = " selected";
                    break;

                }
        }

        $eventDatesRepeatType .='

            <select class="" name="event_date_add_repeat_type" id="event_date_add_repeat_type">

               
                <option value="single" ' . $selected_single . '>Single</option>
                <option value="weekly" ' . $selected_weekly . '>Multiple</option>
                

            </select>
            <label for="event_date_add_repeat_type">Repeat Type</label>
        ';
        //<option value="monthly" ' . $selected_monthly . '>Monthly</option>




        if($eventBuilder->getFromDates('event_date_add_week_days')){

            $selectedDays = $eventBuilder->getFromDates('event_date_add_week_days');

            $monday_selected = $tuesday_selected = $wednesday_selected = $thursday_selected = $friday_selected = $saturday_selected = $sunday_selected = "";

            if(in_array("monday", $selectedDays)){
                $monday_selected = " selected";
            }

            if(in_array("tuesday", $selectedDays)){
                $tuesday_selected = " selected";
            }

            if(in_array("wednesday", $selectedDays)){
                $wednesday_selected = " selected";
            }

            if(in_array("thursday", $selectedDays)){
                $thursday_selected = " selected";
            }

            if(in_array("friday", $selectedDays)){
                $friday_selected = " selected";
            }

            if(in_array("saturday", $selectedDays)){
                $saturday_selected = " selected";
            }

            if(in_array("sunday", $selectedDays)){
                $sunday_selected = " selected";
            }
           

        }else{
             $monday_selected = " selected";
        }
        $eventDatesWeekDays .='

            <select multiple name="event_date_add_week_days[]" id="event_date_add_week_days">

                <option value="monday"' . $monday_selected . '>Monday</option>
                <option value="tuesday"' . $tuesday_selected . '>Tuesday</option>
                <option value="wednesday"' . $wednesday_selected . '>Wednesday</option>
                <option value="thursday"' . $thursday_selected . '>Thursday</option>
                <option value="friday"' . $friday_selected . '>Friday</option>
                <option value="saturday"' . $saturday_selected . '>Saturday</option>
                <option value="sunday"' . $sunday_selected . '>Sunday</option>

            </select>
        ';
        // $eventDatesRepeatType .= '
        //         <label>Repeat Type</label>
        // ';

        $eventDatesSave .='
            <input class="btn " data-target="event_dates_add" type="submit" value="Add" name="event_date_add_save">
        ';
    }

    // show batches of dates from session

    if($eventBuilder->getFromDates('event_date_add_dates')){

    // check if collection / date already active
        if($eventBuilder->getFromDates('active_node')){

            $active_node = $eventBuilder->getFromDates('active_node');

            $active_collection = (isset($active_node['collection'])) ? $active_node['collection'] : ""; 
            $active_date = (isset($active_node['date'])) ? $active_node['date'] : "";
            $active_time = (isset($active_node['time'])) ? $active_node['time'] : "";



        }

        $eventDatesArray = $eventBuilder->getFromDates('event_date_add_dates');

            $numberOfDates = 0;

        foreach($eventDatesArray as $key => $value){

            
                $eventDateAddDates .= '<ul class="collapsible" data-collapsible="accordion">
                        <li>
                          <div class="collapsible-header font_bold '; 

                          if(isset($active_collection) and $active_collection == $key){$eventDateAddDates .= ' active';}


                          if($value['details']['repeat_type'] == "weekly"){
                            $repeat_type_value = "multiple";
                          }else{
                            $repeat_type_value = $value['details']['repeat_type'];
                          }
                          $eventDateAddDates .= '">
                          <i class="material-icons">toc</i>
                          ' . $repeat_type_value . '
                            <span>';

                    if($value['details']['repeat_type'] == "single"){
                        $eventDateAddDates .= '
                            ' . $value['details']['start_date'];

                    }else{
                        $eventDateAddDates .= '
                            ' . $value['details']['start_date'] . ' - ' . $value['details']['end_date'];
                    }

                    $eventDateAddDates .= '
                            </span>
                          <a class=" secondary-content" href="./promoter.html?build=events&edit_event_date=' . $eventBuilder->details['idevent'] . '&dates&collection=' . $key . '&deleteDatesCollection"><i class=" material-icons ">delete</i></a>
                          
                          </div>
                          <div class="collapsible-body">
                                <ul class="collapsible" data-collapsible="accordion" ">';

                                foreach($value['dates'] as $singleDateKey => $singleDateValue ){
                                    $dayFromDate = getDateForMe("dayName", null, $singleDateValue['event_date_date']);

                                    $eventDateAddDates .= '
                                    <li>
                                        <div class="collapsible-header font_bold ';
                                        if(isset($active_date) and $active_date == $singleDateKey){$eventDateAddDates .= ' active';}
                                        $eventDateAddDates .= '">

                                        
                                            <a class="" href="./promoter.html?build=events&edit_event_date=' . $eventBuilder->details['idevent'] . '&dates&collection=' . $key . '&deleteDate=' . $singleDateKey . '"><i class="material-icons left">delete</i></a> 
                                      ' . $dayFromDate . ', 
                                      ' . $singleDateValue['event_date_date'] . '
                                       ( '; 
                                            $eventDateAddDates .= (sizeof($singleDateValue['event_date_time']) != 0) ? sizeof($singleDateValue['event_date_time']) : "no time set";

                                        $eventDateAddDates .= ' )

                                        </div>';

                                        if(!empty($singleDateValue['event_date_time'])){
                                            $eventDateAddDates .= '
                                            <div class="collapsible-body">

                                                    <ul class="collection">';

                                                    foreach($singleDateValue['event_date_time'] as $singleTimeKey => $singleTime){
                                                
                                                      $eventDateAddDates .= '
                                                      <li class="collection-item">
                                                                                                        
                                                        ' . $singleTime . '

                                                        <a class=" right " href="./promoter.html?build=events&edit_event_date=' . $eventBuilder->details['idevent'] . '&dates&collection=' . $key . '&date=' . $singleDateKey . '&deleteTime=' . $singleTimeKey . '"><i class="material-icons left ">delete</i>
                                                        </a>
                                                            
                                                        
                                                       </li>';
                                                    //increment number of dates if times available
                                                    $numberOfDates++;
                                                    }

                                                    $eventDateAddDates .= '</ul>

                                            </div> 
                                         ';

                                         
                                        }else{
                                            //increment number of dates +1
                                            $numberOfDates++;

                                        }
                                    $eventDateAddDates .= '
                                    </li>
                                    ';
                                }
                    $eventDateAddDates .='
                                </ul>
                            </div>
                        </li>
                        
                      </ul>
                       ';



                
        }

        $eventDateAddDates .= '
        <div> Total number of events to add: <span class="font_bold">' . $numberOfDates . '</span>';
        

    }

    $event_date_navigation = '

        <a class="right btn waves-effect waves-light" href="tickets&promoter.html?build=events&edit_event_date=' . $eventBuilder->details['idevent'] . '">Next</a>
    ';

    $buildPage->addBlock("event_dates");
    //$buildPage->addToBlock("event_dates", $eventDates); // it shows already saved event dates
    $buildPage->addToBlock("event_date_navigation", $event_date_navigation);
    $buildPage->addToBlock("event_dates_options", $eventDatesOptions);
    $buildPage->addToBlock("event_date_add_start_date", $eventDatesStartDate);
    $buildPage->addToBlock("event_date_add_end_date", $eventDatesEndDate);
    $buildPage->addToBlock("event_date_add_time", $eventDatesTime);
    $buildPage->addToBlock("event_date_add_end_time", $eventDatesEndTime);
    $buildPage->addToBlock("event_date_add_repeat_type", $eventDatesRepeatType);
    $buildPage->addToBlock("event_date_add_save", $eventDatesSave);


    $buildPage->addToBlock("event_date_add_week_days", $eventDatesWeekDays);
    $buildPage->addToBlock("event_date_add_month_day", $eventDatesMonthDay);
    $buildPage->addToBlock("event_date_add_venue_save", $eventDatesVenueSave);
    $buildPage->addToBlock("event_date_add_dates", $eventDateAddDates);


    // end of add new date




    //TICKETS

    


        $event_tickets_array = $eventBuilder->getFromTickets('ticket');
        if(!empty($event_tickets_array)){

            foreach($event_tickets_array as $key => $singleTicket){
                $eventTickets .= '
                    <tr class="">
                    <td>' . $singleTicket['event_ticket_name'] . '</td>
                    <td>' . $singleTicket['event_ticket_quantity'] . '</td>
                    <td>' . $singleTicket['event_ticket_price'] . '</td>
                    <td><a class="callback_get tooltipped" data-position="bottom" data-tooltip="Delete ticket" href="promoter.html?build=events&edit_event_date=' . $eventBuilder->details['idevent'] . '&deleteTicket=' . $key . '"><i class="material-icons " style="font-size:2em;">delete</i></a></td>
                    </tr>';
            }
        } 

        

    $buildPage->addToBlock("event_ticket", $eventTickets);
    $buildPage->addToBlock("event_ticket_navigation", $event_ticket_navigation);





    // end tickets


 
    $checkCircle = '<i class="material-icons green right">check_circle</i>';
    
    $buildPage->addBlock("dates_checked");
    $buildPage->addBlock("tickets_checked");
    $buildPage->addBlock("venue_checked");


    $all_required_fileds_correct = 0; // just to check if all fileds are checked 
   
    if($eventBuilder->details['idvenue']){
        $buildPage->addToBlock("venue_checked", $checkCircle);
        $all_required_fileds_correct++;
    }
    if(sizeof($eventBuilder->dates['event_date_add_dates']) > 0){
        $buildPage->addToBlock("dates_checked", $checkCircle);
        $all_required_fileds_correct++;
    }
    if(sizeof($eventBuilder->tickets['ticket']) > 0){
        $buildPage->addToBlock("tickets_checked", $checkCircle);
        $all_required_fileds_correct++;
    }

    // EVENT ACTION OPTIONS

    $event_action_options ='';

    if($all_required_fileds_correct == 3){
         $event_action_options .= '<a href="#" class="btn left trigger-modal" data-text="You are going to create ' . $numberOfDates . ' dates for this event. Proceed?" data-yes="' . $page_to_open . '?build=events&edit_event_date=' . $eventBuilder->details['idevent'] . '&publish=true" >Publish dates & tickets</a>

            <a class="btn right" target="blank" href="preview?event/' . $eventBuilder->details['idevent_short'] . '/' . $eventBuilder->details['event_url'] . '">Preview</a>';
    }else{
        $event_action_options = '<a class="btn right" href="promoter.html?build=events&edit_event_date=' . $eventBuilder->details['idevent'] .'">Save</a>';
    }



    $buildPage->addBlock("event_action_options");
    $buildPage->addToBlock("event_action_options", $event_action_options);


showVar($eventBuilder);








<?php

$admin_template_folder = "/dashboard/";
$admin_template_file = "dashboard.phtml";


require_once("./backend/submenu.php");


$option = "";
$stripe = true;
$addNewEvent = "";
$afterHeader = "";
$tableView = "";
$template_footer = "";
$template_footer_after = "";
$menu_single = "";


if($currentUser->idstripe == ""){

	$addNewEvent .= '
		<a href="' . $buildPage->settings['Stripe_connect_url'] . '"><img src="./images/providers/stripe.png"></a>
	';

	$stripe = false;

	$script = '<script>
		var free_event = false;

		$(document).on("click", "#create-event-button", function(e){


			if(free_event != true){

				e.preventDefault();
				Materialize.toast("Is it a free ticket? <button id=\"free_event_yes\" class=\"btn white blue-text margin-0\" style=\"margin-left:5px;\"> Yes </button><button id=\"free_event_no\" class=\"btn white blue-text\" style=\"margin-left:5px;\"> No </button>", 10000, "blue text-bold");
			}

		});

		$(document).on("click", "#free_event_no", function(e){

			Materialize.toast("Connect Stripe Account first", 4000, "red text-bold");

		});

		$(document).on("click", "#free_event_yes", function(e){

			free_event = true;
			$("#create-event").append("<input type=\"hidden\" name=\"free_event\" value=\"true\">");
			$("#create-event-button").trigger("click");


		});

		</script>';

	$buildPage->addToBlock("template_footer_after_scripts", $script);

}

	$addNewEvent .= '
			<form id="create-event" method="post" action="promoter.html?build=events">
				<button type="submit" id="create-event-button" class="btn waves-ligt waves-efect red accent-4 white-text text-bold" name="add">Create new event<i class="material-icons right small">add_circle_outline</i></button>
			</form>';


if(isset($_GET['option'])){

	switch($_GET['option']){

		case "myevents":
			$admin_template_file = "dashboard_myevents.phtml";
			$option = "myevents";
			$buildPage->addToBlock("welcome_text_right", $addNewEvent);
			$buildPage->addToBlock("welcome_text_left", "Welcome to TICKETSTOP!");
			$buildPage->addToBlock("welcome_bottom", "");

			break;
		case "pastevents":
			$admin_template_file = "dashboard_pastevents.phtml";
			$option = "pastevents";
			$buildPage->addToBlock("welcome_text_right", $addNewEvent);
			$buildPage->addToBlock("welcome_text_left", "Welcome to TICKETSTOP!");
			$buildPage->addToBlock("welcome_bottom", "");

			break;
	}
}else{
	// ADD NEW EVENT
		
		$buildPage->addToBlock("welcome_text_right", $addNewEvent);
		$buildPage->addToBlock("welcome_text_left", "Welcome to TICKETSTOP!");
		$buildPage->addToBlock("welcome_bottom", '<img src="./images/page/promotion/promotion.png" width="100%" style="padding-top:5px;">');

}






// list events owned by promoter

if($option == "myevents"){

	$orderBy = "E.artistname";

	$result = getEvents(null, null, $orderBy, null, null, true);


	if($result != false){


		$allMyEvents = '';

			foreach($result as $r)
		 	{
			
			$eventStatus = convertEventStatus($r['active']);

			$allMyEvents .= '
			<tr>
				<td class="eventName"><a href="promoter.html?build=events&edit=' . $r['idevent'] . '&modify" class="">' . $r['artistname'] . '</a></td>
				<td class="eventStatus">' . $eventStatus . '</td>
				<td class="eventOptions "> 
					<a href="promoter.html?build=events&edit=' . $r['idevent'] . '&addDate" class="" >[Add date to this event]</a>
				</td>
				
			</tr>';

			}


	}else{
		$allMyEvents = '<tr><td><p class="padding-20">Empty</p></td></tr>';
	}


	$buildPage->addToBlock("my_all_events_title", "MY EVENTS");
	$buildPage->addToBlock("my_all_events", $allMyEvents);

}





if($option == ""){

// UPCOMING DATES
	//$active = "active";
	$limit = 100;
	$orderBy = "ED.event_date_date ASC, ED.event_date_time ASC";
	$date = " >= '" . time() . "'";

	if($currentUser->user_access_tier == "promoter"){
		$promoter = $currentUser->iduser;
	}else{
		$promoter = null;
	}

	$result = getEventDate($idevent_date = null, $active = "active", $limit, $orderBy, $date, $groupBy = null, $privacy = null, $event_active = "active", $promoter);

	$upcomingEvents = createTableViewForDashboardEvents($result, $showEdit = true, $includeReport = true);   //bottom of this file


	$buildPage->addBlock("upcoming_dates_title");
	$buildPage->addToBlock("upcoming_dates_title", 'UPCOMING EVENTS');

	$buildPage->addBlock("upcoming_dates");
	$buildPage->addToBlock("upcoming_dates", $upcomingEvents);

}

if($option == "pastevents"){
	// PAST EVENTS

	$active = "active";
	$limit = 50;
	$orderBy = "ED.event_date_date DESC, ED.event_date_time DESC";
	$date = "< '" . time() . "'";
	$result = getEventDate($idevent_date = null, $active, $limit, $orderBy, $date);


	$pastEvents = createTableViewForDashboardEvents($result, $showEdit = false, $showReport = true);



	$buildPage->addBlock("past_dates_title");
	$buildPage->addToBlock("past_dates_title", 'PAST DATES');


	$buildPage->addBlock("past_dates");
	$buildPage->addToBlock("past_dates", $pastEvents);
}


// FUNCTIONS

function createTableViewForDashboardEvents($result, $showEdit = false, $includeReport = false){
	$tableView = "";
	if($result != false and $result[0]['idevent'] != ""){

		
		foreach($result as $r){

		

		$eventDetails = new eventDetails($r['idevent_date']);

		
	 	
		$event_status = convertEventStatus($r['event_date_status']);
		
		
		


		$tableView .= '
		<tr>
			
			<td class="eventName">'
			 . $eventDetails->eventDetails['title'] . '
			
			</td>
			<td class="eventName">
				<a href="' . $eventDetails->eventDateDetails['url'] . '" target="_blank">Preview</a>	
			
			</td>
			<td class="eventDate ">' . $eventDetails->eventDateDetails['date'] . ' at ' . $eventDetails->eventDateDetails['time'] . '
			</td>
			<td class="eventStatus">' . $eventDetails->eventDateDetails['status'] . '
			</td>
			<td class="eventTickets">';
			if($eventDetails->tickets['ticket']){
				foreach($eventDetails->tickets['ticket'] as $ticketDetails){
					
					$tableView .= '<b>' . $ticketDetails['name'] . '</b> [' . $ticketDetails['sold'] . ' / ' . $ticketDetails['quantity'] . ']';
					$tableView .= ' ';
				}
			}
			$tableView .= '	
			</td>

			<td class="eventTotal">' . $eventDetails->eventDateDetails['ticketTotal_no_commission'] . '
			</td>
			<td class="eventOptions "> 
			';

			
			if($includeReport == true){
				$tableView .= '
					<a class="left" href="promoter.html?build=events&report=' . $r['idevent_date'] . '" >Attendee&#39;s List</a> 
					<i class="material-icons  slide_target" data-target="more_options" style="font-size:18px; margin-left:5px; cursor:pointer;">add_circle_outline</i>
						<div class="more_options hideMe">
							<a class="" href="promoter.html?build=eventDates&edit=' . $r['idevent_date'] . '" >Manage date</a><br>
							<a class="" href="promoter.html?build=events&edit=' . $r['idevent'] . '&addDate&copy=' . $r['idevent_date'] . '" >Copy</a><br>
							<a class="" href="promoter.html?build=eventDates&edit=' . $r['idevent_date'] . '&scanner#scanner" >Scanner Setup</a> 
						</div>

				';
			}
			
			$tableView .= '
			</td>
		</tr>';
		}


	}else{
		$tableView .= '<tr><td><p class="padding-20">Empty</p></td></tr>';
	}

	return $tableView;

}





<?php

$afterHeader ='
	<!-- CSS  -->
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  	<link rel="stylesheet" href="./backend/theme/css/materialize.css">
  	<link href="backend/theme/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  ';

$template_footer = '
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	 <script src="backend/theme/js/materialize.js"></script>

	
';

$template_footer_after ='
	<script src="backend/theme/js/init.js"></script>

	<script src="backend/theme/js/script.js"></script>
';

$buildPage->addToBlock("template_footer_after_scripts", "");



// add all default block
$buildPage->addToBlock("template_footer_after", $template_footer_after);



$buildPage->addToBlock("afterHeader", $afterHeader);


$buildPage->addToBlock("title", "TICKETSTOP ADMIN");

$buildPage->addToBlock("template_footer", $template_footer);

$buildPage->addBlock("info");
if(isset($_GET['info']) && $_SESSION['info'] != ""){

	$info = $_SESSION[info];
	$buildPage->addToBlock("info", $info);
	$_SESSION['info'] = "";
}else{
	$_SESSION['info'] = "";
}





require_once("backend/menu.php");



	






<?php

if($callback == true){

	//data = data to return
	//script = javascript to execue
	//target = where to show new data
	//toggle = id to show/hide
	

	$data = $script = $target = $toggle = $modifyThis = "" ;
	
//IMAGES
	if(isset($_FILES['event_img']) or $_REQUEST['event_add_picture']){


		$data = $event_images;
		if(isset($error['image'])  and $error['image'] != ""){
			$script = "Materialize.toast('" . $error['image'] . "', 5000,  'red-background');";
		}else{
			$script = '<script>
			Materialize.toast("Picture added", 5000); 
			
			$("#event_img").val("");
			
			</script>';
		}
		$target = "event_images_callback";
		


		buildResponse($data, $target, $script);

		


	}

	elseif(isset($_POST['crop_image'])){


		$data = $event_images;
		if(isset($error['image'])  and $error['image'] != ""){
			$script = "Materialize.toast('" . $error['image'] . "', 5000,  'red-background');";
		}else{
			$script = '<script>
			Materialize.toast("Picture croped", 5000); 
			
			$("#event_img").val(""); 
			$("#fullscreenImageloader").addClass("hideMe");
                    $("#fullscreenImageloaderContent").html("");
                    $("#img_to_crop").val(""); 
			</script>';
		}
		$target = "event_images_callback";
		


		buildResponse($data, $target, $script);


	}



	elseif(isset($_GET['deleteImage'])){

		$data = $event_images;
		$script = "Materialize.toast('Picture removed', 3000); $('.tooltipped').tooltip('remove');	";;
		$target = "event_images_callback";

		buildResponse($data, $target, $script);
		

	}

	elseif(isset($_GET['defaultEventImage'])){

		$data = $event_images;
		$script = "Materialize.toast('Picture set as main', 3000); $('.tooltipped').tooltip('remove');	";;
		$target = "event_images_callback";

		buildResponse($data, $target, $script);
		

	}





//TICKET

	elseif(isset($_POST['event_ticket_add']) and isset($_POST['event_ticket_name']) and $_POST['event_ticket_name'] != ""){

		$data = $eventTickets;

		$message = 'Settings saved';
		

		if(isset($error['ticket']['settings']) and $error['ticket']['settings'] != ""){
			$script = "<script>Materialize.toast('" . $error['ticket']['settings'] . "',  3000, 'red-background');</script>";
		}else{
			$script = "<script>Materialize.toast('" . $message . "', 3000);</script>";
		}
		
		$target = "event_ticket_callback";
		$toggle = "";


		

		buildResponse($data, $target, $script);


	}

	elseif(isset($_POST['event_existing_ticket_settings_save']) and isset($_POST['event_ticket_number'])){

		$data = $eventExistingTicket;

		$message = 'Settings saved';
		

		if(isset($error['ticket']['settings']) and $error['ticket']['settings'] != ""){
			$script = "<script>Materialize.toast('" . $error['ticket']['settings'] . "',  3000, 'red-background');</script>";
		}else{
			$script = "<script>Materialize.toast('" . $message . "', 3000);</script>";
		}
		
		$target = "event_existing_tickets_callback";
		$toggle = "";


		

		buildResponse($data, $target, $script);
	


	}

	elseif(isset($_POST['event_ticket_settings_save']) and isset($_POST['event_ticket_number'])){

		$data = $eventTickets;

		$message = 'Settings saved';
		

		if(isset($error['ticket']['settings']) and $error['ticket']['settings'] != ""){
			$script = "<script>Materialize.toast('" . $error['ticket']['settings'] . "',  3000, 'red-background');</script>";
		}else{
			$script = "<script>Materialize.toast('" . $message . "', 3000);</script>";
		}
		
		$target = "event_ticket_callback";
		$toggle = "";


		

		buildResponse($data, $target, $script);
	


	}

	elseif(isset($_GET['deleteTicket']) and $_GET['deleteTicket'] != ""){


		$data = $eventTickets;

		$message = 'Ticket removed';
		

		if(isset($error['string'])){
			$script = "<script>Materialize.toast('" . $error['ticket']['settings'] . "',  3000, 'red-background');</script>";
		}else{
			$script = "<script>Materialize.toast('" . $message . "', 3000);</script>";
		}
		
		$target = "event_ticket_callback";
		$toggle = "";

		buildResponse($data, $target, $script);
	

	}


	else{
	

	// if all callbacks passed debug
	
	print_r(json_encode(array(array("post" => $_POST, "get" => $_GET), "debug", "")));
	}
	

}


<?php

$idevent_date = clearPost($_GET['edit']);
$result = getPromoterIdFromEventDate($idevent_date);


if(isset($eventDetails) and isset($currentUser) and ($currentUser->user_access_tier_number >= 8 or $currentUser->iduser == $result['idpromoter'])){


// default redirectURL

	$redirectUrlTemp = "./promoter.html?build=eventDates&edit=" . $idevent_date;	

	




		// zapisz zmiany 
	if(isset($_POST['save_event_date'])){



		
		$event_date_status = clearPost($_POST['event_date_status']);

		
		$sql = "
		UPDATE event_date
		SET event_date_status = '$event_date_status'
		WHERE idevent_date = '$idevent_date'";

		fireSql($sql, "update", $single=false);


		
		// add ticket to date

		if(isset($_POST['event_ticket_name']) and $_POST['event_ticket_name'] != ""){




			$idEvent = $eventDetails->idevent;
			$ticket_name = (isset($_POST['event_ticket_name'])) ? clearPost($_POST['event_ticket_name']) : "";
			$ticket_quantity = (isset($_POST['event_ticket_quantity'])) ? clearPost($_POST['event_ticket_quantity']) : "";
			$ticket_price = (isset($_POST['event_ticket_price'])) ? clearPost($_POST['event_ticket_price']) : "";


			if(empty($ticket_name)){
				$error['ticket'] .= "Ticket name empty";
			}
			if(empty($ticket_quantity)){
				$error['ticket'] .= "<br>Ticket quantity empty";
			}
			if(!check_variable($ticket_quantity, "int")){
				$error['ticket'] .= "<br>Ticket quantity is not a number";
			}
			if($ticket_price == ""){
				$error['ticket'] .= "<br>Ticket price empty";

			}else{
				$ticket_price = replace_text(",", ".", $ticket_price);


				if(!check_variable($ticket_price, "float")){
					$_SESSION['error']['ticket'] .= "<br> Ticket price format is not correct ( 12.59 )";
				}
				$ticket_price = number_format(round($ticket_price, 2, PHP_ROUND_HALF_UP), 2);
			}


			if(empty($error['ticket'])){



					$id = createID();
					$ticket_commission = calculateCommission($ticket_price);

					$sql = "INSERT INTO event_ticket (idevent_ticket, idevent_date, event_ticket_name, event_ticket_price, event_ticket_quantity, event_ticket_commission, event_ticket_active) 
						VALUES ('" . $id . "', '" . $idevent_date . "', '" . $ticket_name . "', '" . $ticket_price . "', '" . $ticket_quantity . "', '" . $ticket_commission . "', '0' )";
					
					$result = fireSql($sql, "insert", $single = false);

					

			}


			$redirectUrl = $redirectUrlTemp;


		}

	}





// MODIFY EXISTING TICKET

	if(isset($_POST['event_existing_ticket_settings_save']) and isset($_POST['event_ticket_number'])){

		$idticket = clearPost($_POST['event_ticket_id']);
		$key = clearPost($_POST['event_ticket_number']);


		// change quantity
		$newTicketQuantity = clearPost($_POST['event_ticket_new_quantity']);

		
		if($newTicketQuantity > 0 and check_variable($newTicketQuantity, "int")){

			if($newTicketQuantity > $eventDetails->tickets['ticket'][$key]['sold']){

				modifyTicket($idticket, "quantity", $newTicketQuantity);

			}else{
				$error['ticket']['settings'] = "Quantity not correct";
				$error['string'] = "Quantity not correct";
			}
		}

		$ticket_settings = array();

		$active = clearPost($_POST['event_ticket_active']);


		$ticket_settings['active'] = $active;
		
		if(isset($_POST['event_ticket_settings_ticket_information'])){
			$ticket_settings['more_info'] = clearPost($_POST['event_ticket_settings_ticket_information']);
		}

		if(isset($_POST['event_ticket_settings_ticket_delay'])){

			$delay_days = clearPost($_POST['event_ticket_settings_ticket_delay_days']);
			$delay_hours = clearPost($_POST['event_ticket_settings_ticket_delay_hours']);

			$ticket_settings['delay'] = true;
			$ticket_settings['delay_days'] = $delay_days;
			$ticket_settings['delay_hours'] = $delay_hours;
			$ticket_settings['delay_timestamp'] = ($delay_days * 24 * 60 * 60) + ($delay_hours * 60 * 60);
		}else{
			$ticket_settings['delay'] = false;
			$ticket_settings['delay_days'] = 0;
			$ticket_settings['delay_hours'] = 0;
			$ticket_settings['delay_timestamp'] = 0;
		}

		modifyTicket($idticket, "settings", $ticket_settings);


		$redirectUrl = $redirectUrlTemp;	

	}






}
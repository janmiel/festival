<?php

$admin_template_folder = "/eventDates/";
$admin_template_file = "eventDates_edit.phtml";


        

if(isset($_GET['edit']) and $_GET['edit'] != ""){

}else{

    $redirectUrl = "404.html";
    $redirectTo($redirectUrl);

}

$idEventDate = clearPost($_GET['edit']);



    $eventDetails = new eventDetails($idEventDate, $ticketsActive = false);


    if($eventDetails != false){
        //JUST DO THE CODE BELOW

    }else{
        $redirectUrl = "./404.html";
        redirectTo($redirectUrl);
    }

    require_once("./backend/submenu.php");

    $buildPage->addToBlock("welcome_text_left", $eventDetails->eventDetails['title']);
    $buildPage->addToBlock("welcome_text_right", "");
    $buildPage->addToBlock("welcome_bottom", "");
   
         
    $event_title = $eventDetails->eventDetails['title'];
     
 
        $buildPage->addBlock("event_title");
        $buildPage->addToBlock("event_title" , $event_title);


        $event_url = '<a target="blank" href="' . $eventDetails->eventDateDetails['url'] . '">' . $eventDetails->eventDetails['event_url'] . '</a>';
    
        $buildPage->addBlock("event_url");
        $buildPage->addToBlock("event_url" , $event_url);


    $event_status = $eventDetails->eventDetails['active'];
    $buildPage->addToBlock("event_status", $event_status);


    $event_date_status = '
        <select name="event_date_status">
            <option value="active" '; if($eventDetails->eventDateDetails['status'] == 'active'){ $event_date_status .= " selected";} $event_date_status .= '>Active</option>
            <option value="notActive" '; if($eventDetails->eventDateDetails['status'] == 'notActive'){ $event_date_status .= " selected";} $event_date_status .= '>Not active</option>
        </select>
        <label>Event Status</label>
    ';
    $buildPage->addBlock("event_date_status");

    if($currentUser->user_access_tier_number >= 8){
        $buildPage->addToBlock("event_date_status" , $event_date_status);
    }




    $event_category = $eventDetails->eventDetails['categoryName'];

    $buildPage->addBlock("event_category");
    $buildPage->addToBlock("event_category", $event_category);


    $event_privacy =  $eventDetails->eventDetails['privacy'];
    $buildPage->addToBlock("event_privacy", $event_privacy);

//EVENT RESTRICTION


    $event_age_info = $eventDetails->age_info['name'];

    $event_age_info .= '<br>' . $eventDetails->age_info['value'];
    
    $buildPage->addToBlock("event_age_info", $event_age_info);
    

    // feel in age info contact if age info changes

    $event_age_info_script = '
    <script>
        $("#event_age_info").on("change", function(){
            
            if($(this).find(":selected").text().toLowerCase() == "other"){
            
                $("#event_age_info_description").removeAttr("disabled");
            }else{
                $("#event_age_info_description").prop("disabled", true);
            }
            $("#event_age_info_description").val($(this).find(":selected").data("value"));
            $("#event_age_info_description").trigger("autoresize");

        });
    </script>';

    $buildPage->addToBlock("template_footer_after", $event_age_info_script);
    

    
    

// EVENT DESCRIPTION
    $event_description = $eventDetails->eventDetails['description'];
     
    $buildPage->addBlock("event_description");
    $buildPage->addToBlock("event_description", $event_description);

   





///////////////////////////////////////////
    // EDIT EVENT DATE 


     // event venue

     
          
    $buildPage->addToBlock("event_venue", $eventDetails->venue['name'] . '<br>' . $eventDetails->venue['address']);
        
  



    //end of event venue



    //add new date
    // $eventDatesOptions .= '

    //     <a id="showAddDateForm" class="tooltipped" data-tooltip="Add new date"><i class="material-icons medium">add_circle_outline</i></a>

    // ';







////END OF - DON'T DELETE THIS AS AARON MAY CHANGE HIS MIND 




// DATE

    $buildPage->addToBlock("event_date", $eventDetails->eventDateDetails['date']);


// TIME

    $buildPage->addToBlock("event_time", $eventDetails->eventDateDetails['time']);





//TICKETS



   $scriptTicketSettings = '
    <script>
        $(document).on("click", ".open-ticket-settings", function(){

            event.preventDefault();
                       
            $(this).parent().parent().find(".ticket-settings").slideToggle("slow");

        });
        
    </script>
    ';

    $buildPage->addToBlock("template_footer_after", $scriptTicketSettings);


    // existing tickets

    $event_existing_tickets_array = $eventDetails->tickets['ticket'];

    $eventExistingTicket = "";
    if(!empty($event_existing_tickets_array)){

            foreach($event_existing_tickets_array as $key => $singleTicket){

                $eventExistingTicket .= '
                <div class="row border-bottom">   
                    <div class="col s3">' . $singleTicket['name'] . '</div>
                    <div class="col s3">' . $singleTicket['quantity'] . ' (sold: ' . $singleTicket['sold'] . ')</div>
                    <div class="col s3">' . $singleTicket['price'] . '</div>
                    <div class="col s3 right-align">
                    ';

                    if($singleTicket['active'] == 0){
                        $eventExistingTicket .= 'Ticket inactive <i class="material-icons red-text">warning</i>';
                    }

                    $eventExistingTicket .= '
                        <a href="#tickets" class="open-ticket-settings"><i class="material-icons black-text">settings</i></a>
                        
                    </div>
                    <div class="col s12 ticket-settings hideMe">                   
                        

                                <div class="row white data-to-collect">

                                    <input type="hidden" id="event_existing_tickets" name="event_existing_ticket" >
                                    <input type="hidden" name="event_ticket_number" value="' . $key . '">
                                    <input type="hidden" name="event_ticket_id" value="' . $singleTicket['idticket'] . '">

                                     <div class="col s12 m3 right-align">
                                        <div class="text-bold" style="padding-top:1rem">NEW QUANTITY: </div>
                                    </div>

                                    <div class="col s12 m9 ">

                                        <div class="input-field col s12">
                                             <input name="event_ticket_new_quantity" >
                                        </div>
                                        
                                    </div>

                                    <div class="col s12 m3 right-align">
                                        <div class="text-bold" style="padding-top:1rem">MORE INFO: </div>
                                    </div>

                                    <div class="col s12 m9 ">

                                        <div class="input-field col s12">
                                             <input name="event_ticket_settings_ticket_information" value="' . $singleTicket['settings']['more_info'] . '" data-length="100" >
                                        </div>
                                        
                                    </div>



                                    <div class="col s12 m3 right-align">
                                        <div class="text-bold" style="padding-top:1rem">MORE OPTIONS: </div>
                                    </div>

                                    <div class="col s12 m9">


                                        <div class="row">

                                            <div class="input-field col s12">
                                                <input type="checkbox" name="event_ticket_active" id="event_ticket_active_' . $key . '" ';
                                                if($singleTicket['active'] == 1){
                                                    $eventExistingTicket .= 'checked';
                                                }
                                                $eventExistingTicket .= '>
                                                <label for="event_ticket_active_' . $key . '">Active</label>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="input-field col s12">
                                                <input type="checkbox" name="event_ticket_settings_ticket_delay" id="event_ticket_settings_ticket_delay_' . $key . '" ';
                                                if($singleTicket['settings']['delay'] == true){
                                                    $eventExistingTicket .= 'checked';
                                                }
                                                $eventExistingTicket .= '>
                                                <label for="event_ticket_settings_ticket_delay_' . $key . '">Limit sales dates</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 input-field inline browser-default">
                                        
                                                <b>Start Sales: </b>
                                                    <input type="text" style="width:20px" class="browser-default" placeholder="0" name="event_ticket_settings_ticket_delay_days" value="' . $singleTicket['settings']['delay_days'] . '">
                                                     days and 
                                                     <input type="text" style="width:20px" class="browser-default" placeholder="0" name="event_ticket_settings_ticket_delay_hours" value="' . $singleTicket['settings']['delay_hours'] . '">
                                                      hours before the event starts.
                                          
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 right-align">
                                        !Notice: you are modifying "on sale" ticket
                                            <button name="event_existing_ticket_settings_save" class="btn submit-data-to-collect callback_post">UPDATE</button>
                                        </div>
                                    </div>


                                </div>



                    </div>
                   
                </div>
                    ';
            }
        } 

    $buildPage->addToBlock("event_existing_tickets", $eventExistingTicket);

    // end tickets


 
  

    $all_required_fileds_correct = 0; // just to check if all fileds are checked 
   

    if(isset($eventDetails->new_dates_temp)){
        if($eventDetails->new_dates_temp['idvenue']){
            $buildPage->addToBlock("venue_checked", $checkCircle);
            $all_required_fileds_correct++;
        }
        if(sizeof($eventDetails->new_dates_temp['dates']) > 0){
            $buildPage->addToBlock("dates_checked", $checkCircle);
            $all_required_fileds_correct++;
        }
        if(sizeof($eventDetails->new_dates_temp['tickets']) > 0){
            $buildPage->addToBlock("tickets_checked", $checkCircle);
            $all_required_fileds_correct++;
        }
    }

    // EVENT ACTION OPTIONS

    $event_action_options ='';

    if($all_required_fileds_correct == 3 and !isset($_GET['modify'])){
         $event_action_options .= '<a href="#" class="btn left trigger-modal" data-text="You are going to create ' . $numberOfDates . ' dates for this event. Proceed?" data-yes="' . $page_to_open . '?build=events&edit=' . $eventDetails->details['idevent'] . '&publish=true" >Publish dates & tickets</a>

            <a class="btn right" target="blank" href="' . $buildPage->settings['page_url'] . '?event/' . $eventDetails->details['idevent_short'] . '/' . $eventDetails->details['event_url'] . '/preview">Preview</a>';
    }
        $event_action_options .= '<div class="fixed-action-btn"><button type="submit" name="save_event_date" id="save_event_date" class="btn-floating btn-large red white-text text-bold  ">Save</button></div>';
    



    $buildPage->addBlock("event_action_options");
    $buildPage->addToBlock("event_action_options", $event_action_options);




// SCANNER SETUP

    $textForScanner = "action=setupDevice&eventDateId=" . $eventDetails->eventDateDetails['idevent_date'];
    $textForScanner = encodeTextForScanner($textForScanner);

    $scannerSetup = '<script> 

        var qrcode = kjua(

            {
                render: "image",
                ecLevel: "H",
                minVersion: 6,
                ecLevel: "L",

                fill: "#333333",
                
                text: "' . $textForScanner .'",
                size: "300",
                back: "#ffffff",
                quiet: 0,                

                mode: "plain",


                mSize: 10,

                mPosX: 50,
                mPosY: 50,

                label: "TICKETSTOP",
                fontname: "Ubuntu",
                fontcolor: "#C70200",

                
            }



            );
    
        $(".qrcode").append(qrcode);

     </script>';

     $buildPage->addToBlock("template_footer_after_scripts", $scannerSetup);


// javascript qrcode

$scriptQrcode = '<script src="./backend/theme/js/qrcode.min.js"></script>';
$buildPage->addToBlock("template_footer_after", $scriptQrcode);



// javascript form validation

$eventFormValidation = '
<script>


$(document).on("click", "#save_event", function(event){

    $(".border-red").removeClass("border-red");

    var error = false;

    if($("#event_artistname").val() == ""){error = true; Materialize.toast("Event name missing",  5000, "red-background");}

    if(CKEDITOR.instances["ckeditor"].getData() == ""){error = true; Materialize.toast("Event description missing",  5000, "red-background");}

    if($("#event_date_add_venue_id").val() == "" ){
        
        if($("#event_date_add_venue_name").val() == ""){error = true; Materialize.toast("Venue name missing",  5000, "red-background"); $("#event_date_add_venue_name").addClass("border-red");}

        if($("#event_date_add_venue_street").val() == ""){error = true; Materialize.toast("Venue street missing",  5000, "red-background");$("#event_date_add_venue_street").addClass("border-red");}

        if($("#event_date_add_venue_city").val() == ""){error = true; Materialize.toast("Venue city missing",  5000, "red-background"); $("#event_date_add_venue_city").addClass("border-red");}

        if($("#event_date_add_venue_county").val() == ""){error = true; Materialize.toast("Venue county missing",  5000, "red-background"); $("#event_date_add_venue_county").addClass("border-red");}

    }else{
        if($("#event_date_add_venue_name").val() == ""){error = true; Materialize.toast("Venue not selected",  5000, "red-background"); $("#event_date_add_venue_name").addClass("border-red");}
    }

    if($("#event_existing_dates").val() != "existing"){

        if($("#event_date_add_start_time").val() == null){error = true; Materialize.toast("Time missing",  5000, "red-background"); $("#event_date_add_start_time").addClass("border-red");}
        if($("#event_date_add_start_date").val() == ""){error = true; Materialize.toast("Date missing",  5000, "red-background"); $("#event_date_add_start_date").addClass("border-red");}
    }

    if($("#event_existing_tickets").val() != "existing"){
        if($("#event_ticket_name").val() == ""){error = true; Materialize.toast("Ticket name missing",  5000, "red-background"); $("#event_ticket_name").addClass("border-red");}
        if($("#event_ticket_quantity").val() == ""){error = true; Materialize.toast("Ticket quantity missing",  5000, "red-background"); $("#event_ticket_quantity").addClass("border-red");}
        if($("#event_ticket_price").val() == ""){error = true; Materialize.toast("Ticket price missing",  5000, "red-background"); $("#event_ticket_price").addClass("border-red");}
    }
    

    if(error == true){
        event.preventDefault();
    }
});


</script>
';


if(!isset($_GET['modify'])){
    $buildPage->addToBlock("template_footer_after", $eventFormValidation);
}


$buildPage->addToBlock("debug", print_r($eventDetails, true));
<?php

$admin_template_folder = "/page/";
$admin_template_file = "page_edit.phtml";


$sql = "
select
idpage,
page_title,
page_template,
page_name,
page_description,
page_key_words,
page_url,
page_tags,
page_content,
page_publish,
page_use_editor,
page_location,
page_style
from cms_page where idpage = '" . clearPost($_GET['edit']) . "' ";

$result = fireSql($sql , "select" , true);


// formularz edycji

$formName = "Edit Page: " . $result['page_name'];
$pageName = '<input type="text"  id="page_name" name="page_name" value="' . $result['page_name'] . '">
            <label for="page_name" class="active">Name</label>
            ';

$pageTemplate = '
    <select name="page_template">
    		<option value="">None</option>
    ';


$path = $themes_path . "/template/";
$listOfTemplates = listFolders($path);


foreach($listOfTemplates as $template){
$pageTemplate .= "<option value=" . $template . "";
	if($result['page_template'] == $template){$pageTemplate .= " selected";}
$pageTemplate .= ">" . $template . "</option>";	
}


$pageTemplate .=' 
    </select>
    <label>Template</label>
    ';

$useEditor = '
    <input type="checkbox" id="page_use_editor" name="page_use_editor" ' . $result['page_use_editor'] . '
    ';

if ($result['page_use_editor'] == 0){
    $use_editor = 0;
}else {
    $use_editor = 1; $useEditor .= " checked";
}
$useEditor .= '
        >
        <label for="page_use_editor">Use editor</lebel>
        ';

$pageContent .= '
    Content
        <textarea name="page_content"
';

    if($use_editor == 1){$pageContent .= " id=ckeditor";}
    $pageContent .= '>';
    if(is_file($result['page_location'])){
        $text = file_get_contents(trim($result['page_location']));
            $pageContent .= htmlspecialchars($text);
    }
$pageContent .='   
    </textarea>
    ';

$pageTitle .='
    <input class="readonly" readonly type="text" id="page_title" name="page_location" value="' . $result['page_location'] . '" >
    <label for="page_title">File Name</label>
';
$pageDescription .='
    <textarea class="materialize-textarea" type="text" id="page_description" name="page_description"' . $result['page_description'] . '" ></textarea>
    <label for="page_description">Page Description</label>
';
$pageKeywords .='
    <input type="text" id="page_key_words" name="page_keywords" value="' . $result['page_key_words'] . '" >
    <label for="page_kaywords">Page Key Words</label>
';
$pageEditUrl .=
      '<input class="" type="text" id="page_url" name="page_url" value="' . $result['page_url'] . '" >
    <label for="page_url">Page URL: ' .$pageRoot. $result['page_url'] . ' </label>
';

$tagsArray = explode(":::", $result['page_tags']);

$pageTagsFooterJs .='
    <script>
        $(".chips-initial").material_chip({
        data: [
    ';
        foreach($tagsArray as $tags){
            if($tags != ""){
                $pageTagsFooterJs .='
                {tag: "' . $tags . '",},
                ';
                }
            }
        $pageTagsFooterJs .='
        ],
        });
    </script>
    ';

$pageTags .= '
    <label class="acitve">Tags</label><br>
  <div class="chips chips-initial"></div>
  <input type="text" class="hide" value="' . $result['page_tags'] . '" name="page_tags" id="page_tags">
';


$pagePublish ='
    <label class="active">Publish</label>
    <div class="switch">
        <label>
          Off
          <input name="page_publish"';

            if ($result['page_publish'] == 1) {$pagePublish .=  ' checked ' ;}
                $pagePublish .=  ' value="1"';

            if ($result['page_publish'] == 0) {$pagePublish .=  ' value="0" ';}

          $pagePublish .=' type="checkbox">
          <span class="lever"></span>
          On
        </label>
    </div>
';

$pageSave ='
    <input class="btn" type="submit" name="Save" value="Save" />
    ';

$buildPage->addToBlock("form1_name", $formName);
$buildPage->addToBlock("page_name", $pageName);
$buildPage->addToBlock("page_template", $pageTemplate);
$buildPage->addToBlock("page_editor", $useEditor);
$buildPage->addToBlock("page_content", $pageContent);
$buildPage->addToBlock("page_title", $pageTitle);
$buildPage->addToBlock("page_description", $pageDescription);
$buildPage->addToBlock("page_keywords", $pageKeywords);
$buildPage->addToBlock("page_url", $pageEditUrl);
$buildPage->addToBlock("page_tags", $pageTags);
$buildPage->addToBlock("page_publish", $pagePublish);
$buildPage->addToBlock("page_save", $pageSave);
$buildPage->addToBlock("template_footer_after", $pageTagsFooterJs);



$(document).ready(function(){

//loader



//callbacks

//response[0] = data
//response[1] = target
//response[2] = script

	$(document).on("click", ".callback_get", function(event){

		event.preventDefault();
		var url = $(this).attr("href");
		var url = url + "&callback";

		callAjax("GET", "", url, $(this));



	});


	$(document).on("click", ".callback_post", function(event){
	
		event.preventDefault();
		var formId = $(this).data("target");

		var validate = false;
		var form_validation = [];

		
		if($(this).hasClass("validate-form")){
			validate = true;
		}


		var formData = new FormData();

		inputContent = $(this).closest(".data-to-collect").find("input, select");

		if(validate == true){


			form_validation = validateForm(inputContent, $(this).data("validate"));


		}

		if(form_validation['validation'] == true || validate == false){

			$.each(inputContent, function(inputKey, inputValue){

				if(inputValue.type == "checkbox"){

					if(inputValue.checked === true){
						formData.append(inputValue.name, "1");
						
					}

				}else if(inputValue.type == "select-multiple"){

					selectValues = $(inputValue).val();
					$.each(selectValues, function(index, value){
						console.log(value);
						formData.append(inputValue.name, value );
					});	

				}else{
						
					formData.append(inputValue.name, inputValue.value);
					
				}
			});


			formData.append($(this).attr("name"), "1");

			var url = window.location.href.split("#")[0] + "&callback";

			
			//data.append("callback","1");

		    callAjax("POST", formData, url, $(this));
		
		}else{

			$(inputContent).removeClass("border-red");
			
			$.each(form_validation, function(inputKey, inputValue){
				
				
				if(inputValue == false){
					$(inputContent[inputKey]).addClass("border-red");  
				}

			});


		}

    

  	});

	$(document).on("click", ".trigger-modal", function(event){
		event.preventDefault();



		$(".modal").modal("open");
		var text = $(this).data('text');
		$("#modal-text").text(text);
		$('#modal_answer_yes').attr("href", $(this).data('yes'));
	});




/*ajax handler*/



	/*trigger image upload*/

	$(".file-path").on("change", function(){

		console.log(1);

		if($(".file-path").val().length > 1){

			console.log(2);
			$("#addEventImage").trigger("click");
		}
	});

	/*trigger select change*/
	$(document).on("change", "select.force-callback", function(){
		console.log('starting select change');
		$(this).closest("form").find(".callback_post").trigger("click");

	});


	$(".hideFullScreen").on("click", function(){
		$("#fullscreenloader").addClass("hideMe");
	});


	$(document).on("click", ".delete", function(){

		if(!confirm("Delete?")){
			return false;
		}

	});



	$(document).on('click', ".form-validation", function(event){

		


		var formData = new FormData();

		inputContent = $(this).closest(".data-to-collect").find("input, select");

		form_validation = validateForm(inputContent, $(this).data("validate"));

		console.log(inputContent);
		console.log($(this).data("validate"));


		if(form_validation['validation'] == false){

			event.preventDefault();


			$(inputContent).removeClass("border-red");
			
			$.each(form_validation, function(inputKey, inputValue){
				
				
				if(inputValue == false){
					$(inputContent[inputKey]).addClass("border-red");  
				}

			});




		}

		
	});




	$(document).on("click" , ".slide_target", function(){

	
		
			target = $(this).data('target');
			$(this).parent().find("."+target).slideToggle("slow");

			if($(this).text() == "remove_circle_outline"){

				$(this).text("add_circle_outline");				

			}else{
				$(this).text("remove_circle_outline");
			}
			
			
	});



	$(".printPrintable").click(function(){

		//$('.section-to-print')

      window.print();
      
    });





});


function callAjax(method, formData, url, thisObject = null){

		

		$.ajax({
            type: method,
            timeout: 30000,
            url: url,
            data: formData,
          
           
		    success:function(response){
		    	
		       	response = JSON.parse(response);

		       	if($("#" + response[1])){
		       		$("#" + response[1]).html(response[0]);
		       	}
		       	//add script
		       	if(response[2] != ""){

		       		$("#" + response[1]).append(response[2]);
		       	}
		       	//toggle something
		       	if(response[3] != ""){
		       		$("#" + response[3]).hide();	
		       	}

		       	init();
		    },
		    error: function(jqXHR, exception) {
		    	console.log('error');
          		console.log(jqXHR);
          		console.log(exception);
       		},

		   processData: false,
		   contentType: false,
		   cache: false
         });		

	}


	$( document ).ajaxStart(function() {
  		
  		$("#fullscreenloader .content").html('<img src="./images/page/loading.gif">');
		$("#fullscreenloader").removeClass("hideMe");
  		

	});

	$( document ).ajaxStop(function() {
  		
  		$("#fullscreenloader").addClass("hideMe");
  		$("#fullscreenloader .content").html("");
  		
	});




function validateForm(formData, requiredFields){

		var requiredFields = requiredFields.split(",");

		var validation = [];
		validation['validation'] = true;

		$.each(requiredFields, function(fieldKey, fieldValue){

			if(fieldValue == 1){

				if(formData[fieldKey].type == "checkbox"){

					if(formData.checked === true){
						validation[fieldKey] = true;
					}else{
						validation[fieldKey] = false;
						validation['validation'] = false;
					}
				
				}else{

					if(formData[fieldKey].value != ""){
						validation[fieldKey] = true;
					}else{
						validation[fieldKey] = false;
						validation['validation'] = false;
					}
					
				}
			}else{
				validation[fieldKey] = true;
			}
		
		});


return validation; 
}

function showAddDateForm(){
	
	$("#addDateForm").slideToggle("slow");
	$("#slide_event_date").addClass("hideMe");
	$("#add_event_date").removeClass("hideMe");
}

function showAddTicketForm(){
	
	$("#addTicketForm").slideToggle("slow");
	$("#slide_event_ticket").addClass("hideMe");
	$("#add_event_ticket").removeClass("hideMe");
}



(function($){
  $(function(){

  $(document).on("click", ".openHidden", function(){
          openWhat = $(this).attr("data-openId");
          $("#"+openWhat).slideToggle();
      });

    init();

  }); // end of document ready
})(jQuery); // end of jQuery name space



    function init(){

      
      $('.tooltipped').tooltip({delay: 350, position: "bottom"});

      $('.button-collapse').sideNav();
      $('.parallax').parallax();

      $(".dropdown-button").dropdown({
            
        belowOrigin: true, 
        constrain_width: false,
        
      });

      $('select').material_select();

      $('.collapsible').collapsible();

      $('.chips').material_chip();

      $(".datepicker").pickadate({
        format: 'yyyy-mm-dd',
        closeOnSelect: true,
         selectMonths: true, // Creates a dropdown to control month
        selectYears: 2, // Creates a dropdown of 15 years to control year
        min : new Date(),
        onSet: function( arg ){
          if ( 'select' in arg ){ //prevent closing on selecting month/year
              this.close();
          }
        }
      });
   

      

      


      $('.modal').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            in_duration: 300, // Transition in duration
            out_duration: 200, // Transition out duration
            starting_top: '4%', // Starting top style attribute
            ending_top: '10%', // Ending top style attribute
      }
      );


      




    }